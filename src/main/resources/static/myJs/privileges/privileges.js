$(document).ready(function () {
    //设置表标题
    var aoColumns = [
        {
            "data": function (data, type, val) {
                return "<input type='checkbox' value='" + data.id + "' name='ids'>";
            }, "sTitle": "<input type='checkbox' id='idT'>", "sWidth": "5px"
        },
        {"data": "name", "sTitle": "权限名称"},
        {"data": "url", "sTitle": "权限URL"},
        {"data": "type", "sTitle": "请求类型"},
        {"data": "desc", "sTitle": "权限描述"},
        {"data": "createStamp", "sTitle": "创建时间"}
    ];
    //初始化表格
    $('.dataTables-example').dataTable({
        "bLengthChange": true,  ///是否可以修改页面显示行数
        "aLengthMenu": [10, 25, 50, 100], ///设置可选的显示行数
        "iDisplayLength": 10,  ///默认显示10行
        "bSort": true, ///是否可排序
        "bPaginate": true, ///显示使用分液器
        "bFilter": true, //搜索栏
        "aoColumns": aoColumns
    });

    //加载数据
    function reloadTable() {
        $.ajax({
            type: 'get',
            url: '/privileges?size=100',
            dataType: 'json',
            success: function (msg) {
                $('.dataTables-example').dataTable().fnClearTable();
                $('.dataTables-example').dataTable().fnAddData(msg.rows.length == 0 ? null : msg.rows);
            },
            error: function () {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: '消息提示',
                    message: '服务器异常'
                });
            }
        });
    }

    //加载所有父权限
    reloadParent();
    //加载所有权限
    reloadTable();

//全选按钮
    $("#idT").click(function () {
        if (this.checked)
            $("input[name=ids]").prop("checked", true);
        else
            $("input[name=ids]").prop("checked", false);
    });

//打开添加权限界面
    $("#addBtn").click(function () {
        $(".col-md-12 input").val('');
        $(".col-md-12 textarea").val('');
        $("select[name=parent] option").each(function () {
            if ($(this).val == -1) $(this).attr("selected", true);
            else $(this).attr("selected", false);
        });
        $("#myModal").modal('show');
    });

//打开修改权限界面
    $("#updateBtn").click(function () {
        //判断权限是否选择了多个
        if ($("input[name=ids]:checked").length == 0 || $("input[name=ids]:checked").length > 1) {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_INFO,
                title: '消息提示',
                message: '请选择一个权限信息修改！'
            });
            return false;
        }
        $(".col-md-12 input[name!=sex][name!=status]").val('');
        //加载权限、权限信息
        $.ajax({
            type: 'get',
            url: '/privileges/' + $("input[name=ids]:checked").val(),
            dataType: 'json',
            success: function (msg) {
                $("#id").val(msg.data.id);
                $("input[name=name]").val(msg.data.name);
                $("input[name=type]").val(msg.data.type);
                $("input[name=url]").val(msg.data.url);
                if (msg.data.parentId == 0) {
                    $("select[name=parent] option").each(function () {
                        if ($(this).val == -1) $(this).attr("selected", true);
                        else $(this).attr("selected", false);
                    });
                } else {
                    $("select[name=parent] option").each(function () {
                        if ($(this).val() == msg.data.parentId) {
                            $(this).prop("selected", true);
                        } else
                            $(this).prop("selected", false);
                    });
                }
                $("textarea[name=desc]").val(msg.data.desc);
            },
            error: function () {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: '消息提示',
                    message: '服务器异常'
                });
            }
        });
        $("#myModal").modal('show');
    });

//删除权限信息
    $("#delBtn").click(function () {
        var ids = "";
        //获得选中的权限信息
        $("input[name=ids]:checked").each(function () {
            ids += $(this).val() + ',';
        });
        if (ids == "") {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_INFO,
                title: '消息提示',
                message: '请选择要删除的权限！'
            });
            return false;
        }
        BootstrapDialog.confirm({
            title: '消息提示',
            message: '确定要删除选中的权限信息吗？',
            type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
            closable: true, // <-- Default value is false
            draggable: true, // <-- Default value is false
            btnCancelLabel: 'Cancel', // <-- Default value is 'Cancel',
            btnOKLabel: 'OK', // <-- Default value is 'OK',
            btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
            callback: function (result) {
                if (result) {
                    ids = ids.substring(0, ids.length - 1);
                    //执行删除请求
                    $.ajax({
                        traditional: false,
                        type: 'delete',
                        url: '/privileges/' + ids,
                        dataType: 'json',
                        success: function (msg) {
                            if (msg.status == 500) {
                                BootstrapDialog.show({
                                    type: BootstrapDialog.TYPE_WARNING,
                                    title: '消息提示',
                                    message: msg.error
                                });
                            }
                            reloadTable();
                        },
                        error: function () {
                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_DANGER,
                                title: '消息提示',
                                message: '服务器异常'
                            });
                        }
                    });
                }
            }
        });
    });

//表单验证
    function check() {
        if ($("input[name=type]").val().trim() == "") {
            return false;
        }
        if ($("input[name=url]").val().trim() == "") {
            return false;
        }
        if ($("input[name=name]").val().trim() == "") {
            return false;
        }
        return true;
    }

//权限新增修改方法
    $("#subBtn").click(function () {
        if (!check()) {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_WARNING,
                title: '消息提示',
                message: '请将必填信息填完'
            });
            return false;
        }
        //获得权限id，为空则是添加，否则修改
        var id = $("#id").val();
        var type = id == '' ? "POST" : "PUT";
        var url = id == '' ? "/privileges" : "/privileges/" + id;
        var data = {
            name: $("input[name=name]").val(),
            url: $("input[name=url]").val(),
            type: $("input[name=type]").val(),
            parentId: $("select[name=parent]").val(),
            desc: $("textarea[name=desc]").val()
        };
        $.ajax({
            type: type,
            url: url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (msg) {
                reloadTable();
                $("#myModal").modal('hide');
                $(".col-md-12 input").val('');
                $(".col-md-12 textarea").val('');
                $("select[name=parent] option").each(function () {
                    if ($(this).val == -1) $(this).attr("selected", true);
                    else $(this).attr("selected", false);
                });
            },
            error: function () {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: '消息提示',
                    message: '服务器异常'
                });
            }
        });
    });

})
;