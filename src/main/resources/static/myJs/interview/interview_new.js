


//获取状态
function getStatus(value, option, rows) {
    var array = ["未预约", "已预约", "拒绝", "失约", "等通知", "已结束"];
    return array[value];
}

//获取职位类型
function position(value, option, rows) {
    var array = ["Java", "Python", "C++", "软件测试工程师"];
    return array[value];
}

//获取面试方式
function getInterview(value, option, rows) {
    var array = ["电话面试", "面试", "网络面试"];
    return array[value];
    //return value == 0 ? "电话面试" : "面试";
}

//获取面试阶段
function getStage(value, option, rows) {
    var array = ["初试", "二面", "三面"];
    return array[value];
}

//获取
function getInterviewResults(value, option, rows) {
    var array = ["不合适", "候选人", "已发office", "面试爽约"];
    return array[value];
}

var phone_ok = true;
$(document).ready(function () {
    $.jgrid.defaults.styleUI = 'Bootstrap';
    // Configuration for jqGrid Example 2
    $("#table_list_2").jqGrid({
        height: 480,
        autowidth: true,    //水平自动适应
        shrinkToFit: true,  //列头自动适应
        rowNum: 20,
        rowList: [10, 20, 50, 100],
        colNames: ['序号', '姓名', '手机号', '应聘职位', '面试公司', '状态', '面试方式', '面试阶段', '面试时间', '面试结果', '成绩', '操作'],
        colModel: [
            {
                name: 'id',
                align: "left",
                width: 25,
                key: true
            },
            {
                name: 'name',
                align: "left",
                width: 65,
            },
            {
                name: 'phone',
                align: "left",
                width: 100
            },
            {
                name: 'position',
                width: 60,
                align: "left",
                formatter: position
            },
            {
                name: 'corporation',
                width: 60,
                align: "left",
            },
            {
                name: 'status',
                width: 60,
                align: "left",
                formatter: getStatus
            },
            {
                name: 'interview',
                width: 80,
                align: "left",
                formatter: getInterview
            },
            {
                name: 'stage',
                width: 60,
                align: "left",
                formatter: getStage
            },
            {
                name: 'interviewTime',
                width: 120,
                align: "left",
            },
            {
                name: 'interviewResults',
                width: 60,
                align: "left",
                formatter: getInterviewResults
            }, {
                name: 'cj',
                width: 90,
                align: "left",
                formatter: function (value, option, rows) {
                    var value = '';
                    if (rows.zq != '') {
                        var zqs = rows.zq.split(',');
                        var totals = rows.total.split(',');
                        var topicTypes = rows.topicType.split(',');
                        for (var i = 0; i < topicTypes.length; i++) {
                            value += getTopicType(topicTypes[i], null, null) + ":" + zqs[i] + "/" + totals[i] + ",";
                        }
                    }
                    return value.substring(0, value.length - 1);
                }
            },
            {
                sortable: false,
                width: 155,
                formatter: function (value, option, rows) {
                    var btn = '<button name="updateBtn" class="btn btn-white btn-sm" value="' + rows.id + '" data-toggle="tooltip" data-placement="left" title="修改面试信息">修改</button>';
                    btn += '<button name="infoBtn" class="btn btn-white btn-sm" value="' + rows.id + '" data-toggle="tooltip" data-placement="left" title="面试人员详情">详情</button>';
                    if (rows.fileName == null || rows.fileName == '') {
                        btn += '<button name="jlNoBtn" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="下载简历">简历下载</button>';
                    } else
                        btn += '<button name="jlBtn" class="btn btn-white btn-sm" value="' + rows.id + '" data-toggle="tooltip" data-placement="left" title="下载简历">简历下载</button>';
                    return btn;
                }
            }
        ],
        multiselect: true,
        /*设置分页显示的导航条信息*/
        jsonReader: {
            root: "rows",
            page: "page",
            total: "pageSize",
            records: "total"
        },
        /*像后台请求的参数信息*/
        prmNames: {
            page: "page",
            size: "limit",
            sort: "order"
        },
        pager: "#pager_list_2",
        viewrecords: true,
        caption: "面试人员信息列表",
        hidegrid: false,
        url: "/interview",
        mtype: "GET",
        datatype: "json",
        gridComplete: function () {
            bind();
        }
    });

    jQuery("#table_list_2").jqGrid('navGrid', '#pager_list_2', {del: false, add: false, edit: false, search: false});

    jQuery("#table_list_2").navGrid('#pager_list_2', {
        edit: false,
        add: false,
        del: false,
        search: false
    }).navButtonAdd('#pager_list_2', {
        caption: "添加", buttonicon: "ui-icon-add", onClickButton: function () {
            $("#form_div input[name!=sex][name!=corporation]").val('');
            $("#form_div select[name=status] :eq(0)").prop("selected", true);
            $("#form_div select[name=interview] :eq(0)").prop("selected", true);
            $("#form_div select[name=stage] :eq(0)").prop("selected", true);
            $("#form_div select[name=interviewResults] :eq(1)").prop("selected", true);
            $("#myModal").modal("show");
        },
        position: "last"
    }).navButtonAdd('#pager_list_2', {
        caption: "删除",
        buttonicon: "ui-icon-del",
        onClickButton: function () {
            var ids = $('#table_list_2').jqGrid('getGridParam', 'selarrrow');
            if (ids == '') {
                swal("消息提示", "请选择要删除的数据", "info");
                return false;
            }
            swal({
                title: "您确定要删除选中的信息吗",
                text: "删除后将无法恢复，请谨慎操作！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "删除",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    traditional: false,
                    type: 'delete',
                    url: '/interview/' + ids,
                    dataType: 'json',
                    success: function (msg) {
                        if (msg.status == 200) {
                            $("#table_list_2").jqGrid().trigger("reloadGrid");
                            swal("删除成功！", "您已经永久删除了这些信息。", "success");
                        } else swal("消息提示", msg.error, "error");
                    },
                    error: function () {
                        swal("消息提示", "服务器异常", "error");
                    }
                });
            });
        },
        position: "last"
    });
    bindSearch();//加载搜索功能

    $("input[name=phone]").blur(function () {
        $this = $(this);
        if (this.value != '' && this.value.length == 11) {
            $.ajax({
                type: 'GET',
                url: '/interview/phone?phone=' + this.value,
                dataType: 'json',
                success: function (msg) {
                    if (msg.status == 200) {
                        if (msg.data != null) {
                            $this.next().html("手机号已存在!");
                            $("input[name=phone]").next().css("color", "red");
                            phone_ok = true;
                        } else {
                            $this.next().html("必填");
                            $("input[name=phone]").next().css("color", "");
                            phone_ok = false;
                        }
                    } else {
                        swal("消息提示", msg.error, "error");
                    }
                },
                error: function () {
                    swal("消息提示", "服务器异常", "error");
                }
            });
        }
    });

    //提交
    $("#addBtn").click(function () {
        $("input[name=phone]").blur();
        var id = $("#id").val();
        if (checkedInterview()) {
            return false;
        }
        if (phone_ok && id == '') {
            $("input[name=phone]").blur();
            return false;
        }
        if ($("select[name=position]").val().trim() == -1) {
            return false;
        }
        //获得用户id，为空则是添加，否则修改
        var type = id == '' ? "POST" : "PUT";
        var url = id == '' ? "/interview" : "/interview/" + id;
        var formData = new FormData();
        formData.append("id", id);
        formData.append("name", $("input[name=name]").val());
        formData.append("age", $("input[name=age]").val());
        formData.append("interviewTime", $("input[name=interviewTime]").val());
        formData.append("status", $("select[name=status]").val());
        formData.append("interview", $("select[name=interview]").val());
        formData.append("phone", $("input[name=phone]").val());
        formData.append("sex", $("input[name=sex]:checked").val());
        formData.append("position", $("select[name=position]").val());
        formData.append("corporation", $("input[name=corporation]").val());
        formData.append("channel", $("input[name=channel]").val());
        formData.append("stage", $("select[name=stage]").val());
        formData.append("interviewResults", $("select[name=interviewResults]").val());
        formData.append("evaluate", $("textarea[name=evaluate]").val());
        formData.append("startTime", $("input[name=startTime]").val());
        formData.append("endTime", $("input[name=endTime]").val());
        var file = $('#uploadBtn')[0].files[0];
        formData.append("file", file);
        $.ajax({
            type: type,
            url: url,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            data: formData,
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 200) {
                    $("#myModal").modal('hide');
                    $("#form_div input[name!=sex]").val('');
                    $("#table_list_2").jqGrid().trigger("reloadGrid");
                    swal("消息提示", "操作成功!", "success");
                } else {
                    swal("消息提示", msg.error, "error");
                }
            },
            error: function () {
                swal("消息提示", "服务器异常", "error");
            }
        });
    });
});

//绑定查询功能
function bindSearch() {
    $div = $(".ui-jqgrid-title").parent();
    var name = '<div class="ui-jqgrid-title" style="margin-bottom: -88px;margin-left: -99px;margin-top: 6px;"><label>姓名:&nbsp;&nbsp;</label><input id="name" style="height: 21px;width:80px;" type="text">';
    var sex = '<label style="margin-left: 20px">性别:&nbsp;&nbsp;</label><select style="width: 100px;height: 21px;" id="sex">';
    sex += '<option selected value="">不限</option>';
    sex += '<option value="false">女</option>';
    sex += '<option value="true">男</option></select>';
    var position = '<label style="margin-left: 20px">应聘职位:&nbsp;&nbsp;</label><select style="width: 100px;height: 21px;" id="position"><option value="">不限</option><option value="0">Java</option><option value="1">Python</option></select>';
    var status = '<label style="margin-left: 20px">状态:&nbsp;&nbsp;</label><select id="status" style="width: 80px;height: 21px;">';
    status += '<option selected value="">不限</option>';
    status += '<option value="0">未预约</option>';
    status += '<option value="1">已预约</option>';
    status += '<option value="2">拒绝</option>';
    status += '<option value="3">失约</option>';
    status += '<option value="4">等通知</option>';
    status += '<option value="5">已结束</option></select>';
    var interview = '<label style="margin-left: 20px">面试方式:&nbsp;&nbsp;</label><select id="interview" style="width: 100px;height: 21px;" >';
    interview += '<option selected value="">不限</option>';
    interview += '<option value="0">电话面试</option>';
    interview += '<option value="1">面试</option></select>';
    interview += '<option value="2">网络面试</option></select>';
    var timeInt = '<label style="margin-left: 20px">面试时间:&nbsp;&nbsp;</label><select id="timeInt" style="width: 100px;height: 21px;">';
    timeInt += '<option selected value="">不限</option>';
    timeInt += '<option value="0">今天</option>';
    timeInt += '<option value="1">明天</option>';
    timeInt += '<option value="2">后天</option>';
    timeInt += '<option value="7">本周</option></select>';
    var result = '<label style="margin-left: 20px">面试结果:&nbsp;&nbsp;</label><select id="result" style="width: 100px;height: 21px;">';
    result += '<option selected value="">不限</option>';
    result += '<option value="0">不合适</option>';
    result += '<option value="1">候选人</option>';
    result += '<option value="2">已发office</option>';
    result += '<option value="3">面试爽约</option></select>';
    var zw = '<button style="margin-left: 20px;margin-bottom: 59px;visibility:hidden;" class="btn btn-white btn-sm"  data-toggle="tooltip" data-placement="left" title="查询">Search</button></div>';
    var search = '<button id="searchBtn" style="margin-left: 18px;margin-bottom: 2px;" class="btn btn-white btn-sm"  data-toggle="tooltip" data-placement="left" title="查询">Search</button>';
    /*$div.html($div.html() + "<div style='margin-top: 5px;margin-left: 100px;'>" + name + sex + position + status + interview + timeInt + result + search + zw + "</div>");*/
    $("#searchBtn").click(function () {
        $("#table_list_2").jqGrid('setGridParam', {
            postData: {
                name: $("#name").val(),
                sex: $("#sex").val(),
                position: $("#position").val(),
                status: $("#status").val(),
                interview: $("#interview").val(),
                interviewResults: $("#result").val(),
                timeInt: $("#timeInt").val()
            }
        }).trigger("reloadGrid");
    });
}

//绑定按钮事件
function bind() {
    $("button[name=updateBtn]").click(function () {
        $.ajax({
            type: 'get',
            url: '/interview/' + this.value,
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 200) {
                    $("#id").val(msg.data.id);
                    $("input[name=name]").val(msg.data.name);
                    $("input[name=age]").val(msg.data.age);
                    $("input[name=interviewTime]").val(msg.data.interviewTime);
                    $("select[name=status] option").each(function () {
                        if ($(this).val() == msg.data.status) $(this).prop("selected", true);
                        else $(this).prop("selected", false);
                    });
                    $("select[name=interview] option").each(function () {
                        if ($(this).val() == msg.data.interview) $(this).prop("selected", true);
                        else $(this).prop("selected", false);
                    });
                    $("input[name=phone]").val(msg.data.phone);
                    $("input[name=sex]").each(function () {
                        if ($(this).val() == msg.data.sex + '') $(this).prop("checked", true);
                        else $(this).prop("checked", false);
                    });
                    //$("input[name=position]").val(msg.data.position);

                    $("select[name=position] option").each(function () {
                        if ($(this).val() == msg.data.position) $(this).prop("selected", true);
                        else $(this).prop("selected", false);
                    });

                    $("input[name=corporation]").val(msg.data.corporation);
                    $("input[name=channel]").val(msg.data.channel);
                    $("select[name=stage] option").each(function () {
                        if ($(this).val() == msg.data.stage) $(this).prop("selected", true);
                        else $(this).prop("selected", false);
                    });
                    $("select[name=interviewResults] option").each(function () {
                        if ($(this).val() == msg.data.interviewResults) $(this).prop("selected", true);
                        else $(this).prop("selected", false);
                    });
                    $("textarea[name=evaluate]").val(msg.data.evaluate);
                    checkedInterview();
                    $("#myModal").modal('show');
                } else {
                    swal("消息提示", msg.error, "error");
                }
            },
            error: function () {
                swal("消息提示", "服务器异常", "error");
            }
        });
    });
    $("button[name=infoBtn]").click(function () {
        $.ajax({
            type: 'get',
            url: '/interview/' + this.value,
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 200) {
                    $("#infoDiv span[name=name]").text(msg.data.name);
                    $("#infoDiv span[name=phone]").text(msg.data.phone);
                    $("#infoDiv span[name=age]").text(msg.data.age);
                    $("#infoDiv span[name=sex]").text(msg.data.age);
                    $("#infoDiv span[name=interviewTime]").text(msg.data.interviewTime);
                    $("#infoDiv span[name=interview]").text(getInterview(msg.data.interview));
                    $("#infoDiv span[name=status]").text(getStatus(msg.data.status));
                    $("#infoDiv span[name=stage]").text(getStage(msg.data.stage));
                    $("#infoDiv span[name=interviewResults]").text(getInterviewResults(msg.data.interviewResults));
                    $("#infoDiv span[name=position]").text(position(msg.data.position, null, null));
                    $("#infoDiv span[name=corporation]").text(msg.data.corporation);
                    $("#infoDiv span[name=channel]").text(msg.data.channel);
                    $("#infoDiv span[name=evaluate]").text(msg.data.evaluate);
                    $("#myInfoModal").modal("show");
                } else {
                    swal("消息提示", msg.error, "error");
                }
            },
            error: function () {
                swal("消息提示", "服务器异常", "error");
            }
        });
    });
    $("button[name=jlBtn]").click(function () {
        location.href = "/interview/down/" + this.value;
    });
    $("button[name=jlNoBtn]").click(function () {
        swal("消息提示", "没有找到该人员简历文件", "info");
    });
}


$(".selectInterview").bind("change",function(){
    var dataname = $(this).val();
    if (dataname==2) {
        $("#wangluomianshi").show();
        $("#yibanmianshi").hidden();
    }else{
        $("#wangluomianshi").hidden();
        $("#yibanmianshi").show();
    }
});