
//获取职位类型
function getJobType(value, option, rows) {
    var array = ["java软件开发", "Python软件开发"];
    return array[value];
}



//获取面试阶段
function getStage(value, option, rows) {
    var array = ["初试", "二面", "三面"];
    return array[value];
}

//获取
function getInterviewResults(value, option, rows) {
    var array = ["不合适", "候选人", "已发office", "面试爽约"];
    return array[value];
}

$(document).ready(function () {
    $.jgrid.defaults.styleUI = 'Bootstrap';
    // Configuration for jqGrid Example 2
    $("#table_list_2").jqGrid({
        height: 480,
        autowidth: true,    //水平自动适应
        shrinkToFit: true,  //列头自动适应
        rowNum: 20,
        rowList: [10, 20, 50, 100],
        colNames: ['序号', '题干', '答案选项', '正确答案', '题目类型', '操作'],
        colModel: [
            {
                name: 'id',
                align: "left",
                width: 60,
                key: true
            },
            {
                name: 'name',
                align: "left",
                width: 80,
                /*formatter: function (value, option, rows) {
                    var array = ["数据库", "Java基础", "Java框架"];
                    return array[value];
                }*/
            },
            {
                name: 'selectAnswer',
                align: "left",
                width: 100
            },
            {
                name: 'answer',
                width: 100,
                align: "left",
                /*formatter: function (value, option, rows) {
                    var array = ["Java", "Python"];
                    return array[value];
                }*/
            },
            {
                name: 'topicType',
                width: 150,
                align: "left",
                formatter: function (value, option, rows) {
                    var array = ["数据库", "Java基础", "Java框架"];
                    return array[value];
                }
            },
            {
                sortable: false,
                width: 155,
                formatter: function (value, option, rows) {
                    var btn = '<button name="updateBtn" class="btn btn-white btn-sm" value="' + rows.id + '" data-toggle="tooltip" data-placement="left" title="修改面试信息">修改</button>';
                    btn += '<button name="infoBtn" class="btn btn-white btn-sm" value="' + rows.id + '" data-toggle="tooltip" data-placement="left" title="面试人员详情">详情</button>';
                    return btn;
                }
            }
        ],
        multiselect: true,
        /*设置分页显示的导航条信息*/
        jsonReader: {
            root: "rows",
            page: "page",
            total: "pageSize",
            records: "total"
        },
        /*像后台请求的参数信息*/
        prmNames: {
            page: "page",
            size: "limit",
            sort: "order"
        },
        pager: "#pager_list_2",
        viewrecords: true,
        caption: "面试人员信息列表",
        hidegrid: false,
        url: "/topic/findTemplate",
        mtype: "GET",
        datatype: "json",
        gridComplete: function () {
            bind();
        }
    });

    jQuery("#table_list_2").jqGrid('navGrid', '#pager_list_2', {del: false, add: false, edit: false, search: false});

    jQuery("#table_list_2").navGrid('#pager_list_2', {
        edit: false,
        add: false,
        del: false,
        search: false
    }).navButtonAdd('#pager_list_2', {
        caption: "添加", buttonicon: "ui-icon-add", onClickButton: function () {
            $("#form_div input[name!=sex][name!=corporation]").val('');
            $("#form_div select[name=topicType] :eq(0)").prop("selected", true);
            $("#form_div select[name=jobType] :eq(0)").prop("selected", true);
            //$("#form_div select[name=stage] :eq(0)").prop("selected", true);
            //$("#form_div select[name=interviewResults] :eq(1)").prop("selected", true);
            $("#myModal").modal("show");
        },
        position: "last"
    }).navButtonAdd('#pager_list_2', {
        caption: "删除",
        buttonicon: "ui-icon-del",
        onClickButton: function () {
            var ids = $('#table_list_2').jqGrid('getGridParam', 'selarrrow');
            if (ids == '') {
                swal("消息提示", "请选择要删除的数据", "info");
                return false;
            }
            swal({
                title: "您确定要删除选中的信息吗",
                text: "删除后将无法恢复，请谨慎操作！",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "删除",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    traditional: false,
                    type: 'delete',
                    url: '/topic/' + ids,
                    dataType: 'json',
                    success: function (msg) {
                        if (msg.status == 200) {
                            $("#table_list_2").jqGrid().trigger("reloadGrid");
                            swal("删除成功！", "您已经永久删除了这些信息。", "success");
                        } else swal("消息提示", msg.error, "error");
                    },
                    error: function () {
                        swal("消息提示", "服务器异常", "error");
                    }
                });
            });
        },
        position: "last"
    });

    //新增或者修改

    //提交
    $("#addBtn").click(function () {
        //alert("ss")
        /*if (checkedInterview()) {
            return false;
        }*/
        //获得用户id，为空则是添加，否则修改
        var id = $("#id").val();
        var type = id == '' ? "POST" : "PUT";
        var url = id == '' ? "/topic" : "/topic/" + id;

        var topicType=$("select[name=topicType]").val();
        var name=$("textarea[name=name]").val();
        var selectAnswer=$("textarea[name=selectAnswer]").val();
        var answer=$("input[name=answer]").val();
        var data = {
            id:id,
            topicType:topicType,
            name: name,
            selectAnswer:selectAnswer,
            answer:answer
        };

        $.ajax({
            type: type,
            url: url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 200) {
                    $("#myModal").modal('hide');
                    $("#form_div input[name!=sex]").val('');
                    $("#table_list_2").jqGrid().trigger("reloadGrid");
                    swal("消息提示", "操作成功!", "success");
                } else {
                    swal("消息提示", msg.error, "error");
                }
            },
            error: function () {
                swal("消息提示", "服务器异常", "error");
            }
        });
    });
});


//更新提交

//绑定按钮事件
function bind() {
    $("button[name=updateBtn]").click(function () {
        $.ajax({
            type: 'get',
            url: '/topic/' + this.value,
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 200) {

                    $("#id").val(msg.data.id);

                    $("select[name=topicType] option").each(function () {
                        if ($(this).val() == msg.data.topicType) $(this).prop("selected", true);
                        else $(this).prop("selected", false);
                    });


                    $("textarea[name=name]").val(msg.data.name);

                    $("textarea[name=selectAnswer]").val(msg.data.selectAnswer);

                    /*$("select[name=answer] option").each(function () {
                        if ($(this).val() == msg.data.answer) $(this).prop("selected", true);
                        else $(this).prop("selected", false);
                    });*/

                    $("input[name=answer]").val(msg.data.answer);



                    //checkedInterview();

                    $("#myModal").modal('show');
                } else {
                    swal("消息提示", msg.error, "error");
                }
            },
            error: function () {
                swal("消息提示", "服务器异常", "error");
            }
        });
    });

    /**
     * 查看详情
     */
    $("button[name=infoBtn]").click(function () {
        $.ajax({
            type: 'get',
            url: '/topic/' + this.value,
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 200) {
                    $("#infoDiv span[name=topicType]").text(getTopicType(msg.data.topicType));
                    $("#infoDiv span[name=name]").text(msg.data.name);
                    $("#infoDiv span[name=selectAnswer]").text(msg.data.selectAnswer);
                    $("#infoDiv span[name=answer]").text(msg.data.answer);
                    $("#myInfoModal").modal("show");
                } else {
                    swal("消息提示", msg.error, "error");
                }
            },
            error: function () {
                swal("消息提示", "服务器异常", "error");
            }
        });
    });

}