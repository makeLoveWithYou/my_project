function NoteInsert() {
    $("#form_div input[name!=sex][name!=corporation]").val('');
    $("#form_div select[name=status] :eq(0)").prop("selected", true);
    $("#form_div select[name=interview] :eq(0)").prop("selected", true);
    $("#form_div select[name=stage] :eq(0)").prop("selected", true);
    $("#form_div select[name=interviewResults] :eq(1)").prop("selected", true);
    $("#myModal").modal("show");
}


function info(id) {

    $.ajax({
        type: 'get',
        url: '/interview/' + id,
        dataType: 'json',
        success: function (msg) {
            if (msg.status == 200) {
                $("#infoDiv span[name=name]").text(msg.data.name);
                $("#infoDiv span[name=phone]").text(msg.data.phone);
                $("#infoDiv span[name=age]").text(msg.data.age);
                $("#infoDiv span[name=sex]").text(msg.data.age);
                $("#infoDiv span[name=interviewTime]").text(msg.data.interviewTime);
                $("#infoDiv span[name=interview]").text(getInterview(msg.data.interview));
                $("#infoDiv span[name=status]").text(getStatus(msg.data.status));
                $("#infoDiv span[name=stage]").text(getStage(msg.data.stage));
                $("#infoDiv span[name=interviewResults]").text(getInterviewResults(msg.data.interviewResults));
                $("#infoDiv span[name=position]").text(msg.data.position);
                $("#infoDiv span[name=corporation]").text(msg.data.corporation);
                $("#infoDiv span[name=channel]").text(msg.data.channel);
                $("#infoDiv span[name=evaluate]").text(msg.data.evaluate);
                $("#myInfoModal").modal("show");
            } else {
                swal("消息提示", msg.error, "error");
            }
        },
        error: function () {
            swal("消息提示", "服务器异常", "error");
        }
    });
}

//获取面试方式
function getInterview(value, option, rows) {
    return value == 0 ? "电话面试" : "面试";
}


//获取状态
function getStatus(value, option, rows) {
    var array = ["未预约", "已预约", "拒绝", "失约", "等通知", "已结束"];
    return array[value];
}


//获取面试阶段
function getStage(value, option, rows) {
    var array = ["初试", "二面", "三面"];
    return array[value];
}

//获取
function getInterviewResults(value, option, rows) {
    var array = ["不合适", "候选人", "已发office", "面试爽约"];
    return array[value];
}
