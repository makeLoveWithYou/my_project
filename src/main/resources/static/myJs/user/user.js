$(document).ready(function () {
    //设置表标题
    var aoColumns = [
        {
            "data": function (data, type, val) {
                return "<input type='checkbox' value='" + data.id + "' name='ids'>";
            }, "sTitle": "<input type='checkbox' id='idT'>", "sWidth": "5px", "bSortable": false
        },
        {"data": "username", "sTitle": "帐户名"},
        {"data": "name", "sTitle": "用户名"},
        {"data": "phone", "sTitle": "联系电话"},
        {"data": "email", "sTitle": "电子邮箱"},
        {
            "data": "status", "sTitle": "状态", render: function (data, type, val) {
                return data == 0 ? "正常" : "禁用";
            }
        },
        {"data": "desc", "sTitle": "用户角色"},
        {"data": "createStamp", "sTitle": "创建时间"},
        {"data": "updateStamp", "sTitle": "修改时间"}
    ];
    //初始化表格
    $('.dataTables-example').dataTable({
        "bLengthChange": true,  ///是否可以修改页面显示行数
        "aLengthMenu": [10, 25, 50, 100], ///设置可选的显示行数
        "iDisplayLength": 10,  ///默认显示10行
        "bSort": true, ///是否可排序
        "bPaginate": true, ///显示使用分液器
        "bFilter": true, //搜索栏
        "aoColumns": aoColumns
    });

//加载数据
    function reloadTable() {
        $.ajax({
            type: 'get',
            url: '/user?sort=t.update_stamp,desc',
            dataType: 'json',
            success: function (msg) {
                $('.dataTables-example').dataTable().fnClearTable();
                $('.dataTables-example').dataTable().fnAddData(msg.rows.length == 0 ? null : msg.rows);
            },
            error: function () {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: '消息提示',
                    message: '服务器异常'
                });
            }
        });
    }

    reloadTable();
    loadRole();

//全选按钮
    $("#idT").click(function () {
        if (this.checked)
            $("input[name=ids]").prop("checked", true);
        else
            $("input[name=ids]").prop("checked", false);
    });

//打开添加用户界面
    $("#addBtn").click(function () {
        $(".col-md-12 input[name!=sex][name!=status]").val('');
        $("select[name=role] option").each(function () {
            if ($(this).val == -1) $(this).attr("selected", true);
            else $(this).attr("selected", false);
        });
        $("#myModal").modal('show');
    });


//打开修改用户界面
    $("#updateBtn").click(function () {
        //判断用户是否选择了多个
        if ($("input[name=ids]:checked").length == 0 || $("input[name=ids]:checked").length > 1) {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_INFO,
                title: '消息提示',
                message: '请选择一个用户信息修改！'
            });
            return false;
        }
        $(".col-md-12 input[name!=sex][name!=status]").val('');
        //加载用户、角色信息
        $.ajax({
            type: 'get',
            url: '/user/' + $("input[name=ids]:checked").val(),
            dataType: 'json',
            success: function (msg) {
                $("#userid").val(msg.data.id);
                $("input[name=username]").val(msg.data.username);
                $("input[name=name]").val(msg.data.name);
                $("input[name=phone]").val(msg.data.phone);
                $("input[name=age]").val(msg.data.age);
                $("input[name=sex]").each(function () {
                    if (this.value == msg.data.sex + '') $(this).prop("checked", true);
                });
                $("input[name=status]").each(function () {
                    if (this.value == msg.data.status) $(this).prop("checked", true);
                });
                $("input[name=password]").val(msg.data.password);
                $("input[name=email]").val(msg.data.email);
                if (msg.data.role != null) {
                    $("select[name=role] option").each(function () {
                        if ($(this).val() == msg.data.role.id) {
                            $(this).prop("selected", true);
                        } else
                            $(this).prop("selected", false);
                    });
                }
            },
            error: function () {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: '消息提示',
                    message: '服务器异常'
                });
            }
        });
        $("#myModal").modal('show');
    });

//删除用户信息
    $("#delBtn").click(function () {
        var ids = "";
        //获得选中的用户信息
        $("input[name=ids]:checked").each(function () {
            ids += $(this).val() + ',';
        });
        if (ids == "") {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_INFO,
                title: '消息提示',
                message: '请选择要删除的用户！'
            });
            return false;
        }
        BootstrapDialog.confirm({
            title: '消息提示',
            message: '确定要删除选中的用户信息吗？',
            type: BootstrapDialog.TYPE_WARNING,
            closable: true,
            draggable: true,
            btnCancelLabel: 'Cancel',
            btnOKLabel: 'OK',
            btnOKClass: 'btn-warning',
            callback: function (result) {
                if (result) {
                    ids = ids.substring(0, ids.length - 1);
                    //执行删除请求
                    $.ajax({
                        traditional: false,
                        type: 'delete',
                        url: '/user/dels/' + ids,
                        dataType: 'json',
                        success: function (msg) {
                            reloadTable();
                        },
                        error: function () {
                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_DANGER,
                                title: '消息提示',
                                message: '服务器异常'
                            });
                        }
                    });
                }
            }
        });
    });

//表单验证
    function check() {
        if ($("input[name=username]").val().trim() == "") {
            return false;
        }
        if ($("input[name=name]").val().trim() == "") {
            return false;
        }
        if ($("input[name=phone]").val().trim() == "") {
            return false;
        }
        if ($("input[name=password]").val().trim() == "") {
            return false;
        }
        if ($("input[name=email]").val().trim() == "") {
            return false;
        }
        if ($("select[name=role]").val().trim() == -1) {
            return false;
        }
        return true;
    }

//用户新增修改方法
    $("#subBtn").click(function () {
        if (!check()) {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_WARNING,
                title: '消息提示',
                message: '请将必填信息填完'
            });
            return false;
        }
        //获得用户id，为空则是添加，否则修改
        var userid = $("#userid").val();
        var type = userid == '' ? "POST" : "PUT";
        var url = userid == '' ? "/user" : "/user/" + userid;
        var data = {
            username: $("input[name=username]").val(),
            age: $("input[name=age]").val(),
            name: $("input[name=name]").val(),
            phone: $("input[name=phone]").val(),
            sex: $("input[name=sex]:checked").val(),
            roleId: $("select[name=role]").val(),
            password: $("input[name=password]").val(),
            email: $("input[name=email]").val(),
            status: $("input[name=status]:checked").val()
        };
        $.ajax({
            type: type,
            url: url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (msg) {
                reloadTable();
                $("#myModal").modal('hide');
                $(".col-md-12 input[name!=sex][name!=status]").val('');
                $("select[name=role] option").each(function () {
                    if ($(this).val == -1) $(this).attr("selected", true);
                    else $(this).attr("selected", false);
                });
            },
            error: function () {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_DANGER,
                    title: '消息提示',
                    message: '服务器异常'
                });
            }
        });
    });

})
;