function loadRole() {
    //加载角色信息
    $.ajax({
        type: 'get',
        url: '/role?size=1000',
        dataType: 'json',
        success: function (msg) {
            var html = "<option value='-1'>请选择角色</option>";
            for (var i = 0; i < msg.rows.length; i++) {
                html += "<option value='" + msg.rows[i].id + "'>" + msg.rows[i].name + "</option>";
            }
            $("select[name=role]").html(html);
        },
        error: function () {
            alert('error');
        }
    });
}

function IsPC() {
    var userAgentInfo = navigator.userAgent;
    var Agents = ["Android", "iPhone",
        "SymbianOS", "Windows Phone",
        "iPad", "iPod"];
    var flag = true;
    for (var v = 0; v < Agents.length; v++) {
        if (userAgentInfo.indexOf(Agents[v]) > 0) {
            flag = false;
            break;
        }
    }
    return flag;
}

//获取题目类型
function getTopicType(value, option, rows) {
    var array = ["数据库", "Java基础", "Java框架", "算法初级"];
    return array[value];
}

//加载父权限
function reloadParent() {
    $.ajax({
        type: 'get',
        url: '/privileges?size=100&parentId=0',
        dataType: 'json',
        success: function (msg) {
            var html = "<option selected value='0'>默认父权限</option>";
            for (var i = 0; i < msg.rows.length; i++) {
                html += "<option value='" + msg.rows[i].id + "'>" + msg.rows[i].name + "</option>";
            }
            $("select[name=parent]").html(html);
        },
        error: function () {
            BootstrapDialog.show({
                type: BootstrapDialog.TYPE_DANGER,
                title: '消息提示',
                message: '服务器异常'
            });
        }
    });
}

//校验
function checkedInterview() {
    var bol = false;
    //不能为空校验
    $("input[req=true]").each(function () {
        if ($(this).val() == "") {
            $(this).next().css("color", "red");
            $(this).next().html("该选项不能为空!");
            bol = true;
        } else {
            $(this).next().html("必填");
            $(this).next().css("color", "");
        }
    });
    if ($("input[name=phone]").val() == "") {
        $("input[name=phone]").next().html("手机号不能为空!");
        $("input[name=phone]").next().css("color", "red");
        bol = true;
    } else if (isNaN($("input[name=phone]").val())) {
        $("input[name=phone]").next().html("请填写正确的手机号!");
        $("input[name=phone]").next().css("color", "red");
        bol = true;
    } else if ($("input[name=phone]").val().length != 11) {
        $("input[name=phone]").next().html("手机号长度为11位数!");
        $("input[name=phone]").next().css("color", "red");
        bol = true;
    } else {
        $("input[name=phone]").next().html("必填");
        $("input[name=phone]").next().css("color", "");
    }
    //验证职位
    if ($("select[name=position]").val() == "") {
        $("select[name=position]").next().html("请选择职位");
        $("select[name=position]").next().css("color", "red");
        bol = true;
    } else {
        $("select[name=position]").next().html("必填");
        $("select[name=position]").next().css("color", "");
    }


    if ($("select[name=status]").val() == 1) {
        if ($("input[name=interviewTime]").val() == "") {
            $("input[name=interviewTime]").next().css("color", "red");
            $("input[name=interviewTime]").next().html("已预约请选择预约时间!");
            bol = true;
        } else {
            $("input[name=interviewTime]").next().html("必填");
            $("input[name=interviewTime]").next().css("color", "");
        }
    } else {
        $("input[name=interviewTime]").next().html("&nbsp;");
        $("input[name=interviewTime]").next().css("color", "");
    }
    return bol;
}