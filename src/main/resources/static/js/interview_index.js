var HH = 0;//时
var mm = 0;//分
var ss = 0;//秒
var timeState = false;//时间状态 默认为false
var questions = QuestionJosn;
var itemList = ["A", "B", "C", "D", "E", "F"]
var activeQuestion = 0; //当前操作的考题编号
var questioned = 0; //
var checkQues = [1, 3, 4, 5, 4]; //已做答的题的集合
// /*选中考题*/
var Question;
var timeout = 10;
var sumTime = 30;
var countTime = 0;
var currentTopicId;
var currentAnswer = "";
var currentAnswerCount = 0;

function clickTrim(source) {
    currentAnswer = '';
    ++currentAnswerCount;
    var id = source.id;
    $("#" + id).find("input").prop("checked", !$("#" + id).find("input").prop("checked"));
    $("#" + id).addClass("clickTrim");
    $("input[type=checkbox][va=value_]:checked").each(function () {
        currentAnswer += $(this).val() + ",";
    });
    currentAnswer = currentAnswer.substring(0, currentAnswer.length - 1);
    $("#ques" + activeQuestion).removeClass("question_id").addClass("clickQue");
    var ques = 0;
    for (var i = 0; i < checkQues.length; i++) {
        if (checkQues[i].id == activeQuestion && checkQues[i].item != id) {
            ques = checkQues[i].id;
            checkQues[i].item = id;//获取当前考题的选项ID
            checkQues[i].answer = $("#" + id).find("input[va=value_]:checked").val();//获取当前考题的选项值
        }
    }
    if (checkQues.length == 0 || Question != activeQuestion && activeQuestion != ques) {
        var check = {};
        check.id = activeQuestion;//获取当前考题的编号
        check.item = id;//获取当前考题的选项ID
        check.answer = $("#" + id).find("input[va=value_]:checked").val();//获取当前考题的选项值
        checkQues.push(check);
    }
    $(".question_info").each(function () {
        var otherId = $(this).attr("id");
        if (otherId != id) {
            // $("#" + otherId).find("input").prop("checked", false);
            $("#" + otherId).removeClass("clickTrim");
        }
    });
    Question = activeQuestion;
}

/*实现计时器*/
var time = setInterval(function () {
    if (timeState) {
        timeout = timeout - 1;
        if (timeout <= 0) {
            timeState = false;
            $("#tsSpan").css("display", "inline");
            $.get("/topic/interview?timeout=0", function (data) {
            });
        }
        $("#time_").text(timeout);
        loadColor();
    }
}, 1000);

function loadColor() {
    if (timeout >= 7) {
        $("#time_").css("color", "green");
    } else if (timeout >= 3) {
        $("#time_").css("color", "blue");
    } else if (timeout >= 0) {
        $("#time_").css("color", "red");
    }
}

//展示考卷信息
function showQuestion(id) {
    $(".questioned").text(id + 1);
    // questioned = (id + 1) / questions.length
    if (activeQuestion != undefined) {
        $("#ques" + activeQuestion).removeClass("question_id").addClass("active_question_id");
    }
    activeQuestion = id;
    $(".question").find(".question_info").remove();
    var question = questions[0];
    $(".question_title").html("<strong>第 " + (id + 1) + " 题 、</strong>" + question.name);
    currentTopicId = question.topicId;
    var items = question.selectAnswer.split(";");
    var item = "";
    for (var i = 0; i < items.length; i++) {
        item = "<li class='question_info' onclick='clickTrim(this)' id='item"
            + i + "'><input type='checkbox' va='value_' value='" + itemList[i] + "'>&nbsp;" + itemList[i] + "." + items[i] + "</li>";
        $(".question").append(item);
    }
    $(".question").attr("id", "question" + id);
    $("#ques" + id).removeClass("active_question_id").addClass("question_id");
    for (var i = 0; i < checkQues.length; i++) {
        if (checkQues[i].id == id) {
            $("#" + checkQues[i].item).find("input").prop("checked", "checked");
            $("#" + checkQues[i].item).addClass("clickTrim");
            $("#ques" + activeQuestion).removeClass("question_id").addClass("clickQue");
        }
    }
    // progress(1);
}

/*答题卡*/
function answerCard(total) {
    $(".question_sum").text(total);
    for (var i = 0; i < questions.length; i++) {
        var questionId = "<li id='ques" + i + "'onclick='saveQuestionState(" + i + ")' class='questionId'>" + (i + 1) + "</li>";
        $("#answerCard ul").append(questionId);
    }
}


/*设置进度条*/
function progress(length, total) {
    var prog = (length / total);
    var pro = $(".progress").parent().width() * prog;
    $(".progres").text((prog * 100).toString().substr(0, 5) + "%")
    $(".progress").animate({
        width: pro,
        opacity: 0.5
    }, 1000);
}

/*保存考题状态 已做答的状态*/
function saveQuestionState(clickId) {
    showQuestion(clickId)
}

var page = 0;
var size = 1;
var total = 0;
$(function () {
    //加载
    function loadTopic() {
        //请求题目
        $.ajax({
            type: "GET",
            url: "/topic/interview?page=" + page + "&size=" + size + "&timeout=" + timeout,
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 202) {
                    topend();
                    load(msg);
                } else if (msg.status == 200) {
                    load(msg);
                } else {
                    alert(msg.error);
                }
            },
            error: function () {
                alert("服务器异常");
            }
        });
    }

    //load
    function load(msg) {
        $("#tsSpan").css('display', 'none');//隐藏提示

        activeQuestion = msg.page;
        questions = msg.rows;
        progress(msg.page + 1, msg.total);//设置进度条
        answerCard(msg.total);//加载答题卡
        showQuestion(msg.page);//展示考卷信息
        total = msg.total;
        sumTime = msg.data.sumtime;
        timeout = msg.data.timeout;
        loadColor();
        countTime = msg.data.counttime;
        if (timeout > 0) timeState = true;
        page = msg.page;
        currentAnswerCount = msg.data.currTopicCount == null ? 0 : msg.data.currTopicCount;
        $("#sumTime_").text(sumTime);
        $("#time_").text(timeout);
        $("#countTime_").text(countTime);
        if ((page + 1) == total) {
            $("#nextQuestion").attr("disabled", true);
        }
        if (msg.data.status) {
            $("#submitQuestions").attr("disabled", true);
            statistics();
            topend();
            $("#tsSpan").text('已交卷！');
        }
        if (sumTime >= countTime) {//判断
            topend();
        }
    }

    //页面刷洗执行加载方法
    loadTopic();

    $(".middle-top-left").width($(".middle-top").width() - $(".middle-top-right").width())
    $(".progress-left").width($(".middle-top-left").width() - 200);

    //提交试卷
    $("#submitQuestions").click(function () {
        if (((page + 1) >= total) || (sumTime >= countTime)) {
            if (confirm("确定要提交了吗？")) {
                subAnswer(true);
            }
        } else
            alert("题目未答完");
        // alert("已做答:" + (currentAnswerCount) + "道题,还有" + (total - currentAnswerCount) + "道题未完成");
    });

    //进入下一题
    $("#nextQuestion").click(function () {
        page++;
        if ((page + 1) >= total) {
            $("#nextQuestion").attr("disabled", true);
        }
        subAnswer(false);
        /*if (currentAnswer != "") {
            subAnswer(false);
        } else {
            //加载题目
            loadTopic();
        }*/
    });

    //提交答案到后台
    function subAnswer(bol) {
        $.ajax({
            type: "PUT",
            url: "/topic/interview",
            data: {
                answer: currentAnswer,
                topicId: currentTopicId,
                last: bol
            },
            dataType: 'json',
            success: function (msg) {
                currentAnswer = "";
                if (msg.status == 200) {
                    if (bol) {
                        //统计数据
                        statistics();
                        topend();
                        $("#tsSpan").text('已交卷！');
                        $("#submitQuestions").attr("disabled", true);
                    } else {
                        //加载题目
                        loadTopic();
                    }
                } else
                    alert(msg.error);
            },
            error: function () {
                alert("服务器异常");
            }
        });
    }

    //访问后台统计数据面试人员的答题情况
    function statistics() {
        //统计数据
        $.ajax({
            type: "GET",
            url: "/topic/interview/statistics",
            dataType: 'json',
            success: function (msg) {
                if (msg.status == 200) {
                    timeState = false;
                    $("#answerCount_").text(msg.data.answerCount);
                    $("#correctAnswerCount_").text((msg.data.correctAnswerCount / msg.data.total) * 100);
                    // $("#noAnswerCount_").text(msg.data.total - msg.data.answerCount);
                    $("#timeout_").text(msg.data.timeout);
                } else
                    alert(msg.error);
            },
            error: function () {
                alert("服务器异常");
            }
        });
    }

    //结束题目,改变css样式
    function topend() {
        $("#nextQuestion").attr("disabled", true);
        $("#tsSpan").text('做题时间已结束！');
        $("#tsSpan").css('display', 'inline');
        $("#tsSpan").css('color', 'red');
    }
});