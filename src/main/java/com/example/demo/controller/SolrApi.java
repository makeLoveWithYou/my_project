package com.example.demo.controller;

import com.example.demo.dto.ResultDto;
import com.example.demo.dto.UserDto;
import com.example.demo.form.UserForm;
import com.example.demo.service.SolrService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 用户
 * @date 2018/5/31 10:28
 */
@Api(value = "Solr操作入口", description = "Solr操作入口", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RestController
@RequestMapping("/solr")
public class SolrApi {

    @Autowired()
    private SolrService solrService;

    @ApiOperation(value = "通过Solr查询信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "编号", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "username", value = "用户名（可分词匹配处理）", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "email", value = "邮箱", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "当前页数", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序，如：?sort=id或者?sort=id,desc", dataType = "String", paramType = "query")
    })
    @RequestMapping(method = RequestMethod.GET)
    public ResultDto findIndex(Pageable pageable, @RequestParam(value = "sort", required = false) String sort, @RequestParam(value = "id",
            required = false) Long id, @RequestParam(value = "username", required = false) String username,
                               @RequestParam(value = "email", required = false) String email) {
        StringBuffer q = new StringBuffer();
        if (!StringUtils.isEmpty(id)) q.append(" AND id:" + id);
        if (!StringUtils.isEmpty(username)) q.append(" AND username:" + username);
        if (!StringUtils.isEmpty(email)) q.append(" AND email:" + email);
        return solrService.queryTwoService((q.length() < 1) ? "*:*" : q.substring(5, q.length()), "id,username,email,password,age,desc", pageable, sort, UserDto.class);
    }

    @ApiOperation(value = "创建、更新Solr索引")
    @PostMapping
    public ResultDto createIndex(@RequestBody UserForm userForm) {
        ArrayList<UserDto> users = new ArrayList<>(1);
        users.add(new UserDto(userForm.getId(), userForm.getUsername(), userForm.getEmail(), userForm.getPassword(), userForm.getAge(), null));
        return solrService.createIndexOneService(users);
    }

    @ApiOperation(value = "删除Solr索引")
    @DeleteMapping
    @ApiImplicitParam(name = "params", value = "删除条件如：id:1", required = true, dataType = "string", paramType = "path")
    public ResultDto deleteIndex(String params) {
        return solrService.deleteIndexService(params);
    }

    @ApiOperation(value = "根据id批量删除索引")
    @DeleteMapping(value = "/dels")
    @ApiImplicitParam(name = "ids", value = "根据id批量删除索引", allowMultiple = true, required = true, dataType = "array", paramType = "query")
    public ResultDto deleteIndex(@RequestParam(value = "ids") List<String>  ids) {
        return solrService.deleteIndexService(ids);
    }
}
