package com.example.demo.controller;

import com.example.demo.dto.ResultDto;
import com.example.demo.pojo.Topic;
import com.example.demo.service.TopicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@Api(value = "面试题库操作入口", description = "面试题模板基本信息操作API", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping("/topic")
public class TopicController {
    @Autowired
    TopicService topicService;

    @GetMapping("/page")
    @RequiresPermissions({"60001"})
    public String page() {
        return "topic";
    }


    @ApiOperation("面试题模板查询")
    @GetMapping("/findTemplate")
    @ResponseBody
    @RequiresPermissions({"50006"})
    public ResultDto find(Topic topic, @RequestParam(value = "page", required = false, defaultValue = "1") Integer page, @RequestParam(value = "limit", required = false, defaultValue = "20") Integer limit) {
        PageRequest pageable = new PageRequest(page > 0 ? page - 1 : page, limit);
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            Page<Topic> pages = topicService.find(topic, pageable);
            dto.setTotal((int) pages.getTotalElements());
            int pagesize = (int) Math.ceil((pages.getTotalElements() * 1.0) / pageable.getPageSize());
            dto.setPage(page);
            dto.setPageSize(pagesize < 1 ? 1 : pagesize);
            dto.setRows(pages.getContent());
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }

    @ApiOperation("面试题模板新增")
    @PostMapping
    @RequiresPermissions({"50002"})
    @ResponseBody
    public ResultDto save(@RequestBody Topic topic) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            dto.setData(topicService.save(topic));
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }

    @ApiOperation("面试题模板修改")
    @PutMapping("{id}")
    @RequiresPermissions({"50003"})
    @ResponseBody
    public ResultDto update(@PathVariable("id") Long id, @RequestBody Topic topic) {
        ResultDto dto = new ResultDto();

        dto.errorStatus();
        try {
            if (topicService.update(topic))
                dto.successStatus();
            else dto.setError("修改失败");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }


    @ApiOperation("面试题模板删除")
    @DeleteMapping("/{id}")
    @RequiresPermissions({"50004"})
    @ResponseBody
    public ResultDto delete(@PathVariable("id") String id) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            String ids[] = id.split(",");
            for (int i = 0; i < ids.length; i++) {
                topicService.delete(Long.valueOf(ids[i]));
            }
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }


    @ApiOperation("面试题模板详情")
    @GetMapping("{id}")
    @RequiresPermissions({"50005"})
    @ResponseBody
    public ResultDto get(@PathVariable("id") Long id) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        Topic template = null;
        try {
            template = new Topic();
            Topic templats = topicService.get(id);
            BeanUtils.copyProperties(templats, template);
            //template.setId(templats.getId());
            dto.setData(template);
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }
}
