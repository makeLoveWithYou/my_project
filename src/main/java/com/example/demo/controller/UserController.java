package com.example.demo.controller;

import com.example.demo.common.CodeInfo;
import com.example.demo.common.MapKeys;
import com.example.demo.dto.ResultDto;
import com.example.demo.dto.UserDto;
import com.example.demo.form.UserForm;
import com.example.demo.pojo.Log;
import com.example.demo.pojo.User;
import com.example.demo.service.LogService;
import com.example.demo.service.UserService;
import com.example.demo.util.HttpUtil;
import com.example.demo.util.ThreadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 账户控制层
 * @date 2018/5/24 14:53
 */
/*
swagger通过注解表明该接口会生成文档，包括接口名、请求方法、参数、返回信息的等等。
    @Api：修饰整个类，描述Controller的作用
    @ApiOperation：描述一个类的一个方法，或者说一个接口
    @ApiParam：单个参数描述
    @ApiModel：用对象来接收参数
    @ApiProperty：用对象接收参数时，描述对象的一个字段
    @ApiResponse：HTTP响应其中1个描述
    @ApiResponses：HTTP响应整体描述
    @ApiIgnore：使用该注解忽略这个API
    @ApiError ：发生错误返回的信息
    @ApiImplicitParam：一个请求参数
    @ApiImplicitParams：多个请求参数
* */
@Controller
@RequestMapping("/user")
@Api(value = "账户操作入口", description = "账户基本信息操作API", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private LogService logService;


    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * TODO 对象关联查询方式
     * 查询账户信息
     *
     * @param username
     * @param email
     * @param pageable
     * @return
     */
    /*@ApiOperation(value = "对象关联查询账户方式", notes = "可根据账户名、邮箱，进行条件查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "账户名", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "email", value = "邮箱", required = false, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "当前页数", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序，如：?sort=id或者?sort=id,desc", required = false, dataType = "String", paramType = "query")
    })
    @RequestMapping(method = RequestMethod.GET)
    public Map<String, Object> findUserOne(@RequestParam(name = "username", required = false) String username, @RequestParam(name = "email", required = false) String email, Pageable pageable) {
        Page<User> pages = userService.findUserService(username, email, pageable);
        Map<String, Object> map = new Hashtable<>();
        map.put(MapKeys.TOTAL, pages.getTotalElements());
        map.put(MapKeys.ROWS, pages.getContent());
        return map;
    }*/


    /**
     * TODO 自定义SQL查询方式
     * 查询账户信息
     *
     * @param pageable
     * @return
     */
    @ApiOperation(value = "自定义SQL查询账户方式", notes = "可根据账户名、邮箱，进行条件查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页数", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", required = false, dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序，如：?sort=id或者?sort=id,desc", required = false, dataType = "String", paramType = "query")
    })
    @GetMapping
    @RequiresPermissions({"10005"})
    public @ResponseBody
    ResultDto findUserTwo(UserForm form, @RequestParam(name = "sort", required = false) String sort, Pageable pageable) {
        ResultDto result = null;
        try {
            //截取排序字符串如：id,desc 替换为 id desc
            if (!StringUtils.isEmpty(sort) && sort.indexOf(",") != -1)
                sort = sort.replace(',', ' ');
            User user = new User();
            BeanUtils.copyProperties(form, user);
            result = userService.findUserServiceTwo(user, ((User) SecurityUtils.getSubject().getPrincipal()).getId(), sort, pageable);
            result.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询账户信息错误");
            if (StringUtils.isEmpty(result)) result = new ResultDto();
            result.errorStatus();
        }
        return result;
    }

    /**
     * 账户页面
     *
     * @return
     */
    @RequiresPermissions({"10007"})
    @ApiOperation(value = "跳转账户页面")
    @GetMapping(value = "/page")
    public String userPage() {
        return "user";
    }

    /**
     * 账户详情
     *
     * @return
     */
    @ApiOperation(value = "获取账户详情信息")
    @ApiImplicitParam(name = "id", value = "账户编号", required = true, dataType = "long", paramType = "path")
    @GetMapping(value = "/{id}")
    @RequiresPermissions({"10006"})
    public @ResponseBody
    ResultDto getUser(@PathVariable("id") Long id) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            result.setData(this.userService.getUserService(id));
            result.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("获取账户详情失败");
        }
        return result;
    }


    /**
     * 根据id删除账户信息
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除账户信息")
    @ApiImplicitParam(name = "id", value = "账户编号", required = true, dataType = "long", paramType = "path")
    @DeleteMapping(value = "/{id}")
    @RequiresPermissions({"10002"})
    public @ResponseBody
    ResultDto deleteUser(@PathVariable Long id, HttpServletRequest request) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            userService.deleteUser(id);
            result.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "删除了ID为" + id + "的账户", MapKeys.LOG_2, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除账户信息错误");
        }
        return result;
    }

    /**
     * 批量删除账户信息
     *
     * @param idStr
     * @return
     */
    @ApiOperation(value = "删除账户信息")
    @ApiImplicitParam(name = "ids", value = "账户编号(格式：1,2,3,4)", required = true, dataType = "string", paramType = "path")
    @DeleteMapping(value = "/dels/{ids}")
    @RequiresPermissions({"10008"})
    public @ResponseBody
    ResultDto deleteUser(@PathVariable("ids") String idStr, HttpServletRequest request) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            if (idStr != null) {
                String[] ids = idStr.split(",");
                for (int i = 0; i < ids.length; i++) userService.deleteUser(Long.valueOf(ids[i]));
            }
            result.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "删除了ID为" + idStr + "的账户", MapKeys.LOG_2, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除账户信息错误");
        }
        return result;
    }


    @ApiOperation(value = "/添加账户信息")
    @RequestMapping(method = RequestMethod.POST)
    @RequiresPermissions({"10004"})
    public @ResponseBody
    ResultDto saveUser(@RequestBody UserForm userForm, HttpServletRequest request) {
        ResultDto resultDto = new ResultDto();
        resultDto.errorStatus();
        try {
            User user_new = new User();
            BeanUtils.copyProperties(userForm, user_new);
            user_new = userService.saveUserService(user_new, userForm.getRoleId());
            Long id = user_new.getId();
            resultDto.setData(user_new);
            resultDto.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "添加了ID为：" + id + "的账户", MapKeys.LOG_2, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("新增账户信息错误");
        }
        return resultDto;
    }

    @ApiOperation(value = "修改账户信息")
    @PutMapping(value = "/{id}")
    @ApiImplicitParam(name = "id", value = "账户编号", paramType = "path", dataType = "long")
    @RequiresPermissions({"10003"})
    public @ResponseBody
    ResultDto updateUser(@PathVariable(name = "id") Long userid, @RequestBody UserForm userForm, HttpServletRequest request) {
        ResultDto resultDto = new ResultDto();
        resultDto.errorStatus();
        try {
            userForm.setId(userid);
            User user_update = new User();
            BeanUtils.copyProperties(userForm, user_update);
            if (userService.updateUserService(user_update, userForm.getRoleId())) resultDto.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "修改了ID为：" + userid + "的账户", MapKeys.LOG_2, HttpUtil.getIpAdrress(request), user.getId())));

        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改账户信息错误");
        }
        return resultDto;
    }

    /**
     * @Description: 账户信息导出excel
     * @author ShengGuang.Ye
     * @date 2018/5/28 15:55
     * @version V1.0
     */
    @ApiOperation(value = "导出账户信息")
    @GetMapping(value = "/download")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页数", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "size", value = "总数据量", paramType = "query", dataType = "long")
    })
    @RequiresPermissions({"10001"})
    public void downLoad(HttpServletResponse response, Pageable pageable,HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ServletOutputStream out = null;
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        try {
            // 设置response参数，可以打开下载页面
            response.reset();
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            response.setHeader("Content-Disposition", "attachment;filename=" + new String(("userInfo.xlsx").getBytes(), "iso-8859-1"));
            //获取账户信息
            ResultDto result = userService.findUserServiceTwo(new User(), ((User) SecurityUtils.getSubject().getPrincipal()).getId(), null, pageable);
            if (result != null && result.getRows() != null) {
                List<UserDto> users = (List<UserDto>) result.getRows();
                //TODO 生成表格
                @SuppressWarnings("resource")
                XSSFWorkbook xssfWorkbook = new XSSFWorkbook();
                XSSFSheet sheet = xssfWorkbook.createSheet();
                // 设置格式
                sheet.setColumnWidth(0, 252 * 7 + 323);
                sheet.setColumnWidth(1, 252 * 11 + 323);
                sheet.setColumnWidth(3, 252 * 21 + 323);
                sheet.setColumnWidth(4, 252 * 50 + 323);
                //设置标题
                XSSFRow xssfRow = sheet.createRow(0);
                xssfRow.createCell(0).setCellValue("编号");
                xssfRow.createCell(1).setCellValue("姓名");
                xssfRow.createCell(2).setCellValue("年龄");
                xssfRow.createCell(3).setCellValue("邮箱");
                xssfRow.createCell(4).setCellValue("拥有的角色");
                xssfRow.createCell(5).setCellValue("创建时间");
                xssfRow.createCell(6).setCellValue("修改时间");
                for (int i = 0; i < users.size(); i++) {
                    xssfRow = sheet.createRow(i + 1);
                    xssfRow.createCell(0).setCellValue(users.get(i).getId().toString());
                    xssfRow.createCell(1).setCellValue(StringUtils.isEmpty(users.get(i).getUsername()) ? "" : users.get(i).getUsername());
                    xssfRow.createCell(2).setCellValue(StringUtils.isEmpty(users.get(i).getAge()) ? "" : users.get(i).getAge().toString());
                    xssfRow.createCell(3).setCellValue(StringUtils.isEmpty(users.get(i).getEmail()) ? "" : users.get(i).getEmail());
                    xssfRow.createCell(4).setCellValue(StringUtils.isEmpty(users.get(i).getDesc()) ? "" : users.get(i).getDesc());
                    xssfRow.createCell(5).setCellValue(StringUtils.isEmpty(users.get(i).getCreateStamp()) ? "" : users.get(i).getCreateStamp().toLocaleString());
                    xssfRow.createCell(6).setCellValue(StringUtils.isEmpty(users.get(i).getUpdateStamp()) ? "" : users.get(i).getUpdateStamp().toLocaleString());
                }
                xssfWorkbook.write(os);
            }
            byte[] content = os.toByteArray();
            InputStream is = new ByteArrayInputStream(content);
            out = response.getOutputStream();
            bis = new BufferedInputStream(is);
            bos = new BufferedOutputStream(out);
            byte[] buff = new byte[2048];
            int bytesRead;
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
            dto.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "将用户信息导出文件", MapKeys.LOG_2, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) bis.close();
                if (bos != null) bos.close();
                if (out != null) out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 校验账户编号是否正确
     *
     * @param userid
     * @param result
     * @return true：id不正确
     */
    private boolean checkId(Long userid, ResultDto result) {
        if (StringUtils.isEmpty(this.userService.getUserService(userid))) {
            result.setError("账户ID存在");
            result.setStatus(CodeInfo.CODE_400);
            return false;
        }
        return true;
    }
}
