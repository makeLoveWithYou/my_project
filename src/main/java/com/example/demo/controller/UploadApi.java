package com.example.demo.controller;

import com.example.demo.common.MapKeys;
import com.example.demo.dto.ResultDto;
import com.example.demo.pojo.Log;
import com.example.demo.pojo.User;
import com.example.demo.service.LogService;
import com.example.demo.util.HttpUtil;
import com.example.demo.util.ThreadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/upload")
@Api(value = "文件上传操作入口", description = "文件上传操作入口API", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@PropertySource("classpath:config/path.properties")// 用来指定配置文件的位置
public class UploadApi {

    @Value("${upload}")
    private String upload;

    @Autowired
    private LogService logService;

    /**
     * 文件上传
     *
     * @param files
     * @return
     */
    @ApiOperation(value = "文件上传")
    @ApiImplicitParam(value = "上传的文件", name = "files", paramType = "form", dataType = "file")
    @PostMapping(headers = "content-type=multipart/form-data")
    public ResultDto upload(MultipartFile[] files, HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        if (files != null && files.length > 0) {
            String filename = null;
            // 生成文件名并保存到本地
            for (int i = 0; i < files.length; i++) {
                filename = files[i].getOriginalFilename();
                filename = filename.substring(0, filename.lastIndexOf('.')) + '_' + System.currentTimeMillis() +
                        filename.substring(filename.lastIndexOf('.'), filename.length());
                File filepath = new File(upload);
                if (!filepath.exists()) filepath.mkdirs();
                try {
                    files[i].transferTo(new File(upload + filename));
                    dto.successStatus();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            dto.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            String finalFilename = filename;
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "上传了文件：" + finalFilename, MapKeys.LOG_5, HttpUtil.getIpAdrress(request), user.getId())));
        } else {
            dto.setError("未收到文件参数");
        }
        return dto;
    }

}
