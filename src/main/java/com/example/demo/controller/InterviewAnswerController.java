package com.example.demo.controller;

import com.example.demo.common.CodeInfo;
import com.example.demo.common.MapKeys;
import com.example.demo.dto.InterviewDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.pojo.Interview;
import com.example.demo.pojo.InterviewAnswer;
import com.example.demo.service.InterviewAnswerService;
import com.example.demo.service.InterviewService;
import com.example.demo.service.TopicService;
import com.example.demo.util.HttpUtil;
import com.example.demo.util.MyException;
import com.example.demo.util.SessionUtil;
import com.example.demo.util.TimeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Hashtable;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试
 * @date 2018/6/20 11:48
 */
@Controller
@RequestMapping("/topic/interview")
@PropertySource("classpath:config/config.properties")// 用来指定配置文件的位置
@Api(value = "面试人员答题操作入口", description = "面试人员信息答题基本信息操作API", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class InterviewAnswerController {

    @Autowired
    private InterviewAnswerService interviewAnswerService;

    @Autowired
    private InterviewService interviewService;

    @Autowired
    private TopicService topicService;

    @Value("${timeout}")
    private long timeout;

    @Value("${counttime}")
    private long counttime;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @ApiOperation("设置每题超时时间")
    @RequiresPermissions({"60001"})
    @GetMapping("/timeout/{timeout}")
    @ResponseBody
    public synchronized String setTimeOut(@PathVariable("timeout") Long timeout) {
        this.timeout = timeout;
        return "{\"status\":20}";
    }

    @ApiOperation("设置答题面试时间")
    @RequiresPermissions({"60001"})
    @GetMapping("/counttime/{counttime}")
    @ResponseBody
    public synchronized String setTimeOut2(@PathVariable("counttime") Long counttime) {
        this.counttime = counttime;
        return "{\"status\":200}";
    }

    @ApiOperation(value = "面试题查询")
    @GetMapping
    @ApiImplicitParams({
            @ApiImplicitParam(value = "当前页数", name = "page", defaultValue = "0", paramType = "query", dataType = "int"),
            @ApiImplicitParam(value = "显示数量", name = "size", defaultValue = "1", paramType = "query", dataType = "int"),
            @ApiImplicitParam(value = "职位类型", name = "jobType", paramType = "query", dataType = "int"),
            @ApiImplicitParam(value = "面试人员编号", name = "interviewId", paramType = "query", dataType = "int")
    })
    @ResponseBody
    public ResultDto findInterview(Pageable pageable, @RequestParam(value = "bol", required = false, defaultValue = "false") boolean bol, HttpServletRequest request) {
        //初始化实例
        Date date = new Date();
        Hashtable<String, Object> myMap = new Hashtable<>();
        ResultDto result = new ResultDto();
        try {
            // 获取当前登陆用户
            Interview interview = (Interview) SessionUtil.getUser(request.getSession());
            //判断当前答题时间是否还在答题时间范围，不再直接返回答题结束
            if (date.getTime() >= interview.getEndTime().getTime()) {
                throw new MyException("已超出了答题时间范围，答题已结束");
            }
            //查询面试题
            PageRequest newPageable = new PageRequest(interview.getPage(), pageable.getPageSize());
            result = interviewAnswerService.find(new InterviewAnswer(interview.getId()), newPageable, bol);
            //获得初始化时间
            Long initTime = (Long) SessionUtil.getInitTime(request.getSession());
            // 如果刷新当前页面，则继续返回剩余的时间，否则重新返回每题超时时间
            if (pageable.getPageNumber() == 0 && SessionUtil.getLastTime(request.getSession()) != null) {
                //获得上一次答题时间,上一次答题开始时间如：14:00:00,加上15秒:14:00:15
                Long lastTime = ((Long) SessionUtil.getLastTime(request.getSession())) + timeout;
                //当前时间减去用去的时间如果大于则已用完，否则还有时间继续答题
                myMap.put(MapKeys.TIMEOUT, (date.getTime() > lastTime) ? 0 : (lastTime - date.getTime()) / 1000);
            } else {
                myMap.put(MapKeys.TIMEOUT, ((this.timeout / 1000) - 1));
                SessionUtil.setLastTime(date.getTime(), request.getSession());
            }
            SessionUtil.setUser(interview, request.getSession());
            myMap.put(MapKeys.SUMTIME, ((date.getTime() - initTime) / 1000 / 60));//存储用户已使用的时间
            myMap.put(MapKeys.COUNTTIME, counttime);//用户总共答题时间
            result.setPage(interview.getPage());
            result.setStatus((((date.getTime() - initTime) / 1000 / 60) > 30) ? CodeInfo.CODE_202 : CodeInfo.CODE_200);
            // 判断题目是否已完成，完成给前端一个状态，保证用户每次刷新前台都能拿到这个状态
            myMap.put(MapKeys.STATUS, (SessionUtil.getLast(request.getSession()) != null) ? true : false);
            result.setData(myMap);
        } catch (MyException e) {//答题超出时间范围
            result.setStatus(CodeInfo.CODE_202);
            result.setError(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            result.errorStatus();
            log.error("查询面试题错误");
        }
        return result;
    }



    @ApiOperation(value = "面试题交卷")
    @PutMapping
    @ResponseBody
    public ResultDto saveInterview(@RequestParam("answer") String answer, @RequestParam("topicId") Long topicId, @RequestParam(value = "last", required = false, defaultValue = "false") boolean last, HttpServletRequest request) {
        Date date = new Date();
        ResultDto result = new ResultDto();
        try {
            // 设置登陆人信息
            Interview interview = (Interview) SessionUtil.getUser(request.getSession());
            String s = String.valueOf((date.getTime() - (Long) SessionUtil.getInitTime(request.getSession())) / 1000);//答题使用的时间（秒）
            interview.setSurplusTime(s);
            interview.setPage(last ? interview.getPage() : (interview.getPage() + 1));
            if (interviewService.updateInerviewService(new InterviewDto(interview.getId(), s, interview.getPage()))) {
                // 判断是否超出了答题时间范围,设置答案状态
                int status = ((date.getTime() - (Long) SessionUtil.getLastTime(request.getSession())) > timeout) ? 0 : ((topicService.get(topicId).getAnswer()).equals(answer) ? 1 : 2);
                interviewAnswerService.update(new InterviewAnswer(topicId, StringUtils.isEmpty(answer) ? "x" : answer, interview.getId(), date, HttpUtil.getIpAdrress(request), status));
            }
            //是否是最后一道题目
            if (last) {
                SessionUtil.setLast(last, request.getSession());
            }
            SessionUtil.setUser(interview, request.getSession());
        } catch (Exception e) {
            e.printStackTrace();
            result.errorStatus();
            log.error("提交面试题错误");
        }
        return result;
    }

    @ApiOperation(value = "作答统计")
    @GetMapping("/statistics")
    @ResponseBody
    public ResultDto statistics(HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            Interview interview = (Interview) SessionUtil.getUser(request.getSession());
            dto.setData(interviewAnswerService.statistics(interview.getId()));
            dto.successStatus();
//            this.cleanTime(request);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("作答统计错误");
        }
        return dto;
    }

    @ApiOperation("生成题目接口")
    @GetMapping("/generatetopic")
    @ResponseBody
    public ResultDto generatetopic(HttpServletRequest request) {
        ResultDto resultDto = new ResultDto();
        Date date = new Date();
        try {
            Interview interview = (Interview) SessionUtil.getUser(request.getSession());
            //如果用户之前生成过题目则不继续生成题目
            ResultDto dto = interviewAnswerService.find(new InterviewAnswer(interview.getId()), new PageRequest(0, 1), false);
            if (dto.getRows() == null || dto.getRows().size() < 1) {//生成题目
                interviewAnswerService.save(interview.getPosition(), interview.getId());
            }
            //记录初始化时间
            SessionUtil.setInitTime(StringUtils.isEmpty(interview.getSurplusTime()) ? date.getTime() : TimeUtil.minuteSubtract(date, Integer.valueOf((Long.valueOf(interview.getSurplusTime()) / 60) + "")), request.getSession());
        } catch (Exception e) {
            e.printStackTrace();
            resultDto.errorStatus();
            log.error("生成题目错误.");
        }
        return resultDto;
    }

    @ApiOperation("清楚登陆信息")
    @GetMapping("/clean")
    public String cleanTime(HttpServletRequest request) {
        SessionUtil.remove(MapKeys.INITTIME, request.getSession());
        SessionUtil.remove(MapKeys.LASTTIME, request.getSession());
        SessionUtil.remove(MapKeys.CURRTOPICCOUNT, request.getSession());
        SessionUtil.remove(MapKeys.TIMEOUT, request.getSession());
        SessionUtil.remove(MapKeys.LAST, request.getSession());
        SessionUtil.remove(MapKeys.USER, request.getSession());
        return "redirect:/topic/login";
    }
}
