package com.example.demo.controller;

import com.example.demo.common.MapKeys;
import com.example.demo.dto.ResultDto;
import com.example.demo.form.RoleForm;
import com.example.demo.pojo.Log;
import com.example.demo.pojo.Role;
import com.example.demo.pojo.User;
import com.example.demo.service.LogService;
import com.example.demo.service.RoleService;
import com.example.demo.util.HttpUtil;
import com.example.demo.util.ThreadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description :角色控制层
 * ---------------------------------
 * @Author : Ye.Shengguang
 * @Date : 2018/6/3 20:36
 */
@Controller
@Api(value = "角色操作入口", description = "角色基本信息操作API", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping(value = "/role")
public class RoleController {
    @Autowired
    private LogService logService;
    @Autowired
    private RoleService roleService;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @ApiOperation(value = "角色查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "角色code", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "desc", value = "角色描述", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "createPeople", value = "创建人", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "update_people", value = "修改人", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "当前页数", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序，如：?sort=id或者?sort=id,desc", required = false, dataType = "String", paramType = "query")
    })
    @GetMapping
    @RequiresPermissions({"20006"})
    public @ResponseBody
    ResultDto findUserTwo(RoleForm form, String sort, Pageable pageable) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            //截取排序字符串如：id,desc 替换为 id desc
            if (!StringUtils.isEmpty(sort) && sort.indexOf(",") != -1)
                sort = sort.replace(',', ' ');
            Role role = new Role();
            BeanUtils.copyProperties(form, role);
            result = this.roleService.findRoleService(role, sort, pageable, ((User) SecurityUtils.getSubject().getPrincipal()).getId());
            result.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询角色信息错误");
        }
        return result;
    }

    @ApiOperation(value = "角色页面")
    @GetMapping(value = "/page")
    @RequiresPermissions({"20001"})
    public String rolePage() {
        return "role";
    }

    @ApiOperation(value = "角色添加")
    @PostMapping
    @RequiresPermissions({"20005"})
    public @ResponseBody
    ResultDto addRole(@RequestBody RoleForm roleForm, HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        Role role = new Role();
        try {
            BeanUtils.copyProperties(roleForm, role);
            role = roleService.addRoleService(role);
            Long id = role.getId();
            dto.setData(role);
            dto.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "添加了角色ID为" + id + "的信息", MapKeys.LOG_3, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (BeansException e) {
            e.printStackTrace();
            log.error("添加角色信息错误");
        }
        return dto;
    }

    @ApiOperation(value = "角色修改")
    @PutMapping(value = "/{id}")
    @RequiresPermissions({"20004"})
    public @ResponseBody
    ResultDto updateRole(@PathVariable("id") Long id, @RequestBody RoleForm roleForm, HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        Role role = new Role();
        try {
            roleForm.setId(id);
            BeanUtils.copyProperties(roleForm, role);
            if (roleService.updateRoleService(role, roleForm.getIds())) {
                dto.successStatus();
                User user = (User) SecurityUtils.getSubject().getPrincipal();
                ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "修改了角色ID为" + id + "的信息", MapKeys.LOG_3, HttpUtil.getIpAdrress(request), user.getId())));
            } else
                dto.setError("修改角色失败");
        } catch (BeansException e) {
            e.printStackTrace();
            log.error("修改角色信息错误");
        }
        return dto;
    }

    @ApiOperation(value = "角色详情")
    @GetMapping(value = "/{id}")
    @RequiresPermissions({"20002"})
    public @ResponseBody
    ResultDto getRole(@PathVariable("id") Long id) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        if (id == null) {
            dto.setError("ID不能为空");
        } else {
            try {
                dto.setData(this.roleService.getRoleService(id));
                dto.successStatus();
            } catch (BeansException e) {
                e.printStackTrace();
                log.error("获取角色详情信息错误");
            }
        }
        return dto;
    }

    @ApiOperation(value = "角色授权")
    @PostMapping(value = "/authorization")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "权限资源编号（传参格式：1,2,3,4）", allowMultiple = true, required = true, dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "id", value = "角色编号", allowMultiple = true, required = true, dataType = "long", paramType = "query")
    })
    public @ResponseBody
    ResultDto RoleAuthorization(@RequestParam("id") Long id, @RequestParam("ids") String idStr, HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            roleService.saveRolePrivileges(idStr.split(","), id);
            dto.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "给角色ID：" + id + "授权了：" + idStr + "这些ID权限", MapKeys.LOG_3, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("角色授权出错");
        }
        return dto;
    }

    @ApiOperation(value = "根据id批量删除角色")
    @DeleteMapping(value = "/{id}")
    @ApiImplicitParam(name = "ids", value = "根据id批量删除角色", allowMultiple = true, required = true, dataType = "long", paramType = "query")
    public @ResponseBody
    ResultDto deleteRole(@PathVariable("id") String idStr, HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            String[] ids = idStr.split(",");
            StringBuffer sb = new StringBuffer();//记录真正被删除的角色编号
            boolean bol = false;//为true则是有些角色没有删除掉
            for (int i = 0; i < ids.length; i++) {
                if (roleService.delRoleService(Long.valueOf(ids[i])) != null) {
                    bol = true;
                    continue;
                }
                sb.append(ids[i]).append(",");
            }
            if (bol) dto.setError("所选的角色中有些正在被用户使用，请先解除账户和要删除的角色绑定！");
            else dto.successStatus();
            //一个没有删除则不添加进日志里
            if (sb.length() > 0) {
                User user = (User) SecurityUtils.getSubject().getPrincipal();
                ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "删除了角色ID为" + sb.substring(0, sb.length() - 1) + "的信息", MapKeys.LOG_3, HttpUtil.getIpAdrress(request), user.getId())));
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除角色出错");
        }
        return dto;
    }

}
