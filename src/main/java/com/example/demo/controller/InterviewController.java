package com.example.demo.controller;

import com.alibaba.fastjson.JSON;
import com.example.demo.common.MapKeys;
import com.example.demo.dto.InterviewDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.form.InterviewForm;
import com.example.demo.mapper.SysInterviewMapper;
import com.example.demo.pojo.Interview;
import com.example.demo.pojo.Log;
import com.example.demo.pojo.SysInterview;
import com.example.demo.pojo.User;
import com.example.demo.service.InterviewService;
import com.example.demo.service.LogService;
import com.example.demo.util.HttpUtil;
import com.example.demo.util.ThreadUtil;
import com.example.demo.util.UploadUtil;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试人员信息控制层
 * @date 2018/6/6 17:23
 */
@Controller
@RequestMapping("/interview")
@PropertySource("classpath:config/path.properties")// 用来指定配置文件的位置
@Api(value = "面试人员信息操作入口", description = "面试人员信息基本信息操作API", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
public class InterviewController {

    @Autowired
    private InterviewService interviewService;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LogService logService;

    @Value("${upload}")
    private String upload;

    @Autowired
    SysInterviewMapper interviewMapper;

    @GetMapping(value = "/findAll")
    @ResponseBody
    public List<Interview> findAll(@RequestParam(required = false, defaultValue = "1") int page,
                                      @RequestParam(required = false, defaultValue = "5") int rows){
        Example example=new Example(Interview.class);
        Example.Criteria criteria=example.createCriteria();
        criteria.andLike("name","%黄%");
        PageHelper.startPage(page,rows);
        return interviewMapper.selectByExample(example);
    }


    @ApiOperation(value = "面试信息页面")
    @GetMapping(value = "/page")
    @RequiresPermissions({"40001"})
    public String page() {
        return "interview";
    }

    @ApiOperation(value = "面试信息查询")
    @GetMapping
    @RequiresPermissions({"40005"})
    @ResponseBody
    public ResultDto findInterview(InterviewForm interviewForm, @RequestParam(value = "page", required = false, defaultValue = "0")
            Integer page, @RequestParam(value = "rows", required = false, defaultValue = "20") Integer rows, String order, String sord) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            result = this.interviewService.findInterviewService(interviewForm, page, rows, null);
            result.setPage(page);
            result.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询面试信息错误");
        }
        return result;
    }


    @ApiOperation(value = "日期的面试信息查询")
    @GetMapping("/findInterviewAll")
    @RequiresPermissions({"40005"})
    @ResponseBody
    public List<Interview> findInterviewAll(InterviewForm interviewForm, @RequestParam(value = "page", required = false, defaultValue = "0")
            Integer page, @RequestParam(value = "rows", required = false, defaultValue = "2000")
                                                    Integer rows, String order, String sord) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        List<Interview> list = null;
        StringBuilder sb = new StringBuilder();
        try {
            Pageable pageable = null;
            if (!StringUtils.isEmpty(order) && !StringUtils.isEmpty(sord))
                pageable = new PageRequest(page < 1 ? 0 : (page - 1), rows, new Sort(sord.equals("asc") ? Sort.Direction.ASC : Sort.Direction.DESC, order));
            else
                pageable = new PageRequest(page < 1 ? 0 : (page - 1), rows, new Sort(Sort.Direction.DESC, "updateStamp"));
            Page<Interview> pages = this.interviewService.findInterviewServiceAndStatus(interviewForm, pageable);
            list = pages.getContent();

            result.setTotal((int) pages.getTotalElements());
            result.setRows(pages.getContent());
            result.setPage(page);
            int pagesize = (int) Math.ceil((pages.getTotalElements() * 1.0) / rows);
            result.setPageSize(pagesize < 1 ? 1 : pagesize);
            result.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询面试信息错误");
        }
        return list;
    }

    @RequestMapping(value = "calendarEvents")
    @ResponseBody
    public String calendarEvents() {
        //List<LecturerAgendaVo> agendas = lecturerAgendaService.findAllMatchedAgenda();
        StringBuilder sb = new StringBuilder();

        sb.append("[{\n" +
                "\t\"title\": \"生日\",\n" +
                "\t\"start\": \"new Date(2018, 06, 17 + 1, 19, 0)\",\n" +
                "\t\"end\": \"new Date(2018,06, 18 + 1, 22, 30)\",\n" +
                "\t\"allDay\": false\n" +
                "}, {\n" +
                "\t\"title\": \"打开百度\",\n" +
                "\t\"start\": \"new Date(2018, 06, 17 + 1, 19, 0)\",\n" +
                "\t\"end\": \"new Date(2018,06, 18 + 1, 22, 30)\",\n" +
                "\t\"url\": \"http://baidu.com/\"\n" +
                "}]");
        JSON.toJSON(sb);
        return sb.toString();
    }

    /**
     * 面试信息添加
     *
     * @param files
     * @return
     */
    @ApiOperation(value = "面试信息添加")
    @ApiImplicitParam(value = "上传的文件", name = "files", paramType = "form", dataType = "file")
    @PostMapping
    @RequiresPermissions({"40002"})
    @ResponseBody
    public ResultDto saveInterview(@RequestParam(value = "file", required = false) MultipartFile files, InterviewForm interviewForm, BindingResult bindingResult, HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        Interview interview = new Interview();
        try {
//            User user = (User) SecurityUtils.getSubject().getPrincipal();
            BeanUtils.copyProperties(interviewForm, interview);
            interview.setFileName(files != null ? files.getOriginalFilename().substring(files.getOriginalFilename().lastIndexOf('.'), files.getOriginalFilename().length()) : null);
            interview = interviewService.saveInterviewService(interview);
            Long id = interview.getId();
            if (files != null) {
                String finalFilename = UploadUtil.uploadFile(files, interview.getId().toString(), upload);
//                ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "上传面试人员ID:" + id + "的简历:" + finalFilename, MapKeys.LOG_5, HttpUtil.getIpAdrress(request), user.getId())));
            }
            dto.successStatus();
//            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "添加了ID:" + id + "面试信息", MapKeys.LOG_6, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (IOException e) {
            e.printStackTrace();
            log.error("文件上传失败");
            dto.setError("简历上传失败");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("添加面试人员信息出错");
        }
        return dto;
    }

    /**
     * 面试信息修改
     *
     * @param files
     * @return
     */
    @ApiOperation(value = "面试信息修改")
    @ApiImplicitParam(value = "上传的文件", name = "files", paramType = "form", dataType = "file")
    @PutMapping(value = "/{id}", headers = "content-type=multipart/form-data")
    @RequiresPermissions({"40003"})
    @ResponseBody
    public ResultDto updateInterview(@RequestParam(value = "file", required = false) MultipartFile files, @PathVariable("id") Long id, InterviewForm interviewForm, BindingResult bindingResult, HttpServletRequest request) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        InterviewDto interview = new InterviewDto();
        try {
//            User user = (User) SecurityUtils.getSubject().getPrincipal();
            BeanUtils.copyProperties(interviewForm, interview);
            interview.setFileName(files != null ? files.getOriginalFilename().substring(files.getOriginalFilename().lastIndexOf('.'), files.getOriginalFilename().length()) : null);
            interview.setId(id);
            if (interviewService.updateInerviewService(interview)) {
                if (files != null) {
                    String finalFilename = UploadUtil.uploadFile(files, id.toString(), upload);
//                    ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "覆盖面试人员ID:" + id + "的简历:" + finalFilename, MapKeys.LOG_5, HttpUtil.getIpAdrress(request), user.getId())));
                }
                dto.successStatus();
//                ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "修改了ID:" + id + "面试信息", MapKeys.LOG_6, HttpUtil.getIpAdrress(request), user.getId())));
            } else {
                dto.setError("添加失败");
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("文件上传失败");
            dto.setError("简历上传失败");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改面试人员信息出错");
        }
        return dto;
    }


    /**
     * 面试时间的修改，在日历拖动实现修改
     *
     * @return
     */
    @ApiOperation(value = "面试时间的修改，在日历拖动实现修改")
    @RequestMapping(value = "/Calendar")
    @RequiresPermissions({"40003"})
    @ResponseBody
    public ResultDto updateInterviewByCalendar(@RequestParam("id") Long id, @RequestParam("daydiff") Integer daydiff, InterviewForm interviewForm, BindingResult bindingResult, HttpServletRequest request) {

        ResultDto dto = new ResultDto();
        dto.errorStatus();
        //根据ID查询面试者信息
        InterviewDto interview = this.interviewService.getInterviewService(id);

        try {
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            //BeanUtils.copyProperties(interviewForm, interview);
            //interview.setId(id);

            Timestamp time = null;

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(interview.getInterviewTime());
            calendar.add(calendar.DATE, daydiff);//把日期往后增加一天.整数往后推,负数往前移动
            time = new Timestamp(calendar.getTimeInMillis());

            interview.setInterviewTime(time);//面试时间

            if (interviewService.updateInerviewService(interview)) {
                dto.successStatus();
                ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "修改了ID:" + id + "面试信息", MapKeys.LOG_6, HttpUtil.getIpAdrress(request), user.getId())));
            } else {
                dto.setError("添加失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改面试人员信息出错");
        }
        return dto;
    }

    @ApiOperation(value = "面试人员信息删除")
    @DeleteMapping(value = "/{ids}")
    @RequiresPermissions({"40004"})
    public @ResponseBody
    ResultDto delInterview(@PathVariable("ids") String idStr, HttpServletRequest request) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            String ids[] = idStr.split(",");
            for (int i = 0; i < ids.length; i++) {
                interviewService.delInerviewService(Long.valueOf(ids[i]));
            }
            result.successStatus();
//            User user = (User) SecurityUtils.getSubject().getPrincipal();
//            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "删除了面试信息ID为：" + idStr + "的信息", MapKeys.LOG_6, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除面试人员信息错误");
        }
        return result;
    }

    @ApiOperation(value = "面试人员详细信息")
    @GetMapping(value = "/{id}")
    @RequiresPermissions({"40006"})
    public @ResponseBody
    ResultDto getInterview(@PathVariable("id") Long id) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            InterviewDto interview = this.interviewService.getInterviewService(id);
            if (interview == null) {
                result.setError("没有面试人员信息,可能已经被删除!");
            } else {
                result.setData(interview);
                result.successStatus();
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除面试人员信息错误");
        }
        return result;
    }

    @ApiOperation(value = "面试人员手机号验证")
    @GetMapping(value = "/phone")
    @RequiresPermissions({"40006"})
    public @ResponseBody
    ResultDto checkPhone(@RequestParam("phone") String phone) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            result.setData(this.interviewService.getInterviewService(phone));
            result.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除面试人员信息错误");
        }
        return result;
    }

    @ApiOperation(value = "面试人员简历下载")
    @GetMapping(value = "/down/{id}")
    @RequiresPermissions({"40007"})
    public void download(@PathVariable("id") Long id, HttpServletResponse response) {
        try {
            //获得面试人员信息
            InterviewDto interview = this.interviewService.getInterviewService(id);
            // path是指欲下载的文件的路径。
            File file = new File(upload + interview.getId() + interview.getFileName());
            response.reset();
            //设置相应类型application/octet-stream
            response.setContentType("applicatoin/octet-stream");
            //设置头信息                 Content-Disposition为属性名  附件形式打开下载文件   指定名称  为 设定的filename
            response.setHeader("Content-Disposition", "attachment;filename=\"" + URLEncoder.encode((interview.getName() + interview.getFileName()), "UTF-8") + "\"")
            ;
            //输入流写入输出流
            InputStream inputStream = new FileInputStream(file);
            ServletOutputStream ouputStream = response.getOutputStream();
            byte b[] = new byte[1024];
            int n;
            //循环读取 !=-1读取完毕
            while ((n = inputStream.read(b)) != -1) {
                //写入到输出流 从0读取到n
                ouputStream.write(b, 0, n);
            }
            //关闭流、释放资源
            ouputStream.close();
            inputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
