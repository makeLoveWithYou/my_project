package com.example.demo.controller;

import com.example.demo.common.MapKeys;
import com.example.demo.dto.InterviewDto;
import com.example.demo.pojo.Interview;
import com.example.demo.pojo.Log;
import com.example.demo.pojo.User;
import com.example.demo.service.InterviewService;
import com.example.demo.service.LogService;
import com.example.demo.util.HttpUtil;
import com.example.demo.util.SessionUtil;
import com.example.demo.util.ThreadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 登陆控制层
 * @date 2018/6/1 17:56
 */
@Api(value = "登陆操作入口", description = "登陆", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Controller
public class LoginController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LogService logService;

    @Autowired
    private InterviewService interviewService;

    @ApiOperation(value = "登陆页面跳转")
    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @ApiOperation(value = "面试人员登陆页面跳转")
    @GetMapping("/topic/login")
    public String topLogin() {
        return "interview_login";
    }

    @ApiOperation(value = "面试人员登陆功能")
    @PostMapping(value = "/topic/login")
    @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String", paramType = "query")
    public String topLogin(Model model, String phone, HttpServletRequest request) {
        if (StringUtils.isEmpty(phone)) {
            model.addAttribute("error", "phone Not empty");
            return "interview_login";
        }
        try {
            InterviewDto interview = this.interviewService.getInterviewService(phone);

            if (interview != null) {
                //2，面试方式为网络答题
                if (interview.getInterview() == 2) {
                    Date date = new Date();
                    //判断当前时间是否在答题开始时间和答题结束时间之间
                    if (date.getTime() > interview.getStartTime().getTime() && interview.getEndTime().getTime() > date.getTime()) {
                        Interview in = new Interview();
                        BeanUtils.copyProperties(interview, in);
                        SessionUtil.setUser(in, request.getSession());
                        return "redirect:/topic/query";
                    } else {
                        model.addAttribute("error", "答题时间未开始或者已经结束！");
                    }
                }
            } else {
                model.addAttribute("error", "账户不存在！或者没有资格参加网络面试");
            }
        } catch (Exception ae) {
            ae.printStackTrace();
            model.addAttribute("error", "登陆异常！");
        }
        return "interview_login";
    }

    @ApiOperation(value = "登陆功能")
    @PostMapping(value = "login")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "String", paramType = "query")
    })
    public String login(Model model, String username, String password, HttpServletRequest request) {
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            model.addAttribute("error", "username or password is not null");
            return "login";
        }
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        Subject currentUser = SecurityUtils.getSubject();
        try {
            currentUser.login(token);
        } catch (IncorrectCredentialsException ice) {
            logger.info("对用户【" + username + "】进行登录验证，验证未通过，错误的凭证！");
            model.addAttribute("error", "用户名或密码不正确！");
        } catch (UnknownAccountException uae) {
            logger.info("对用户【" + username + "】进行登录验证，验证未通过，未知账户！");
            model.addAttribute("error", "未知账户！");
        } catch (LockedAccountException lae) {
            logger.info("对用户【" + username + "】进行登录验证，验证未通过，账户锁定！");
            model.addAttribute("error", "账户已锁定！");
        } catch (ExcessiveAttemptsException eae) {
            logger.info("对用户【" + username + "】进行登录验证，验证未通过，错误次数太多！");
            model.addAttribute("error", "用户名或密码错误次数太多！");
        } catch (AuthenticationException ae) {
            logger.info("对用户【" + username + "】进行登录验证，验证未通过，堆栈轨迹如下：！");
            ae.printStackTrace();
            model.addAttribute("error", "用户名或密码不正确！");
        }
        if (currentUser.isAuthenticated()) {//登陆成功
            request.getSession().setAttribute(MapKeys.USERNAME, ((User) currentUser.getPrincipal()).getName());
            currentUser.getSession().setTimeout(7200000);
            User user = (User) currentUser.getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "登陆了系统", MapKeys.LOG_1, HttpUtil.getIpAdrress(request), user.getId())));
            return "redirect:index";
        } else {
            token.clear();
            return "login";
        }
    }

    /*@ApiOperation(value = "登陆功能")
    @PostMapping(value = "login")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "password", value = "密码", required = true, dataType = "String", paramType = "query")
    })
    @ResponseBody
    public Map login(String username, String password, HttpServletRequest request) {
        Map retMap = new HashMap<String, Object>();
        retMap.put("code", 20000);
        UsernamePasswordToken token = new UsernamePasswordToken("abc", "123456");
        Subject currentUser = SecurityUtils.getSubject();
        currentUser.login(token);
        if (currentUser.isAuthenticated()) {//登陆成功
            request.getSession().setAttribute(MapKeys.USERNAME, ((User) currentUser.getPrincipal()).getName());
            currentUser.getSession().setTimeout(7200000);
            User user = (User) currentUser.getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "登陆了系统", MapKeys.LOG_1, HttpUtil.getIpAdrress(request), user.getId())));
        } else {
            token.clear();
        }
        return retMap;
    }*/

    @PostMapping("/user/login")
    @ResponseBody
    public String login(String username, String password) {
        return "{{code: 20000, data: {token: \"admin\"}}}";
    }

    @RequestMapping(value = "/user/login", method = RequestMethod.OPTIONS)
    @ResponseBody
    public void logins(String username, String password) {
    }

    @ApiOperation(value = "注销当前登陆页面")
    @GetMapping("/logout")
    public String logout(HttpServletRequest request) {
        request.getSession().removeAttribute(MapKeys.USERNAME);
        SecurityUtils.getSubject().logout();
        return "login";
    }

}
