package com.example.demo.controller;

import com.example.demo.common.MapKeys;
import com.example.demo.dto.ResultDto;
import com.example.demo.form.PrivilegesForm;
import com.example.demo.pojo.Log;
import com.example.demo.pojo.Privileges;
import com.example.demo.pojo.User;
import com.example.demo.service.LogService;
import com.example.demo.service.PrivilegesService;
import com.example.demo.util.HttpUtil;
import com.example.demo.util.ThreadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@Api(value = "权限操作入口", description = "权限基本信息操作API", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping("/privileges")
public class PrivilegesController {

    @Autowired
    private PrivilegesService privilegesService;
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LogService logService;

    @ApiOperation(value = "角色管理页面跳转")
    @GetMapping(value = "/page")
    @RequiresPermissions({"30001"})
    public String page() {
        return "privileges";
    }

    @ApiOperation(value = "权限查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "privilegeCode", value = "权限code", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "name", value = "权限名称", dataType = "string", paramType = "query"),
            @ApiImplicitParam(name = "createPeople", value = "创建人", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "parentId", value = "权限父权限", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "当前页数", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序，如：?sort=id或者?sort=id,desc", dataType = "String", paramType = "query")
    })
    @GetMapping
    @RequiresPermissions({"30002"})
    public @ResponseBody
    ResultDto findPrivileges(PrivilegesForm form, String sort, Pageable pageable) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            //截取排序字符串如：id,desc 替换为 id desc
            if (!StringUtils.isEmpty(sort) && sort.indexOf(",") != -1)
                sort = sort.replace(',', ' ');
            Privileges privileges = new Privileges();
            BeanUtils.copyProperties(form, privileges);
            result = this.privilegesService.findPrivilegesService(privileges, pageable, sort);
            result.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询权限信息错误");
        }
        return result;
    }

    @ApiOperation(value = "权限查询")
    @ApiImplicitParam(name = "id", value = "权限编号", dataType = "long", paramType = "path")
    @GetMapping(value = "/{id}")
    @RequiresPermissions({"30005"})
    public @ResponseBody
    ResultDto getPrivileges(@PathVariable("id") Long id) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            result.setData(privilegesService.getPrivilegesService(id));
            result.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("查询权限信息错误");
        }
        return result;
    }

    @ApiOperation(value = "权限添加")
    @PostMapping
    @RequiresPermissions({"30003"})
    public @ResponseBody
    ResultDto addPrivileges(@RequestBody PrivilegesForm privilegesForm, HttpServletRequest request) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            Privileges privileges = new Privileges();
            BeanUtils.copyProperties(privilegesForm, privileges);
            privileges = privilegesService.addPrivileges(privileges);
            Long id = privileges.getId();
            result.setData(privileges);
            result.successStatus();
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "添加了ID为：" + id + "的权限", MapKeys.LOG_4, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("添加权限信息错误");
        }
        return result;
    }

    @ApiOperation(value = "权限修改")
    @PutMapping(value = "/{id}")
    @RequiresPermissions({"30006"})
    public @ResponseBody
    ResultDto updatePrivileges(@PathVariable("id") Long id, @RequestBody PrivilegesForm privilegesForm, HttpServletRequest request) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            Privileges privileges = new Privileges();
            BeanUtils.copyProperties(privilegesForm, privileges);
            privileges.setId(id);
            if (privilegesService.updatePrivileges(privileges)) {
                result.successStatus();
                User user = (User) SecurityUtils.getSubject().getPrincipal();
                ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "修改了ID为：" + id + "的权限", MapKeys.LOG_4, HttpUtil.getIpAdrress(request), user.getId())));
            } else result.setError("修改失败");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("修改权限信息错误");
        }
        return result;
    }

    @ApiOperation(value = "权限删除")
    @DeleteMapping(value = "/{ids}")
    @RequiresPermissions({"30004"})
    public @ResponseBody
    ResultDto delPrivileges(@PathVariable("ids") String idStr, HttpServletRequest request) {
        ResultDto result = new ResultDto();
        result.errorStatus();
        try {
            String ids[] = idStr.split(",");
            StringBuffer sb = new StringBuffer();
            boolean bol = false;//为true则是有些数据没有删除成功
            for (int i = 0; i < ids.length; i++) {
                if (privilegesService.delPrivileges(Long.valueOf(ids[i])) != null) {
                    bol = true;
                    continue;
                }
                sb.append(ids[i]).append(",");
            }
            if (!bol) result.successStatus();
            else result.setError("所选的权限中有些正在被角色使用，或有子权限绑定，请先解除角色和要删除的权限绑定！");
            User user = (User) SecurityUtils.getSubject().getPrincipal();
            ThreadUtil.executorService.submit(() -> logService.saveLog(new Log(user.getName() + "删除了ID为：" + sb.substring(0, sb.length() - 1) + "的权限", MapKeys.LOG_4, HttpUtil.getIpAdrress(request), user.getId())));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("删除权限信息错误");
        }
        return result;
    }
}
