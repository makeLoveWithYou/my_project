package com.example.demo.controller;

import com.example.demo.common.MapKeys;
import com.example.demo.pojo.Interview;
import com.example.demo.util.SessionUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 主页面控制
 * @date 2018/6/1 17:51
 */
@Controller
@PropertySource("classpath:config/config.properties")// 用来指定配置文件的位置
public class IndexController {

    @Value("${timeout}")
    private long timeout;

    @Value("${counttime}")
    private long counttime;

    /**
     * @Description: 跳转到主页面
     * @author ShengGuang.Ye
     * @date 2018/6/1 17:52
     * @version V1.0
     */
    @GetMapping("/index")
    public String responseIndex(Model model, HttpServletRequest request) {
        model.addAttribute(MapKeys.USERNAME, request.getSession().getAttribute(MapKeys.USERNAME));
        return "index";
    }

    /**
     * @Description: 跳转到面试主页面
     * @author ShengGuang.Ye
     * @date 2018/6/1 17:52
     * @version V1.0
     */
    @GetMapping("/topic/index")
    public String topResponseIndex(HttpServletRequest request) {
        if (SessionUtil.getUser(request.getSession()) == null) {
            return "redirect:/topic/login";
        }
        return "interview_index";
    }

    /**
     * 取面试题
     * @param request
     * @param model
     * @return
     */
    @GetMapping("/topic/query")
    public String topResponseIndex(HttpServletRequest request, Model model) {
        if (SessionUtil.getUser(request.getSession()) == null) {
            return "redirect:/topic/login";
        }
        Interview interview = (Interview) SessionUtil.getUser(request.getSession());
        model.addAttribute(MapKeys.USER, interview.getName());
        model.addAttribute(MapKeys.TIMEOUT, timeout);
        model.addAttribute(MapKeys.COUNTTIME, counttime);
        return "interview_query";
    }

    /**
     * @Description: 页面跳转
     * @author ShengGuang.Ye
     * @date 2018/6/1 17:52
     * @version V1.0
     */
    @GetMapping("/path/{path}")
    public String path(@PathVariable("path") String path) {
        return path;
    }

}
