package com.example.demo.controller;

import com.example.demo.dto.ResultDto;
import com.example.demo.pojo.Template;
import com.example.demo.service.TemplateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试题模板
 * @date 2018/6/21 14:03
 */
@Controller
@Api(value = "面试题模板操作入口", description = "面试题模板基本信息操作API", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequestMapping("/template")
public class TemplateController {

    @Autowired
    private TemplateService templateService;

    @GetMapping("/page")
    @RequiresPermissions({"50001"})
    public String page() {
        return "template";
    }

    @ApiOperation("面试题模板查询")
    @GetMapping("/findTemplate")
    @ResponseBody
    @RequiresPermissions({"50006"})
    public ResultDto find(Template template, @RequestParam(value = "page", required = false, defaultValue = "1") Integer page, @RequestParam(value = "limit", required = false, defaultValue = "20") Integer limit) {
        PageRequest pageable = new PageRequest(page > 0 ? page - 1 : page, limit);
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            Page<Template> pages = templateService.find(template, pageable);
            dto.setTotal((int) pages.getTotalElements());
            int pagesize = (int) Math.ceil((pages.getTotalElements() * 1.0) / pageable.getPageSize());
            dto.setPage(page);
            dto.setPageSize(pagesize < 1 ? 1 : pagesize);
            dto.setRows(pages.getContent());
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }


    /*@ApiOperation("面试题模板查询")
    @GetMapping("/findTemplate")
    @ResponseBody
    @RequiresPermissions({"50006"})
    public LayUIDataTableVo find(Template template, @RequestParam(value = "page", required = false, defaultValue = "1") Integer page, @RequestParam(value = "limit", required = false, defaultValue = "20") Integer limit) {
        PageRequest pageable = new PageRequest(page - 1, limit);
        LayUIDataTableVo dto = new LayUIDataTableVo();
        try {
            Page<Template> pages = templateService.find(template, pageable);
            dto.setMsg("鬼知道发生了什么，反正没错");
            dto.setCount(pages.getTotalElements());
            int pagesize = (int) Math.ceil((pages.getTotalElements() * 1.0) / pageable.getPageSize());
            dto.setCode(200);
            dto.setData(pages.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }*/


    @ApiOperation("面试题模板新增")
    @PostMapping
    @RequiresPermissions({"50002"})
    @ResponseBody
    public ResultDto save(@RequestBody Template template) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            dto.setData(templateService.save(template));
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }

    @ApiOperation("面试题模板修改")
    @PutMapping("{id}")
    @RequiresPermissions({"50003"})
    @ResponseBody
    public ResultDto update(@PathVariable("id") Long id, @RequestBody Template template) {
        ResultDto dto = new ResultDto();

        dto.errorStatus();
        try {
            if (templateService.update(template))
                dto.successStatus();
            else dto.setError("修改失败");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }

    /*@ApiOperation("面试题模板删除")
    @DeleteMapping("/{id}")
    @RequiresPermissions({"50004"})
    @ResponseBody
    public ResultDto delete(@PathVariable("id") Long id) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            templateService.delete(id);
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }*/

    @ApiOperation("面试题模板删除")
    @DeleteMapping("/{id}")
    @RequiresPermissions({"50004"})
    @ResponseBody
    public ResultDto delete(@PathVariable("id") String id) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        try {
            String ids[] = id.split(",");
            for (int i = 0; i < ids.length; i++) {
                templateService.delete(Long.valueOf(ids[i]));
            }
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }


    @ApiOperation("面试题模板详情")
    @GetMapping("{id}")
    @RequiresPermissions({"50005"})
    @ResponseBody
    public ResultDto get(@PathVariable("id") Long id) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        Template template = null;
        try {
            template = new Template();
            Template templats = templateService.get(id);
            BeanUtils.copyProperties(templats, template);
            //template.setId(templats.getId());
            dto.setData(template);
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }

}
