package com.example.demo.mapper;


import com.example.demo.pojo.Interview;
import com.example.demo.pojo.SysInterview;
import com.example.demo.util.MyMapper;

public interface SysInterviewMapper extends MyMapper<Interview> {
}