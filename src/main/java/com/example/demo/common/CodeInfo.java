package com.example.demo.common;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 状态码定义
 * @date 2018/6/1 13:42
 */
public interface CodeInfo {
    Integer CODE_500 = 500;
    Integer CODE_400 = 400;
    Integer CODE_200 = 200;
    Integer CODE_202 = 202;
}
