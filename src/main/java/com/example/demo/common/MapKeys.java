package com.example.demo.common;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 定义公共使用的key
 * @date 2018/5/28 16:01
 */
public interface MapKeys {

    //数据集的键
    String ROWS = "rows";
    //当前页数
    String PAGE = "page";
    //当前登陆用户
    String USER = "_user";
    //当前做题数
    String CURRTOPICCOUNT = "currTopicCount";
    //当前做题id
    String TOPICID = "topicId";
    //是否是最后一题
    String LAST = "true";
    //当前页数
    String TIMEOUT = "timeout";
    //数据集总数据量的键
    String TOTAL = "total";
    //保存状态码的键
    String STATUS = "status";
    //错误信息的键
    String ERROR = "error";
    //获取用户姓名
    String USERNAME = "username";
    //说明删除权限下面有子权限
    String SON = "SON";
    //说明删除的数据被其他数据绑定了
    String BIND = "BIND";

    //TODO 时间
    String LASTTIME = "_lastTime";
    String INITTIME = "_initTime";
    String CURRTIME = "_currTime";
    String COUNTTIME = "counttime";
    String SUMTIME = "sumtime";


    // TODO 日志类型
    Long LOG_1 = 1L;//登陆日志
    Long LOG_2 = 2L;//系统用户日志
    Long LOG_3 = 3L;//系统角色日志
    Long LOG_4 = 4L;//系统权限资源日志
    Long LOG_5 = 5L;//文件上传日志
    Long LOG_6 = 6L;//面试管理日志

    // TODO Solr 键配置
    String Q = "q";//查询条件
    String FL = "fl";//显示字段

}
