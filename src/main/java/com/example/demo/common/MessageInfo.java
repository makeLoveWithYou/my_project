package com.example.demo.common;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 错误信息定义
 * @date 2018/5/25 13:32
 */
public interface MessageInfo {

    String error_500 = "系统异常，攻城狮正在修复中";
    String error_400 = "参数有误";

}
