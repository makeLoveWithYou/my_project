package com.example.demo.pojo;

import javax.persistence.*;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试题模板
 * @date 2018/6/20 10:29
 */
@Entity(name = "sys_template")
public class Template {

    /**
     * 编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 题目类型
     */
    @Column(name = "topictype")
    private Integer topicType;

    /**
     * 题目数量
     */
    @Column(name = "topiccount")
    private Integer topicCount;

    /**
     * 面试人员职位类型
     */
    @Column(name = "jobtype")
    private Integer jobType;

    /**
     * 必中的题目
     */
    @Column(name = "musttopic")
    private String mustTopic;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTopicType() {
        return topicType;
    }

    public void setTopicType(Integer topicType) {
        this.topicType = topicType;
    }

    public Integer getTopicCount() {
        return topicCount;
    }

    public void setTopicCount(Integer topicCount) {
        this.topicCount = topicCount;
    }

    public Integer getJobType() {
        return jobType;
    }

    public void setJobType(Integer jobType) {
        this.jobType = jobType;
    }

    public String getMustTopic() {
        return mustTopic;
    }

    public void setMustTopic(String mustTopic) {
        this.mustTopic = mustTopic;
    }
}
