package com.example.demo.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Table(name = "sys_interview")
public class SysInterview implements Serializable {
    /**
     * 编号
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 性别：0：女，1：男
     */
    private Boolean sex;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 职位
     */
    private Integer position;

    /**
     * 面试公司
     */
    private String corporation;

    /**
     * 简历渠道
     */
    private String channel;

    /**
     * 0：未预约  1：已预约 2：拒绝 3：失约  4：等通知  5：已结束
     */
    private Integer status;

    /**
     * 0：电话面试  1：面试 ,2:网络面试
     */
    private Integer interview;

    /**
     * 面试阶段：0：初试   1：二面   2：三面
     */
    private Integer stage;

    /**
     * 面试时间
     */
    @Column(name = "interview_time")
    private Date interviewTime;

    /**
     * 面试结果（合适、不合适、已发office、候选人）
     */
    @Column(name = "interview_results")
    private String interviewResults;

    /**
     * 评价
     */
    private String evaluate;

    /**
     * 修改时间
     */
    @Column(name = "update_stamp")
    private Date updateStamp;

    /**
     * 简历文件名
     */
    @Column(name = "file_name")
    private String fileName;

    /**
     * 答题开始时间
     */
    @Column(name = "start_time")
    private Date startTime;

    /**
     * 答题结束时间
     */
    @Column(name = "end_time")
    private Date endTime;

    /**
     * 剩余答题时间
     */
    @Column(name = "surplusTime")
    private String surplustime;

    /**
     * 正在做的页数
     */
    private Integer page;

    /**
     * 获取编号
     *
     * @return id - 编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取姓名
     *
     * @return name - 姓名
     */
    public String getName() {
        return name;
    }

    /**
     * 设置姓名
     *
     * @param name 姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取性别：0：女，1：男
     *
     * @return sex - 性别：0：女，1：男
     */
    public Boolean getSex() {
        return sex;
    }

    /**
     * 设置性别：0：女，1：男
     *
     * @param sex 性别：0：女，1：男
     */
    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    /**
     * 获取年龄
     *
     * @return age - 年龄
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 设置年龄
     *
     * @param age 年龄
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * 获取手机号
     *
     * @return phone - 手机号
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置手机号
     *
     * @param phone 手机号
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 获取职位
     *
     * @return position - 职位
     */
    public Integer getPosition() {
        return position;
    }

    /**
     * 设置职位
     *
     * @param position 职位
     */
    public void setPosition(Integer position) {
        this.position = position;
    }

    /**
     * 获取面试公司
     *
     * @return corporation - 面试公司
     */
    public String getCorporation() {
        return corporation;
    }

    /**
     * 设置面试公司
     *
     * @param corporation 面试公司
     */
    public void setCorporation(String corporation) {
        this.corporation = corporation;
    }

    /**
     * 获取简历渠道
     *
     * @return channel - 简历渠道
     */
    public String getChannel() {
        return channel;
    }

    /**
     * 设置简历渠道
     *
     * @param channel 简历渠道
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * 获取0：未预约  1：已预约 2：拒绝 3：失约  4：等通知  5：已结束
     *
     * @return status - 0：未预约  1：已预约 2：拒绝 3：失约  4：等通知  5：已结束
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设置0：未预约  1：已预约 2：拒绝 3：失约  4：等通知  5：已结束
     *
     * @param status 0：未预约  1：已预约 2：拒绝 3：失约  4：等通知  5：已结束
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取0：电话面试  1：面试 ,2:网络面试
     *
     * @return interview - 0：电话面试  1：面试 ,2:网络面试
     */
    public Integer getInterview() {
        return interview;
    }

    /**
     * 设置0：电话面试  1：面试 ,2:网络面试
     *
     * @param interview 0：电话面试  1：面试 ,2:网络面试
     */
    public void setInterview(Integer interview) {
        this.interview = interview;
    }

    /**
     * 获取面试阶段：0：初试   1：二面   2：三面
     *
     * @return stage - 面试阶段：0：初试   1：二面   2：三面
     */
    public Integer getStage() {
        return stage;
    }

    /**
     * 设置面试阶段：0：初试   1：二面   2：三面
     *
     * @param stage 面试阶段：0：初试   1：二面   2：三面
     */
    public void setStage(Integer stage) {
        this.stage = stage;
    }

    /**
     * 获取面试时间
     *
     * @return interview_time - 面试时间
     */
    public Date getInterviewTime() {
        return interviewTime;
    }

    /**
     * 设置面试时间
     *
     * @param interviewTime 面试时间
     */
    public void setInterviewTime(Date interviewTime) {
        this.interviewTime = interviewTime;
    }

    /**
     * 获取面试结果（合适、不合适、已发office、候选人）
     *
     * @return interview_results - 面试结果（合适、不合适、已发office、候选人）
     */
    public String getInterviewResults() {
        return interviewResults;
    }

    /**
     * 设置面试结果（合适、不合适、已发office、候选人）
     *
     * @param interviewResults 面试结果（合适、不合适、已发office、候选人）
     */
    public void setInterviewResults(String interviewResults) {
        this.interviewResults = interviewResults;
    }

    /**
     * 获取评价
     *
     * @return evaluate - 评价
     */
    public String getEvaluate() {
        return evaluate;
    }

    /**
     * 设置评价
     *
     * @param evaluate 评价
     */
    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }

    /**
     * 获取修改时间
     *
     * @return update_stamp - 修改时间
     */
    public Date getUpdateStamp() {
        return updateStamp;
    }

    /**
     * 设置修改时间
     *
     * @param updateStamp 修改时间
     */
    public void setUpdateStamp(Date updateStamp) {
        this.updateStamp = updateStamp;
    }

    /**
     * 获取简历文件名
     *
     * @return file_name - 简历文件名
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * 设置简历文件名
     *
     * @param fileName 简历文件名
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * 获取答题开始时间
     *
     * @return start_time - 答题开始时间
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * 设置答题开始时间
     *
     * @param startTime 答题开始时间
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     * 获取答题结束时间
     *
     * @return end_time - 答题结束时间
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     * 设置答题结束时间
     *
     * @param endTime 答题结束时间
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     * 获取剩余答题时间
     *
     * @return surplusTime - 剩余答题时间
     */
    public String getSurplustime() {
        return surplustime;
    }

    /**
     * 设置剩余答题时间
     *
     * @param surplustime 剩余答题时间
     */
    public void setSurplustime(String surplustime) {
        this.surplustime = surplustime;
    }

    /**
     * 获取正在做的页数
     *
     * @return page - 正在做的页数
     */
    public Integer getPage() {
        return page;
    }

    /**
     * 设置正在做的页数
     *
     * @param page 正在做的页数
     */
    public void setPage(Integer page) {
        this.page = page;
    }
}