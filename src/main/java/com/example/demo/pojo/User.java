package com.example.demo.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 用户对象
 * @date 2018/5/24 13:09
 */
@Entity(name = "sys_user")
@ApiModel(value = "User", description = "用户信息描述")
public class User implements Serializable {

    @ApiModelProperty(value = "用户编号")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "手机号码")
    private String phone;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "性别，false：女，true：男")
    private Boolean sex;

    @ApiModelProperty(value = "状态，0：可用，1：禁用")
    private Integer status;

    @ApiModelProperty(value = "创建人id")
    @Column(name = "create_people")
    private Long createPeople;

    @ApiModelProperty(value = "修改人id")
    @Column(name = "update_people")
    private Long updatePeople;

    @ApiModelProperty(value = "创建时间")
    @Column(name = "create_stamp")
    private Timestamp createStamp;

    @Column(name = "update_stamp")
    @ApiModelProperty(value = "修改时间")
    private Timestamp updateStamp;

    public Long getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Long createPeople) {
        this.createPeople = createPeople;
    }

    public Long getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Long updatePeople) {
        this.updatePeople = updatePeople;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Timestamp getCreateStamp() {
        return createStamp;
    }

    public void setCreateStamp(Timestamp createStamp) {
        this.createStamp = createStamp;
    }

    public Timestamp getUpdateStamp() {
        return updateStamp;
    }

    public void setUpdateStamp(Timestamp updateStamp) {
        this.updateStamp = updateStamp;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public User() {
    }

    public User(String username, String email, String password, Integer age) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.age = age;
    }
}
