package com.example.demo.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试题映射
 * @date 2018/6/6 17:01
 */
@Entity(name = "sys_interview_answer")
public class InterviewAnswer implements Serializable {

    /**
     * 编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 题目编号
     */
    @Column(name = "topicid")
    private Long topicId;

    /**
     * 状态 0：超时 1：正常 2：错误
     */
    private Integer status;

    /**
     * 答案
     */
    private String answer;

    /**
     * 面试人员编号
     */
    @Column(name = "interviewid")
    private Long interviewId;

    /**
     * 创建时间
     */
    @Column(name = "createstamp")
    private Date createStamp;

    /**
     * 修改时间
     */
    @Column(name = "updatestamp")
    private Date updateStamp;

    /**
     * 答题者的ip
     */
    private String ip;

    /**
     * 题目类型
     */
    private Integer topictype;

    public Integer getTopictype() {
        return topictype;
    }

    public void setTopictype(Integer topictype) {
        this.topictype = topictype;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Date getCreateStamp() {
        return createStamp;
    }

    public void setCreateStamp(Date createStamp) {
        this.createStamp = createStamp;
    }

    public Date getUpdateStamp() {
        return updateStamp;
    }

    public void setUpdateStamp(Date updateStamp) {
        this.updateStamp = updateStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTopicId() {
        return topicId;
    }

    public void setTopicId(Long topicId) {
        this.topicId = topicId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getInterviewId() {
        return interviewId;
    }

    public void setInterviewId(Long interviewId) {
        this.interviewId = interviewId;
    }

    public InterviewAnswer() {

    }

    public InterviewAnswer(Long topicId, String answer, Long interviewId, Date updateStamp, String ip, Integer status) {
        this.topicId = topicId;
        this.answer = answer;
        this.interviewId = interviewId;
        this.updateStamp = updateStamp;
        this.ip = ip;
        this.status = status;
    }

    public InterviewAnswer(Long interviewId) {
        this.interviewId = interviewId;
    }
}
