package com.example.demo.pojo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试题
 * @date 2018/6/6 17:01
 */
@Entity(name = "sys_topic")
public class Topic implements Serializable {

    /**
     * 编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 题目
     **/
    private String name;
    /**
     * 选择答案
     **/
    @Column(name = "selectanswer")
    private String selectAnswer;
    /**
     * 正确答案
     **/
    private String answer;
    /**
     * 题目类型
     **/
    @Column(name = "topictype")
    private Integer topicType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSelectAnswer() {
        return selectAnswer;
    }

    public void setSelectAnswer(String selectAnswer) {
        this.selectAnswer = selectAnswer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getTopicType() {
        return topicType;
    }

    public void setTopicType(Integer topicType) {
        this.topicType = topicType;
    }
}
