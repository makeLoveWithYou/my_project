package com.example.demo.pojo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Description :用户角色关联表
 * ---------------------------------
 * @Author : Ye.Shengguang
 * @Date : 2018/6/3 22:46
 */
@Entity(name = "sys_user_role")
public class UserRole implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "role_id")
    private Long roleId;

    public Long getUserId() {
        return userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole(Long userid, Long roleid) {
        this.userId = userid;
        this.roleId = roleid;
    }

    public UserRole() {
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "userId=" + userId +
                ", roleId=" + roleId +
                '}';
    }
}
