package com.example.demo.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 角色实体类
 * @date 2018/5/24 13:06
 */
@Entity(name = "sys_role")
public class Role implements Serializable {

    /**
     * 角色编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 角色code
     */
    private String code;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 描述
     **/
    @Column(name = "`desc`")
    private String desc;

    /**
     * 创建人
     */
    @Column(name = "create_people")
    private Long createPeople;

    /**
     * 修改人
     */
    @Column(name = "update_people")
    private Long updatePeople;

    /**
     * 创建时间
     */
    @Column(name = "create_stamp")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createStamp;

    /**
     * 修改时间
     */
    @Column(name = "update_stamp")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateStamp;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Long updatePeople) {
        this.updatePeople = updatePeople;
    }

    public Timestamp getCreateStamp() {
        return createStamp;
    }

    public void setCreateStamp(Timestamp createStamp) {
        this.createStamp = createStamp;
    }

    public Timestamp getUpdateStamp() {
        return updateStamp;
    }

    public void setUpdateStamp(Timestamp updateStamp) {
        this.updateStamp = updateStamp;
    }

    public Long getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Long createPeople) {
        this.createPeople = createPeople;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", createPeople=" + createPeople +
                ", updatePeople=" + updatePeople +
                ", createStamp=" + createStamp +
                ", updateStamp=" + updateStamp +
                '}';
    }
}
