package com.example.demo.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @Description :日志实体类
 * ---------------------------------
 * @Author : Ye.Shengguang
 * @Date : 2018/6/2 22:46
 */
@Entity(name = "sys_log")
public class Log implements Serializable{

    /**
     * 日志编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 日志类型id
     */
    @Column(name = "log_type_id")
    private Long logTypeId;

    /**
     * ip
     */
    private String IP;

    /**
     * 日志描述
     */
    @Column(name = "`desc`")
    private String desc;

    /***
     * 创建时间
     */
    @Column(name = "create_stamp")
    private Timestamp createStamp;

    /***
     * 记录日志的用户
     */
    @Column(name = "record_person")
    private Long recordPerson;

    @Override
    public String toString() {
        return "Log{" +
                "id=" + id +
                ", logTypeId=" + logTypeId +
                ", IP='" + IP + '\'' +
                ", desc='" + desc + '\'' +
                ", createStamp=" + createStamp +
                ", recordPerson=" + recordPerson +
                '}';
    }

    public Long getRecordPerson() {
        return recordPerson;
    }

    public void setRecordPerson(Long recordPerson) {
        this.recordPerson = recordPerson;
    }

    public Long getLogTypeId() {
        return logTypeId;
    }

    public void setLogTypeId(Long logTypeId) {
        this.logTypeId = logTypeId;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Timestamp getCreateStamp() {
        return createStamp;
    }

    public void setCreateStamp(Timestamp createStamp) {
        this.createStamp = createStamp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Log() {

    }

    public Log(String desc, Long logTypeId, String IP, Long userid) {
        this.desc = desc;
        this.logTypeId = logTypeId;
        this.IP = IP;
        this.createStamp = new Timestamp(System.currentTimeMillis());
        this.recordPerson = userid;
    }

}
