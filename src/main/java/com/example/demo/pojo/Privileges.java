package com.example.demo.pojo;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 权限实体
 * @date 2018/5/24 13:01
 */
@Entity(name = "sys_privileges")
public class Privileges implements Serializable {

    /**
     * 权限编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 访问地址
     */
    private String url;

    /**
     * 权限名称
     */
    private String name;

    /**
     * 访问类型
     */
    private String type;

    /**
     * 父级权限
     **/
    @Column(name = "parent_id")
    private Long parentId;

    /**
     * 描述
     **/
    @Column(name = "`desc`")
    private String desc;

    /**
     * 权限编码
     **/
    @Column(name = "privilege_code")
    private String privilegeCode;

    /**
     * 创建人
     */
    @Column(name = "create_stamp")
    private Timestamp createStamp;

    /**
     * 创建时间
     */
    @Column(name = "create_people")
    private Long createPeople;

    @Override
    public String toString() {
        return "Privileges{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", parentId=" + parentId +
                ", desc='" + desc + '\'' +
                ", privilegeCode='" + privilegeCode + '\'' +
                ", createStamp=" + createStamp +
                ", createPeople=" + createPeople +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    public Timestamp getCreateStamp() {
        return createStamp;
    }

    public void setCreateStamp(Timestamp createStamp) {
        this.createStamp = createStamp;
    }

    public Long getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Long createPeople) {
        this.createPeople = createPeople;
    }
}
