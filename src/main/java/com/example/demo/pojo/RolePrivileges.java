package com.example.demo.pojo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Description :角色权限关联表
 * ---------------------------------
 * @Author : Ye.Shengguang
 * @Date : 2018/6/3 22:46
 */
@Entity(name = "sys_role_privileges")
public class RolePrivileges implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "privileges_id")
    private Long privilegesId;

    @Column(name = "role_id")
    private Long roleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPrivilegesId() {
        return privilegesId;
    }

    public void setPrivilegesId(Long privilegesId) {
        this.privilegesId = privilegesId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "RolePrivileges{" +
                "privilegesId=" + privilegesId +
                ", roleId=" + roleId +
                '}';
    }
}
