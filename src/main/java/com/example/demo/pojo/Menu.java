package com.example.demo.pojo;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @Description :菜单实体类
 * ---------------------------------
 * @Author : Ye.Shengguang
 * @Date : 2018/6/2 22:46
 */
@Entity(name = "sys_menu")
public class Menu implements Serializable {

    /**
     * 菜单编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 菜单地址
     */
    private String url;

    /**
     * 菜单图标
     */
    private String img;

    /***
     * 父级菜单
     */
    @Column(name = "parent_id")
    private String parentId;

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", img='" + img + '\'' +
                ", parentId='" + parentId + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
