package com.example.demo.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * @Description :日志类型实体类
 * ---------------------------------
 * @Author : Ye.Shengguang
 * @Date : 2018/6/2 22:46
 */
@Entity(name = "sys_log_type")
public class LogType implements Serializable {

    /**
     * 日志类型编号
     **/
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 类型文本
     */
    private String value;

    @Override
    public String toString() {
        return "LogType{" +
                "id=" + id +
                ", value='" + value + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
