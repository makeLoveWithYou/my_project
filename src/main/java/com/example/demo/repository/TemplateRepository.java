package com.example.demo.repository;

import com.example.demo.pojo.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 模板
 * @date 2018/6/20 10:34
 */
public interface TemplateRepository extends JpaRepository<Template, Long>, JpaSpecificationExecutor<Template> {


}
