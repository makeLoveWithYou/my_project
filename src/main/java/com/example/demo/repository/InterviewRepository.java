package com.example.demo.repository;

import com.example.demo.pojo.Interview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试功能后端操作
 * @date 2018/6/1 16:51
 */
public interface InterviewRepository extends JpaRepository<Interview, Long>, JpaSpecificationExecutor<Interview> {



}
