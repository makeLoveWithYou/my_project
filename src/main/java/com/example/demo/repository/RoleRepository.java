package com.example.demo.repository;

import com.example.demo.pojo.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 角色后端操作
 * @date 2018/6/1 16:50
 */
public interface RoleRepository extends JpaRepository<Role, Long>, JpaSpecificationExecutor<Role> {

}
