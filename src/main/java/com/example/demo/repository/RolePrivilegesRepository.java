package com.example.demo.repository;

import com.example.demo.pojo.RolePrivileges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 角色权限后端操作
 * @date 2018/6/1 16:50
 */
public interface RolePrivilegesRepository extends JpaRepository<RolePrivileges, Long>, JpaSpecificationExecutor<RolePrivileges> {

}
