package com.example.demo.repository;

import com.example.demo.pojo.Log;
import com.example.demo.pojo.Privileges;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 日志后端操作
 * @date 2018/6/1 16:51
 */
public interface LogRepository extends JpaRepository<Log, Long>, JpaSpecificationExecutor<Log> {

}
