package com.example.demo.repository;

import com.example.demo.pojo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 用户仓库
 * @date 2018/5/24 13:13
 */
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    /**
     * 根据用户编号删除用户
     *
     * @param userid
     */
    @Override
    void deleteById(Long userid);

    /**
     * 修改、保存用户信息
     *
     * @param s
     * @param <S>
     * @return
     */
    @Override
    <S extends User> S saveAndFlush(S s);

    /**
     * 查询用户信息
     *
     * @param specification
     * @param pageable
     * @return
     */
    @Override
    Page<User> findAll(Specification<User> specification, Pageable pageable);

    /**
     * 根据用户编号查询用户信息
     *
     * @param id
     * @return
     */
    @Override
    User getOne(Long id);

}
