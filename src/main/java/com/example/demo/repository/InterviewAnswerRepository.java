package com.example.demo.repository;

import com.example.demo.pojo.InterviewAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试题映射
 * @date 2018/6/1 16:51
 */
public interface InterviewAnswerRepository extends JpaRepository<InterviewAnswer, Long>, JpaSpecificationExecutor<InterviewAnswer> {


}
