package com.example.demo.repository;

import com.example.demo.pojo.LogType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 日志类型后端操作
 * @date 2018/6/1 16:51
 */
public interface LogTypeRepository extends JpaRepository<LogType, Long>, JpaSpecificationExecutor<LogType> {

}
