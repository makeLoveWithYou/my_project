package com.example.demo.config;

import com.example.demo.pojo.Role;
import com.example.demo.pojo.User;
import com.example.demo.service.PrivilegesService;
import com.example.demo.service.RoleService;
import com.example.demo.service.UserService;
import com.example.demo.util.StringUtil;
import org.apache.log4j.Logger;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.context.annotation.Lazy;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 登陆配置
 * @date 2018/6/1 16:17
 */
public class UserRealm extends AuthorizingRealm {

    @Resource(name = "userService")
    @Lazy
    private UserService userService;

    @Resource(name = "privilegesService")
    @Lazy
    private PrivilegesService privilegesService;

    @Resource(name = "roleService")
    @Lazy
    private RoleService roleService;
    private Logger logger = Logger.getLogger(UserRealm.class);

    /**
     * 提供用户信息，返回权限信息
     *
     * @param principals
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        User user = (User) principals.getPrimaryPrincipal();
        List<Role> roles = roleService.findByUserId(user.getId());
        Set<String> roleSet = new HashSet<>();
        Set<String> privilegeSet = new HashSet<>();
        for (Role item : roles) {
            roleSet.add(item.getCode());
            privilegesService.findByRoleId(item.getId()).forEach(privileges -> privilegeSet.add(privileges.getPrivilegeCode()));
        }
        // 将角色编码组成的Set提供给授权info
        authorizationInfo.setRoles(roleSet);
        // 将权限编码组成的Set提供给info
        authorizationInfo.setStringPermissions(privilegeSet);
        return authorizationInfo;
    }

    /**
     * 提供帐户信息，返回认证信息
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String userName = (String) authenticationToken.getPrincipal();
        User user = userService.findUserIdByName(userName);
        if (user == null) {
            //用户不存在就抛出异常
            throw new UnknownAccountException();
        }
        if (user.getStatus() == 1) {
            //用户被锁定就抛异常
            throw new LockedAccountException();
        }
        //密码可以通过SimpleHash加密，然后保存进数据库。
        //此处是获取数据库内的账号、密码、盐值，保存到登陆信息info中
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(user,
                StringUtil.getData(user.getPassword()), getName());                   //realm name
        return authenticationInfo;
    }
}