package com.example.demo.util;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 判断分页
 * @date 2018/6/27 15:36
 */
public class PageUtil {

    /**
     * 判断当前页数是否和上一次一样
     *
     * @return
     */
    public static int isPage(int currentPage, HttpServletRequest request) {
        if (SessionUtil.getPage(request.getSession()) != null) {//判断是否存储了上一页
            int lastPage = (int) SessionUtil.getPage(request.getSession());//获得上一次页数
            if (currentPage < lastPage) {//当前页数如果小于上一次页数则继续返回上一次页数，否则继续下一页
                return lastPage;
            }
        }
        return currentPage;
    }

}
