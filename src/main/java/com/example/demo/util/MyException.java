package com.example.demo.util;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 自定义异常
 * @date 2018/6/28 13:49
 */
public class MyException extends Exception {

    private String message;//消息

    public MyException() {

    }

    public MyException(String s) {
        super(s);
        this.message = s;
    }

    @Override
    public String getMessage() {
        return message;
    }
}