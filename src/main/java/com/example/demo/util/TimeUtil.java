package com.example.demo.util;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 时间工具类
 * @date 2018/6/27 16:02
 */
public class TimeUtil {


    /**
     * 分钟减去
     *
     * @return
     */
    public static long minuteSubtract(Date date, int time) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(Calendar.MINUTE, time - (time * 2));
        return instance.getTimeInMillis();
    }

}
