package com.example.demo.util;

import com.example.demo.common.MapKeys;

import javax.servlet.http.HttpSession;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: session工具
 * @date 2018/6/27 15:10
 */
public class SessionUtil {

    /**
     * 获得初始化时间
     *
     * @param session
     * @return
     */
    public static Object getInitTime(HttpSession session) {
        return session.getAttribute(MapKeys.INITTIME);
    }

    /**
     * 设置初始化时间
     *
     * @param value
     * @param session
     * @return
     */
    public static void setInitTime(Object value, HttpSession session) {
        session.setAttribute(MapKeys.INITTIME, value);
    }

    /**
     * 获得当前页数
     *
     * @param session
     * @return
     */
    public static Object getPage(HttpSession session) {
        return session.getAttribute(MapKeys.PAGE);
    }

    /**
     * 设置初当前页数
     *
     * @param value
     * @param session
     * @return
     */
    public static void setPage(Object value, HttpSession session) {
        session.setAttribute(MapKeys.PAGE, value);
    }

    /**
     * 获得上一次开始时间
     *
     * @param session
     * @return
     */
    public static Object getLastTime(HttpSession session) {
        return session.getAttribute(MapKeys.LASTTIME);
    }

    /**
     * 设置上一次开始时间
     *
     * @param value
     * @param session
     * @return
     */
    public static void setLastTime(Object value, HttpSession session) {
        session.setAttribute(MapKeys.LASTTIME, value);
    }

    /**
     * 查看是否是最后一道题目
     *
     * @param session
     * @return
     */
    public static Object getLast(HttpSession session) {
        return session.getAttribute(MapKeys.LAST);
    }

    /**
     * 设置是否是最后一道题目
     *
     * @param value
     * @param session
     * @return
     */
    public static void setLast(Object value, HttpSession session) {
        session.setAttribute(MapKeys.LAST, value);
    }

    /**
     * 查看超时时间
     *
     * @param session
     * @return
     */
    public static Object getTimeout(HttpSession session) {
        return session.getAttribute(MapKeys.TIMEOUT);
    }

    /**
     * 设置超时时间
     *
     * @param value
     * @param session
     * @return
     */
    public static void setTimeout(Object value, HttpSession session) {
        session.setAttribute(MapKeys.TIMEOUT, value);
    }

    /**
     * 获得当前登陆用户
     *
     * @param session
     * @return
     */
    public static Object getUser(HttpSession session) {
        return session.getAttribute(MapKeys.USER);
    }

    /**
     * 设置当前登陆用户
     *
     * @param value
     * @param session
     * @return
     */
    public static void setUser(Object value, HttpSession session) {
        session.setAttribute(MapKeys.USER, value);
    }

    /**
     * 一次设置多个value
     *
     * @param lastTime 上次答题时间
     * @param user     用户
     * @param session  session对象
     */
    public static void set(Object lastTime, Object user, HttpSession session) {
        setLastTime(lastTime, session);
        setUser(user, session);
    }

    /**
     * 添加value
     */
    public static void set(String key, Object value, HttpSession session) {
        session.setAttribute(key, value);
    }

    /**
     * 获得value
     */
    public static Object get(String key, HttpSession session) {
        return session.getAttribute(key);
    }

    /**
     * 清楚数据
     *
     * @param key
     * @param session
     * @return
     */
    public static void remove(String key, HttpSession session) {
        session.removeAttribute(key);
    }

}
