package com.example.demo.util;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class UploadUtil {

    /**
     * 文件上传
     *
     * @param file
     * @param fileName
     * @param savePath
     * @return
     * @throws IOException
     */
    public static String uploadFile(MultipartFile file, String fileName, String savePath) throws IOException {
        String filename = null;
        // 生成文件名并保存到本地
        filename = file.getOriginalFilename();
        filename = fileName + filename.substring(filename.lastIndexOf('.'), filename.length());
        File filepath = new File(savePath);
        if (!filepath.exists()) filepath.mkdirs();
        file.transferTo(new File(savePath + filename));
        return filename;
    }

}
