package com.example.demo.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 异步执行工具类
 * @date 2018/6/5 13:41
 */
public class ThreadUtil {

    public static ExecutorService executorService = Executors.newFixedThreadPool(100);

}
