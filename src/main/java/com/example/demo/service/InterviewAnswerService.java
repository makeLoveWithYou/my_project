package com.example.demo.service;

import com.example.demo.dao.InterviewAnswerDao;
import com.example.demo.dto.ResultDto;
import com.example.demo.dto.TopicAnswerDto;
import com.example.demo.pojo.InterviewAnswer;
import com.example.demo.pojo.Template;
import com.example.demo.pojo.Topic;
import com.example.demo.repository.InterviewAnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试提映射业务层
 * @date 2018/6/1 16:51
 */
@Service
public class InterviewAnswerService {

    @Autowired
    private InterviewAnswerRepository interviewAnswerRepository;

    @Autowired
    private InterviewAnswerDao interviewDao;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private TopicService topicService;

    /**
     * 获得面试题
     *
     * @param pageable
     * @return
     */
    public ResultDto find(InterviewAnswer interviewAnswer, Pageable pageable, boolean bol) {
        return interviewDao.find(interviewAnswer.getInterviewId(), interviewAnswer.getTopicId(), pageable.getPageNumber(), pageable.getPageSize(), bol);
    }

    /**
     * 新增
     *
     * @return
     */
    public List<InterviewAnswer> save(List<InterviewAnswer> list) {
        return interviewAnswerRepository.saveAll(list);
    }

    /**
     * 抽取面试题插入到映射题库
     */
    public void save(Integer jobType, Long interviewId) {
        Template template = new Template();
        template.setJobType(jobType);
        List<Template> templates = templateService.find(template);
        List<Topic> topics = topicService.findTopic(templates);
        List<InterviewAnswer> list = new ArrayList<>();
        topics.forEach(item -> {
            InterviewAnswer interviewAnswer = new InterviewAnswer();
            interviewAnswer.setInterviewId(interviewId);
            interviewAnswer.setTopicId(item.getId());
            interviewAnswer.setTopictype(item.getTopicType());
            Date date = new Date();
            interviewAnswer.setCreateStamp(date);
            interviewAnswer.setUpdateStamp(date);
            list.add(interviewAnswer);
        });
        this.save(list);
    }

    /**
     * 统计考试数据
     *
     * @param interviewId
     * @return
     */
    public TopicAnswerDto statistics(Long interviewId) {
        return interviewDao.statistics(interviewId);
    }

    /**
     * 修改
     *
     * @return
     */
    @Transactional
    public boolean update(InterviewAnswer interviewAnswer) {
        return interviewDao.update(interviewAnswer);
    }

    /**
     * 删除
     *
     * @param id
     */
    public void delete(Long id) {
        interviewAnswerRepository.deleteById(id);
    }

}
