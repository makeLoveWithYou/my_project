package com.example.demo.service;

import com.example.demo.common.MapKeys;
import com.example.demo.dto.ResultDto;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.params.MapSolrParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: Solr基本接口定义实现
 * @date 2018/5/30 14:01
 */
@Service("solrService")
public class SolrService {

    @Autowired
    private SolrClient solrClient;

    /**
     * 删除索引
     *
     * @param params 删除参数，例子：
     *               params = id:1
     *               params = username=XXX
     *               params = 1
     * @return
     */
    public ResultDto deleteIndexService(String params) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        //删除索引
        try {
            solrClient.deleteByQuery(params);
            solrClient.commit();
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            dto.setError("删除索引失败");
        }
        return dto;
    }

    /**
     * 根据id集合批量删除索引
     *
     * @param ids
     * @return
     */
    public ResultDto deleteIndexService(List<String> ids) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        //删除索引
        try {
            solrClient.deleteById(ids);
            solrClient.commit();
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            dto.setError("删除索引失败");
        }
        return dto;
    }


    //TODO 创建索引
    public ResultDto createIndexOneService(List<?> users) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        //创建索引
        // TODO 1.自定义键创建索引
        /*SolrInputDocument solrInputFields = new SolrInputDocument();
        solrInputFields.addField("id", 2);
        solrInputFields.addField("username", "二狗");
        solrInputFields.addField("password", "123");
        solrInputFields.addField("email", "xxx@xx.com");
        solrInputFields.addField("age", 32);
        solrInputFields.addField("desc", "管理员");*/
        try {
//            solrClient.add(solrInputFields);
            // TODO 2.基于实体类创建索引（推荐此方式）
            solrClient.addBeans(users);
            solrClient.commit();
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            dto.setError("创建索引失败.");
        }
        return dto;
    }

    //TODO 第一种查询索引方式  （需要自己根据键获得值，不方便键维护，不推荐使用）
    public ResultDto queryOneService(String params, String fl) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        //TODO 使用Map设置查询条件
        Map<String, String> map = new Hashtable<>();
        map.put(MapKeys.Q, params);
        map.put(MapKeys.FL, fl);
        try {
            QueryResponse response = solrClient.query(new MapSolrParams(map));
            //TODO 第一种获得方式
            SolrDocumentList list = response.getResults();
            list.forEach(item -> {
                System.err.println(item.getFieldValue("id"));
                System.err.println(item.getFieldValue("username"));
                System.err.println(item.getFieldValue("password"));
                System.err.println(item.getFieldValue("email"));
                System.err.println(item.getFieldValue("age"));
                System.err.println(item.getFieldValue("desc"));
            });
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            dto.setError("查询索引异常.");
        }
        return dto;
    }

    /**
     * TODO 第二种查询索引方式  (注：实体类字段类型必须完全跟solr索引字段一模一样)
     *
     * @param params  查询条件    例子:
     *                params=id:1 或者  id:1 AND username:XXX
     * @param fl      显示的字段   例子：id,username,email
     * @param entitys 接收数据的实体
     * @return
     */
    public ResultDto queryTwoService(String params, String fl, Pageable pageable, String sort, Class<?> entitys) {
        ResultDto dto = new ResultDto();
        dto.errorStatus();
        //TODO 使用SolrQuery设置查询条件
        SolrQuery query = new SolrQuery(params);
        query.setStart(((pageable.getPageNumber() <= 0 ? 0 : pageable.getPageNumber() - 1)) * pageable.getPageSize());
        query.setRows(pageable.getPageSize());
        //截取排序字符串如：id,desc 替换为 id desc  (写了排序字段不写排序类型则默认升序)
        if (!StringUtils.isEmpty(sort)) {
            if (sort.indexOf(",") != -1) sort = sort.replace(',', ' ');
            else sort += " asc";
        }
        query.set("sort", sort);
        query.set(MapKeys.FL, fl);
        try {
            QueryResponse response = solrClient.query(query);
            //TODO 第二种获得方式
            dto.setRows(response.getBeans(entitys));
            dto.setTotal((int) response.getResults().getNumFound());
            dto.successStatus();
        } catch (Exception e) {
            e.printStackTrace();
            dto.setError("查询索引异常.");
        }
        return dto;
    }

}
