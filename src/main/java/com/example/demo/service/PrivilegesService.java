package com.example.demo.service;

import com.example.demo.common.MapKeys;
import com.example.demo.dao.PrivilegesDao;
import com.example.demo.dto.PrivilegesDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.pojo.Privileges;
import com.example.demo.pojo.User;
import com.example.demo.repository.PrivilegesRepository;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 权限业务层
 * @date 2018/6/1 16:51
 */
@Service("privilegesService")
public class PrivilegesService {

    @Autowired
    private PrivilegesRepository privilegesRepository;

    @Autowired
    private PrivilegesDao privilegesDao;

    /**
     * 查询权限信息
     *
     * @param privileges
     * @param pageable
     * @return
     */
    @Cacheable(value = "privileges", key = "'findPrivilegesService_'+#privileges.privilegeCode+#privileges.createPeople+#privileges.parentId+#pageable.pageNumber+#pageable.pageSize")
    public ResultDto findPrivilegesService(Privileges privileges, Pageable pageable, String sort) {
        return privilegesDao.findPrivileges(privileges, sort, pageable.getPageNumber(), pageable.getPageSize());
    }

    /**
     * 根据角色编号查询权限
     *
     * @param roleid
     * @return
     */
    @Cacheable(value = "privileges", key = "'findByRoleId_'+#roleid")
    public List<Privileges> findByRoleId(Long roleid) {
        return privilegesDao.findByRoleId(roleid);
    }

    /**
     * 权限详情
     *
     * @param id
     * @return
     */
    @Cacheable(value = "privileges", key = "'getPrivilegesService_'+#id")
    public PrivilegesDto getPrivilegesService(Long id) {
        return privilegesDao.getPrivilegesDao(id);
    }

    /**
     * 权限资源添加
     *
     * @param privileges
     * @return
     */
    @CacheEvict(value = "privileges", allEntries = true)
    public Privileges addPrivileges(Privileges privileges) {
        //设置Code值
        privileges.setPrivilegeCode(privilegesDao.findMaxCode(privileges.getParentId()));
        privileges.setCreatePeople(((User) SecurityUtils.getSubject().getPrincipal()).getId());
        privileges.setCreateStamp(new Timestamp(System.currentTimeMillis()));
        return this.privilegesRepository.saveAndFlush(privileges);
    }

    /**
     * 权限资源修改
     *
     * @param privileges
     * @return
     */
    @CacheEvict(value = "privileges", allEntries = true)
    @Transactional
    public boolean updatePrivileges(Privileges privileges) {
        return this.privilegesDao.updatePrivileges(privileges);
    }

    /**
     * 权限删除
     *
     * @param privilegesid
     */
    @CacheEvict(value = "privileges", allEntries = true)
    public String delPrivileges(Long privilegesid) {
        //查看权限是否被角色绑定
        if (privilegesDao.findPrivilegesRole(privilegesid)) {
            //查看权限是否有子权限绑定
            if (privilegesDao.findPrivilegesSonPrivilieges(privilegesid)) {
                privilegesRepository.deleteById(privilegesid);
                return null;
            } else
                return MapKeys.SON;
        } else {
            return MapKeys.BIND;
        }
    }

}
