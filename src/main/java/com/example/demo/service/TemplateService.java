package com.example.demo.service;

import com.example.demo.dao.impl.TemplateDaoImpl;
import com.example.demo.pojo.Template;
import com.example.demo.repository.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 模板业务层
 * @date 2018/6/1 16:51
 */
@Service("templateService")
public class TemplateService {

    @Autowired
    private TemplateRepository templateRepository;

    @Autowired
    private TemplateDaoImpl templateDao;

    /**
     * 查询
     *
     * @param template
     * @param pageable
     * @return
     */
    public Page<Template> find(Template template, Pageable pageable) {
        return templateRepository.findAll(((root, criteriaQuery, criteriaBuilder) -> {
            //设置条件查询
            List<Predicate> li = new ArrayList<>();
            if (!StringUtils.isEmpty(template.getJobType()))
                li.add(criteriaBuilder.equal(root.get("jobType").as(Integer.class), template.getJobType()));
            if (!StringUtils.isEmpty(template.getTopicType()))
                li.add(criteriaBuilder.equal(root.get("topicType").as(Integer.class), template.getTopicType()));
            if (!StringUtils.isEmpty(template.getId()))
                li.add(criteriaBuilder.equal(root.get("id").as(Integer.class), template.getId()));
            return criteriaBuilder.and(li.toArray(new Predicate[li.size()]));
        }), pageable);
    }

    /**
     * 查询
     *
     * @param template
     * @return
     */
    public List<Template> find(Template template) {
        return templateRepository.findAll(((root, criteriaQuery, criteriaBuilder) -> {
            //设置条件查询
            List<Predicate> li = new ArrayList<>();
            if (!StringUtils.isEmpty(template.getJobType()))
                li.add(criteriaBuilder.equal(root.get("jobType").as(Integer.class), template.getJobType()));
            if (!StringUtils.isEmpty(template.getTopicType()))
                li.add(criteriaBuilder.equal(root.get("topicType").as(Integer.class), template.getTopicType()));
            return criteriaBuilder.and(li.toArray(new Predicate[li.size()]));
        }));
    }

    /**
     * 新增
     */
    public Template save(Template template) {
        return templateRepository.saveAndFlush(template);
    }

    /**
     * 修改
     */
    @Transactional
    public boolean update(Template template) {
        return templateDao.update(template);
    }

    /**
     * 详情
     */
    public Template get(Long id) {
        return templateRepository.getOne(id);
    }

    /**
     * 删除
     */
    public void delete(Long id) {
        templateRepository.deleteById(id);
    }

}
