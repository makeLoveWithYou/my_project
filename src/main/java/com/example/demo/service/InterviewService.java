package com.example.demo.service;

import com.example.demo.dao.InterviewDao;
import com.example.demo.dto.InterviewDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.form.InterviewForm;
import com.example.demo.pojo.Interview;
import com.example.demo.repository.InterviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试业务层
 * @date 2018/6/1 16:51
 */
@Service("interviewService")
public class InterviewService {

    @Autowired
    private InterviewRepository interviewRepository;

    @Autowired
    private InterviewDao interviewDao;


    public ResultDto findInterviewService(InterviewForm interviewForm, Integer page, Integer size, String sort) {
        return interviewDao.findInterview(interviewForm, sort, page, size);
    }

    /**
     * 查询面试人员信息，显示在日历上
     *
     * @param interviewForm
     * @param pageable
     * @return
     */
    //@Cacheable(value = "findInterviewServiceAndStatus", key = "'findInterviewServiceAndStatus_'+#pageable+#interviewForm.timeInt+#interviewForm.name+#interviewForm.position+#interviewForm.sex+#interviewForm.status+#interviewForm.interview+#interviewForm.interviewResults")
    public Page<Interview> findInterviewServiceAndStatus(InterviewForm interviewForm, Pageable pageable) {
        return interviewRepository.findAll(((root, criteriaQuery, criteriaBuilder) -> {
            //设置条件查询
            List<Predicate> li = new ArrayList<>();
            li.add(criteriaBuilder.equal(root.get("status").as(Integer.class), 1));
            return criteriaBuilder.and(li.toArray(new Predicate[li.size()]));
        }), pageable);
    }


    /**
     * 面试信息详情
     */
//    @Cacheable(value = "interview", key = "'getInterview_'+#id")
    public InterviewDto getInterviewService(Long id) {
        return interviewDao.getInterviewDao(id);
    }

    /**
     * 根据手机号获取面试信息详情
     */
    //@Cacheable(value = "interview", key = "'getInterview_'+#phone")
    public InterviewDto getInterviewService(String phone) {
        return interviewDao.getInterviewDao(phone);
    }

    /**
     * 面试信息添加
     */
    //@CacheEvict(value = "interview", allEntries = true)
    public Interview saveInterviewService(Interview interview) {
        interview.setUpdateStamp(new Timestamp(System.currentTimeMillis()));
        return interviewRepository.saveAndFlush(interview);
    }

    /**
     * 面试信息删除
     */
    @CacheEvict(value = "interview", allEntries = true)
    public void delInerviewService(Long id) {
        interviewRepository.deleteById(id);
    }

    /**
     * 面试信息修改
     */
    @Transactional
    @CacheEvict(value = "interview", allEntries = true)
    public boolean updateInerviewService(InterviewDto interview) {
        interview.setUpdateStamp(new Timestamp(System.currentTimeMillis()));
        return interviewDao.updateInterview(interview);
    }


}
