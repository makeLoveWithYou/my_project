package com.example.demo.service;

import com.example.demo.pojo.Log;
import com.example.demo.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 日志业务层
 * @date 2018/6/1 16:51
 */
@Service("logService")
public class LogService {

    @Autowired
    private LogRepository logRepository;

    /**
     * 插入日志信息
     *
     * @param log
     */
    @CacheEvict(value = "log", allEntries = true)
    public void saveLog(Log log) {
        logRepository.saveAndFlush(log);
    }

}
