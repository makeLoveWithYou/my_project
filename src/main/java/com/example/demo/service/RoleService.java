package com.example.demo.service;

import com.example.demo.dao.RoleDao;
import com.example.demo.dto.ResultDto;
import com.example.demo.dto.RoleDto;
import com.example.demo.pojo.Role;
import com.example.demo.pojo.RolePrivileges;
import com.example.demo.pojo.User;
import com.example.demo.repository.RolePrivilegesRepository;
import com.example.demo.repository.RoleRepository;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 角色业务层
 * @date 2018/6/1 17:05
 */
@Service("roleService")
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RolePrivilegesRepository rolePrivilegesRepository;

    @Autowired
    private RoleDao roleDao;

    /**
     * @Description: 根据用户编号查询角色
     * @author ShengGuang.Ye
     * @date 2018/6/1 17:07
     * @version V1.0
     */
    @Cacheable(value = "role", key = "'findByUserId_'+#userid")
    public List<Role> findByUserId(Long userid) {
        return roleDao.findByUserId(userid);
    }

    /**
     * 查询角色信息
     *
     * @param role
     * @param pageable
     * @return
     */
    @Cacheable(value = "role", key = "'findRoleService_'+#userid+#role.createPeople+#pageable.pageNumber+#pageable.pageSize+#sort")
    public ResultDto findRoleService(Role role, String sort, Pageable pageable, Long userid) {
        return roleDao.findRoleDao(role, pageable.getPageNumber(), pageable.getPageSize(), sort, userid);
    }

    /**
     * 添加角色
     *
     * @param role
     * @return
     */
    @CacheEvict(value = "role", allEntries = true)
    public Role addRoleService(Role role) {
        role.setCode(String.valueOf((roleDao.findMaxCode() + 1)));
        role.setCreateStamp(new Timestamp(System.currentTimeMillis()));
        role.setCreatePeople(((User) SecurityUtils.getSubject().getPrincipal()).getId());
        return roleRepository.saveAndFlush(role);
    }

    /**
     * 修改角色
     *
     * @param role
     * @return
     */
    @CacheEvict(value = "role", allEntries = true)
    @Transactional
    public boolean updateRoleService(Role role, List<Long> ids) {
        role.setUpdateStamp(new Timestamp(System.currentTimeMillis()));
        role.setUpdatePeople(((User) SecurityUtils.getSubject().getPrincipal()).getId());
        return roleDao.updateRoleDao(role);
    }

    /**
     * 添加角色权限关联表信息
     *
     * @param pids
     * @param roleid
     */
    public void saveRolePrivileges(String[] pids, Long roleid) {
        //先删除拥有的权限在去添加或者修改权限
        roleDao.delRolePrivilege(roleid);
        for (int i = 0; i < pids.length; i++) {
            RolePrivileges rolep = new RolePrivileges();
            rolep.setRoleId(roleid);
            rolep.setPrivilegesId(Long.valueOf(pids[i]));
            rolePrivilegesRepository.saveAndFlush(rolep);
        }
    }


    /**
     * 角色详情
     *
     * @param roleid
     * @return
     */
    @Cacheable(value = "role", key = "'getRoleService_'+#roleid")
    public RoleDto getRoleService(Long roleid) {
        return roleDao.getRoleDao(roleid);
    }

    /**
     * 删除角色
     *
     * @param roleid
     * @return 删除结果，为空正常，否则返回异常信息
     **/
    @CacheEvict(value = "role", allEntries = true)
    @Transactional
    public String delRoleService(Long roleid) {
        //判断角色是否在使用
        if (roleDao.findRoleUser(roleid)) {
            //删除中间表
            roleDao.delRolePrivilege(roleid);
            //删除角色表
            roleRepository.deleteById(roleid);
            return null;
        } else {
            return "有用户绑定了该角色，请先修改用户绑定的角色";
        }
    }
}
