package com.example.demo.service;

import com.example.demo.dao.TopicDao;
import com.example.demo.pojo.Template;
import com.example.demo.pojo.Topic;
import com.example.demo.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试业务层
 * @date 2018/6/1 16:51
 */
@Service("topicService")
public class TopicService {

    @Autowired
    private TopicRepository topicRepository;

    @Autowired
    private TopicDao topicDao;




    /**
     * 抽取面试题
     *
     * @return
     */
    public List<Topic> findTopic(List<Template> list) {
        return topicDao.findTopic(list);
    }

    /**
     * 从赛选好的数据中获取面试题
     *
     * @param id
     * @return
     */
    public Page<Topic> findTopic(Long id, Integer page) {
        return null;
    }


    /**
     * 新增
     * @param topic
     * @return
     */
    public Topic save(Topic topic) {
        return topicRepository.saveAndFlush(topic);
    }


    /**
     * 分页查询
     * @param topic
     * @param pageable
     * @return
     */
    public Page<Topic> find(Topic topic, Pageable pageable) {
        return topicRepository.findAll(((root, criteriaQuery, criteriaBuilder) -> {
            //设置条件查询
            List<Predicate> li = new ArrayList<>();
            if (!StringUtils.isEmpty(topic.getName()))
                li.add(criteriaBuilder.equal(root.get("name").as(String.class), topic.getName()));
            if (!StringUtils.isEmpty(topic.getTopicType()))
                li.add(criteriaBuilder.equal(root.get("topicType").as(Integer.class), topic.getTopicType()));
            if (!StringUtils.isEmpty(topic.getId()))
                li.add(criteriaBuilder.equal(root.get("id").as(Long.class), topic.getId()));
            return criteriaBuilder.and(li.toArray(new Predicate[li.size()]));
        }), pageable);
    }


    /**
     * 修改
     * @param topic
     * @return
     */
    @Transactional
    public boolean update(Topic topic) {
        return topicDao.update(topic);
    }

    /**
     * 详情
     */
    public Topic get(Long id) {
        return topicRepository.getOne(id);
    }

    /**
     * 删除
     */
    public void delete(Long id) {
        topicRepository.deleteById(id);
    }
}
