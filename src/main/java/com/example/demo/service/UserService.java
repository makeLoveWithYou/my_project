package com.example.demo.service;

import com.example.demo.dao.RoleDao;
import com.example.demo.dao.UserDao;
import com.example.demo.dto.ResultDto;
import com.example.demo.dto.UserDto;
import com.example.demo.pojo.Role;
import com.example.demo.pojo.User;
import com.example.demo.pojo.UserRole;
import com.example.demo.repository.UserRepository;
import com.example.demo.repository.UserRoleRepository;
import com.example.demo.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 用户业务层
 * @date 2018/5/24 13:14
 */
@Service(value = "userService")
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private UserDao userDao;

    /**
     * @Description: 根据用户名|或者手机号获取用户信息
     * @author ShengGuang.Ye
     * @date 2018/6/1 17:19
     * @version V1.0
     */
    public User findUserIdByName(String username) {
        List<User> users = userRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
            //设置条件查询
            List<Predicate> li = new ArrayList<>();
            if (!StringUtils.isEmpty(username)) {
                li.add(criteriaBuilder.or(criteriaBuilder.equal(root.get("username").as(String.class), username),
                        criteriaBuilder.equal(root.get("phone").as(String.class), username)));
            }
            return criteriaBuilder.and(li.toArray(new Predicate[li.size()]));
        });
        return (users == null || users.size() < 1) ? null : users.get(0);
    }

    /**
     * 对象关联查询用户信息
     *
     * @param username
     * @param email
     * @param pageable
     * @return 返回分页用户集
     */
    /*@Cacheable(value = "user", keyGenerator = "wiselyKeyGenerator")
    public Page<User> findUserService(String username, String email, Pageable pageable) {
        return userRepository.findAll((root, criteriaQuery, criteriaBuilder) -> {
            //设置条件查询
            List<Predicate> li = new ArrayList<>();
            if (!StringUtils.isEmpty(username))
                li.add(criteriaBuilder.like(root.get("username").as(String.class), '%' + username + '%'));
            if (!StringUtils.isEmpty(email))
                li.add(criteriaBuilder.equal(root.get("email").as(String.class), email));
            return criteriaBuilder.and(li.toArray(new Predicate[li.size()]));
        }, pageable);
    }*/

    /**
     * 自定义查询用户信息n
     *
     * @param pageable
     * @return 返回分页用户集
     */
    @Cacheable(value = "user", key = "'findUser_'+#sort+#userid")
    public ResultDto findUserServiceTwo(User user, Long userid, String sort, Pageable pageable) {
        user.setId(((User) SecurityUtils.getSubject().getPrincipal()).getId());
        return userDao.findUserDao(user, pageable.getPageNumber(), pageable.getPageSize(), sort);
    }

    /**
     * 用户信息添加
     *
     * @param user
     * @return
     */
    @CacheEvict(value = "user", allEntries = true)
    public User saveUserService(User user, Long roleid) {
        user.setCreateStamp(new Timestamp(System.currentTimeMillis()));
        user.setCreatePeople(((User) SecurityUtils.getSubject().getPrincipal()).getId());
        if (!StringUtils.isEmpty(user.getPassword())) user.setPassword(StringUtil.getData(user.getPassword()));
        user = userRepository.saveAndFlush(user);
        userRoleRepository.saveAndFlush(new UserRole(user.getId(), roleid));
        return user;
    }

    /**
     * 根据用户编号获得用户信息
     *
     * @param id
     * @return
     */
    @Cacheable(value = "user", key = "'getUser_'+#id")
    public UserDto getUserService(Long id) {
        UserDto userDao = this.userDao.getUserDao(id);
        userDao.setPassword(StringUtil.getData(userDao.getPassword()));
        List<Role> roles = roleDao.findByUserId(id);
        if (roles != null && roles.size() > 0) userDao.setRole(roles.get(0));
        return userDao;
    }

    /**
     * 用户信息修改
     *
     * @param user
     * @return
     */
    @CacheEvict(value = "user", allEntries = true)
    @Transactional
    public boolean updateUserService(User user, Long roleid) {
        user.setUpdateStamp(new Timestamp(System.currentTimeMillis()));
        user.setUpdatePeople(((User) SecurityUtils.getSubject().getPrincipal()).getId());
        if (!StringUtils.isEmpty(user.getPassword())) user.setPassword(StringUtil.getData(user.getPassword()));
        if (userDao.updateUserDao(user)) {
            UserRole ur = new UserRole();
            ur.setUserId(user.getId());
            ur.setRoleId(roleid);
            userRoleRepository.saveAndFlush(ur);
            return true;
        }
        return false;
    }

    /**
     * 根据用户编号删除用户信息
     *
     * @param userid
     */
    @CacheEvict(value = "user", allEntries = true)
    @Transactional
    public void deleteUser(Long userid) {
        userDao.delUserRole(userid);
        userRepository.deleteById(userid);
    }

}
