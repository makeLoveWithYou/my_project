package com.example.demo.form;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试信息表单
 * @date 2018/6/6 17:01
 */
public class InterviewForm implements Serializable {

    @ApiModelProperty(value = "编号")
    private Long id;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "0:今天 1:明天 2:后台 7:本周")
    private Integer timeInt;

    @ApiModelProperty(value = "性别：0：女，1：男")
    private Boolean sex;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "联系电话")
    private String phone;

    @ApiModelProperty(value = "职位")
    private Integer position;

    @ApiModelProperty(value = "面试公司")
    private String corporation;

    @ApiModelProperty(value = "简历渠道")
    private String channel;

    @ApiModelProperty(value = "0：未预约  1：已预约 2：拒绝 3：失约  4：等通知  5：已结束")
    private Integer status;

    @ApiModelProperty(value = "0：电话面试  1：面试")
    private Integer interview;

    @ApiModelProperty(value = "面试阶段")
    private Integer stage;

    @ApiModelProperty(value = "面试时间")
    private Timestamp interviewTime;

    @ApiModelProperty(value = "面试结果")
    private Integer interviewResults;

    @ApiModelProperty(value = "面试反馈")
    private String feedback;

    @ApiModelProperty(value = "评价")
    private String evaluate;

    @ApiModelProperty(value = "答题开始时间")
    private Timestamp startTime;

    @ApiModelProperty(value = "答题结束时间")
    private Timestamp endTime;


    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }

    public Integer getTimeInt() {
        return timeInt;
    }

    public void setTimeInt(Integer timeInt) {
        this.timeInt = timeInt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getCorporation() {
        return corporation;
    }

    public void setCorporation(String corporation) {
        this.corporation = corporation;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getInterview() {
        return interview;
    }

    public void setInterview(Integer interview) {
        this.interview = interview;
    }

    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }

    public Timestamp getInterviewTime() {
        return interviewTime;
    }

    public void setInterviewTime(Timestamp interviewTime) {
        this.interviewTime = interviewTime;
    }

    public Integer getInterviewResults() {
        return interviewResults;
    }

    public void setInterviewResults(Integer interviewResults) {
        this.interviewResults = interviewResults;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    @Override
    public String toString() {
        return "InterviewForm{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", timeInt=" + timeInt +
                ", sex=" + sex +
                ", age=" + age +
                ", phone='" + phone + '\'' +
                ", position='" + position + '\'' +
                ", corporation='" + corporation + '\'' +
                ", channel='" + channel + '\'' +
                ", status=" + status +
                ", interview=" + interview +
                ", stage=" + stage +
                ", interviewTime=" + interviewTime +
                ", interviewResults=" + interviewResults +
                ", feedback='" + feedback + '\'' +
                ", evaluate='" + evaluate + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
