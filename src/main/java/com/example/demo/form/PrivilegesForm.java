package com.example.demo.form;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 权限请求数据
 * @date 2018/5/25 14:05
 */
public class PrivilegesForm implements Serializable {

    @ApiModelProperty(value = "权限编号")
    private Long id;

    @ApiModelProperty(value = "权限访问地址")
    private String url;

    @ApiModelProperty(value = "权限名称")
    private String name;

    @ApiModelProperty(value = "权限类型")
    private String type;

    @ApiModelProperty(value = "权限描述")
    private String desc;

    @ApiModelProperty(value = "父权限编号（0为父权下，其他数字则是子权限）")
    private Long parentId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
