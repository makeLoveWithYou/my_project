package com.example.demo.form;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 角色请求数据
 * @date 2018/5/25 14:05
 */
public class RoleForm implements Serializable {

    @ApiModelProperty(value = "角色编号")
    private Long id;

    @ApiModelProperty(value = "角色code")
    private String code;

    @ApiModelProperty(value = "角色名称")
    private String name;

    @ApiModelProperty(value = "描述")
    private String desc;

    @ApiModelProperty(value = "创建人")
    private Long createPeople;

    @ApiModelProperty(value = "修改人")
    private Long updatePeople;

    @ApiModelProperty(value = "权限资源id")
    private List<Long> ids;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Long createPeople) {
        this.createPeople = createPeople;
    }

    public Long getUpdatePeople() {
        return updatePeople;
    }

    public void setUpdatePeople(Long updatePeople) {
        this.updatePeople = updatePeople;
    }

    public List<Long> getIds() {
        return ids;
    }

    public void setIds(List<Long> ids) {
        this.ids = ids;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
