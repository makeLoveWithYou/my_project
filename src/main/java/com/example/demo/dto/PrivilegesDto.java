package com.example.demo.dto;


import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 响应数据
 * @date 2018/5/28 16:25
 */
public class PrivilegesDto implements Serializable {

    /**
     * 权限编号
     **/
    private Long id;

    /**
     * 访问地址
     */
    private String url;

    /**
     * 访问类型
     */
    private String type;

    /**
     * 父级权限
     **/
    private Long parentId;

    /**
     * 描述
     **/
    private String desc;

    /**
     * 权限编码
     **/
    private String privilegeCode;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp createStamp;

    /**
     * 创建人
     */
    private Long createPeople;

    /**
     * 名称
     */
    private String name;

    @Override
    public String toString() {
        return "PrivilegesDto{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", type='" + type + '\'' +
                ", parentId=" + parentId +
                ", desc='" + desc + '\'' +
                ", privilegeCode='" + privilegeCode + '\'' +
                ", createStamp=" + createStamp +
                ", createPeople=" + createPeople +
                ", name=" + name +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode;
    }

    public Timestamp getCreateStamp() {
        return createStamp;
    }

    public void setCreateStamp(Timestamp createStamp) {
        this.createStamp = createStamp;
    }

    public Long getCreatePeople() {
        return createPeople;
    }

    public void setCreatePeople(Long createPeople) {
        this.createPeople = createPeople;
    }
}