package com.example.demo.dto;

import com.example.demo.common.CodeInfo;
import com.example.demo.common.MessageInfo;

import java.io.Serializable;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 响应对象
 * @date 2018/5/28 16:37
 */
public class ResultDto implements Serializable {

    private Integer total;

    private List<?> rows;

    private Object data;

    private String error = "";

    private Integer status = CodeInfo.CODE_200;

    private Integer page;

    private Integer pageSize;//数据总页码

    public ResultDto(Integer status, String error) {
        this.error = error;
        this.status = status;
    }

    public ResultDto() {

    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<?> getRows() {
        return rows;
    }

    public void setRows(List<?> rows) {
        this.rows = rows;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 设置错误状态
     */
    public void errorStatus() {
        this.status = CodeInfo.CODE_500;
        this.error = MessageInfo.error_500;
    }

    /**
     * 设置成功状态
     */
    public void successStatus() {
        this.status = CodeInfo.CODE_200;
        this.error = null;
    }

}
