package com.example.demo.dto;

import java.io.Serializable;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试题响应类
 * @date 2018/6/19 16:52
 */
public class TopicDto implements Serializable {

    private Integer score;

    private Integer okCount;

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getOkCount() {
        return okCount;
    }

    public void setOkCount(Integer okCount) {
        this.okCount = okCount;
    }
}
