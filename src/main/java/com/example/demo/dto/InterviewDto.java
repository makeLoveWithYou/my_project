package com.example.demo.dto;

/**
 * @Description: 面试信息dto
 * @author ShengGuang.Ye
 * @date 2018/6/27 12:37
 * @version V1.0
 */

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Timestamp;
import java.util.Date;

public class InterviewDto {
    /**
     * 编号
     **/
    private Long id;
    /**
     * 姓名
     **/
    private String name;

    /**
     * 性别：0：女，1：男
     **/
    private Boolean sex;
    /**
     * 年龄
     **/
    private Integer age;
    /**
     * 联系电话
     **/
    private String phone;
    /**
     * 职位
     **/
    private Integer position;
    /**
     * 面试公司
     **/
    private String corporation;

    /**
     * 简历渠道
     **/
    private String channel;
    /**
     * 0：未预约  1：已预约 2：拒绝 3：失约  4：等通知  5：已结束
     **/
    private Integer status;
    /**
     * 0：电话面试  1：面试
     **/
    private Integer interview;

    /**
     * 面试阶段
     */
    private Integer stage;

    /**
     * 面试时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp interviewTime;

    /**
     * 面试结果
     */
    private Integer interviewResults;

    /**
     * 面试反馈
     */
    private String evaluate;

    /**
     * 面试反馈
     */
    private String fileName;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Timestamp updateStamp;

    /**
     * 面试开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    /**
     * 面试结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    /**
     * 剩余时间
     */
    private String surplusTime;

    /***
     * 正在做的页数
     */
    private Integer page;

    /**
     * 正确数量
     */
    private String zq;

    /**
     * 题目数量
     */
    private String total;
    /**
     * 题目类型
     */
    private String topicType;

    public InterviewDto(Long id, String surplusTime, Integer page) {
        this.id = id;
        this.surplusTime = surplusTime;
        this.page = page;
    }

    public InterviewDto() {
    }

    public String getZq() {
        return zq;
    }

    public void setZq(String zq) {
        this.zq = zq;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTopicType() {
        return topicType;
    }

    public void setTopicType(String topicType) {
        this.topicType = topicType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSex() {
        return sex;
    }

    public void setSex(Boolean sex) {
        this.sex = sex;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getCorporation() {
        return corporation;
    }

    public void setCorporation(String corporation) {
        this.corporation = corporation;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getInterview() {
        return interview;
    }

    public void setInterview(Integer interview) {
        this.interview = interview;
    }

    public Integer getStage() {
        return stage;
    }

    public void setStage(Integer stage) {
        this.stage = stage;
    }

    public Timestamp getInterviewTime() {
        return interviewTime;
    }

    public void setInterviewTime(Timestamp interviewTime) {
        this.interviewTime = interviewTime;
    }

    public Integer getInterviewResults() {
        return interviewResults;
    }

    public void setInterviewResults(Integer interviewResults) {
        this.interviewResults = interviewResults;
    }

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Timestamp getUpdateStamp() {
        return updateStamp;
    }

    public void setUpdateStamp(Timestamp updateStamp) {
        this.updateStamp = updateStamp;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getSurplusTime() {
        return surplusTime;
    }

    public void setSurplusTime(String surplusTime) {
        this.surplusTime = surplusTime;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }


}
