package com.example.demo.dao;

import com.example.demo.dto.InterviewDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.form.InterviewForm;
import com.example.demo.pojo.Interview;

import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试功能接口定义
 * @date 2018/6/6 17:09
 */

public interface InterviewDao {

    /**
     * 面试人员信息查询
     *
     * @param interview
     * @param sort
     * @param page
     * @param size
     * @return
     */
    ResultDto findInterview(InterviewForm interview, String sort, Integer page, Integer size);

    /**
     * 获取面试人员详细信息
     *
     * @param id
     * @return
     */
    InterviewDto getInterviewDao(Long id);
    /**
     * 根据手机号码获取面试人员详细信息
     *
     * @param phone
     * @return
     */
    InterviewDto getInterviewDao(String phone);

    /**
     * 修改面试人员信息
     *
     * @param interview
     * @return
     */
    boolean updateInterview(InterviewDto interview);


    List<Interview> list(Interview interview);



}
