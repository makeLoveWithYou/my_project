package com.example.demo.dao;

import com.example.demo.pojo.Template;
import com.example.demo.pojo.Topic;

import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试功能接口定义
 * @date 2018/6/6 17:09
 */

public interface TopicDao {

    /**
     * 查询面试题
     */
    List<Topic> findTopic(List<Template> list);

    boolean update(Topic topic);



}
