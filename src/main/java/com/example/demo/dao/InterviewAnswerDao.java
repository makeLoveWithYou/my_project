package com.example.demo.dao;

import com.example.demo.dto.ResultDto;
import com.example.demo.dto.TopicAnswerDto;
import com.example.demo.pojo.InterviewAnswer;

import java.util.List;

public interface InterviewAnswerDao {

    boolean update(InterviewAnswer interviewAnswer);

    ResultDto find(Long interviewId, Long topicId, Integer page, Integer size, boolean bol);

    TopicAnswerDto statistics(Long interviewId);
}
