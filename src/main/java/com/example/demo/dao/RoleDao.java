package com.example.demo.dao;

import com.example.demo.dto.ResultDto;
import com.example.demo.dto.RoleDto;
import com.example.demo.pojo.Role;

import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 角色后端操作
 * @date 2018/6/1 17:24
 */
public interface RoleDao {

    /**
     * @Description: 根据用户编号查询角色
     * @author ShengGuang.Ye
     * @date 2018/6/1 17:07
     * @version V1.0
     */
    List<Role> findByUserId(Long userid);

    /**
     * @Description: 自定义SQL查询用户象关信息
     * @author ShengGuang.Ye
     * @date 2018/5/24 21:30
     * @version V1.0
     */
    ResultDto findRoleDao(Role role, Integer page, Integer size, String orderBy,Long userid);

    /**
     * 修改角色信息的方法
     *
     * @return
     */
    boolean updateRoleDao(Role role);

    /**
     * 查询角色是否被用户绑定
     *
     * @param roleid
     * @return
     */
    boolean findRoleUser(Long roleid);

    /**
     * 根据id查询角色信息
     *
     * @param roleid
     * @return
     */
    RoleDto getRoleDao(Long roleid);

    /**
     * 删除角色权限关联表信息
     *
     * @param roleid
     * @return
     */
    boolean delRolePrivilege(Long roleid);

    /**
     * 查询角色最大code
     *
     * @return
     */
    Integer findMaxCode();


}
