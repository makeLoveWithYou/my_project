package com.example.demo.dao.impl;

import com.example.demo.dao.RoleDao;
import com.example.demo.dto.ResultDto;
import com.example.demo.dto.RoleDto;
import com.example.demo.pojo.Role;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 角色后端操作
 * @date 2018/6/1 17:24
 */
@Repository
public class RoleDaoImpl implements RoleDao {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Role> findByUserId(Long userid) {
        Query query = entityManager.createNativeQuery("select t.id,t.code,t.desc,t.name from sys_role t LEFT JOIN sys_user_role ur ON t.id = ur.role_id WHERE ur.user_id = ?");
        query.setParameter(1, userid);
        List<Role> roles = new ArrayList<>();
        query.getResultList().forEach(item -> roles.add(getPrivileges((Object[]) item)));
        return roles;
    }

    /**
     * @Description: 自定义SQL查询用户象关信息
     * @author ShengGuang.Ye
     * @date 2018/5/24 21:30
     * @version V1.0
     */
    public ResultDto findRoleDao(Role role, Integer page, Integer size, String orderBy, Long userid) {
        //实例化集合存储用户集、分页相关信息
        ResultDto resultDto = new ResultDto();
        // 初始化、拼接sql语句
        StringBuffer sql = new StringBuffer("select t.id,t.name,t.code,t.desc,t.create_stamp,t.update_stamp,u.`name` as createName,uu.`name` as updateName from sys_role t LEFT JOIN sys_user u ON t.create_people = u.id LEFT JOIN sys_user uu ON t.update_people = uu.id");
        if (userid != 1) sql.append(" where t.id!=1");
        else sql.append(" where 1=1");
        setSqlOrQuery(sql, role, true, null);
        if (!StringUtils.isEmpty(orderBy)) sql.append(" order by " + orderBy);
        // 设置SQL参数、执行SQL获得结果,并将返回数据保存到map
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        setSqlOrQuery(sql, role, false, query);
        List<RoleDto> roles = new ArrayList();
        query.getResultList().forEach(item -> roles.add(getRole((Object[]) item)));
        resultDto.setRows(roles);
        // 获得总数据量保存到map返回
        sql = new StringBuffer("select count(1) from sys_role t where 1 = 1");
        setSqlOrQuery(sql, role, true, null);
        query = entityManager.createNativeQuery(sql.toString());
        setSqlOrQuery(sql, role, false, query);
        resultDto.setTotal(StringUtil.nullToInteger(query.getResultList().get(0)));
        return resultDto;
    }

    /**
     * 解析完整user字段
     *
     * @param array
     * @return
     */
    private RoleDto getRole(Object[] array) {
        if (array == null) return null;
        RoleDto dto = new RoleDto();
        dto.setId(StringUtil.nullToLong(array[0]));
        dto.setName(StringUtil.nullToString(array[1]));
        dto.setCode(StringUtil.nullToString(array[2]));
        dto.setDesc(StringUtil.nullToString(array[3]));
        if (!StringUtils.isEmpty(array[4])) dto.setCreateStamp(Timestamp.valueOf(array[4].toString()));
        if (!StringUtils.isEmpty(array[5])) dto.setUpdateStamp(Timestamp.valueOf(array[5].toString()));
        dto.setCreatePeople(StringUtil.nullToString(array[6]));
        dto.setUpdatePeople(StringUtil.nullToString(array[7]));
        return dto;
    }

    /**
     * 设置sql或参数
     *
     * @param sql
     * @param role
     * @param bol  true：设置sql，false：设置参数
     */
    private void setSqlOrQuery(StringBuffer sql, Role role, boolean bol, Query query) {
        if (!StringUtils.isEmpty(role.getCreatePeople())) {
            if (bol) {
                sql.append(" and t.create_people=:create_people");
            } else {
                query.setParameter("create_people", role.getCreatePeople());
            }
        }
        if (!StringUtils.isEmpty(role.getName())) {
            if (bol) {
                sql.append(" and t.name=:name");
            } else {
                query.setParameter("name", role.getName());
            }
        }
        if (!StringUtils.isEmpty(role.getUpdatePeople())) {
            if (bol) {
                sql.append(" and t.update_people=:update_people");
            } else {
                query.setParameter("update_people", role.getUpdatePeople());
            }
        }
    }

    /**
     * 查询角色是否被用户绑定
     *
     * @param roleid
     * @return
     */
    public boolean findRoleUser(Long roleid) {
        Query query = entityManager.createNativeQuery("SELECT count(u.id) FROM sys_role t LEFT JOIN sys_user_role ur ON t.id = ur.role_id LEFT JOIN sys_user u ON ur.user_id = u.id WHERE t.id = ?");
        query.setParameter(1, roleid);
        return Integer.valueOf(query.getResultList().get(0).toString()) > 0 ? false : true;
    }

    /**
     * 根据id查询角色信息
     *
     * @param roleid
     * @return
     */
    public RoleDto getRoleDao(Long roleid) {
        Query query = entityManager.createNativeQuery("select t.id,t.name,t.code,t.desc,t.create_stamp,t.update_stamp,u.`name` as createName,uu.`name` as updateName from sys_role t LEFT JOIN sys_user u ON t.create_people = u.id LEFT JOIN sys_user uu ON t.update_people = uu.id where t.id!=1 and t.id = ?");
        query.setParameter(1, roleid);
        return getRole((Object[]) query.getResultList().get(0));
    }

    /**
     * 删除角色权限关联表信息
     *
     * @param roleid
     * @return
     */
    public boolean delRolePrivilege(Long roleid) {
        Query query = entityManager.createNativeQuery("DELETE FROM sys_role_privileges WHERE role_id = ?");
        query.setParameter(1, roleid);
        return query.executeUpdate() < 1 ? false : true;
    }

    /**
     * 查询角色最大code
     *
     * @return
     */
    public Integer findMaxCode() {
        Query query = entityManager.createNativeQuery("select max(`code`) from sys_role");
        return Integer.parseInt(query.getResultList().get(0).toString());
    }

    /**
     * 修改角色信息的方法
     *
     * @return
     */
    @Override
    public boolean updateRoleDao(Role role) {
        StringBuffer sql = new StringBuffer("update sys_role t set");
        StringBuffer set = new StringBuffer();
        if (!StringUtils.isEmpty(role.getDesc())) set.append(",t.desc = :desc");
        if (!StringUtils.isEmpty(role.getUpdateStamp())) set.append(",t.update_stamp = :update_stamp");
        if (!StringUtils.isEmpty(role.getUpdatePeople())) set.append(",t.update_people = :update_people");
        sql.append(" " + set.substring(1, set.length()));
        sql.append(" where t.id = :id");
        //设置sql参数并执行
        Query query = entityManager.createNativeQuery(sql.toString());
        if (!StringUtils.isEmpty(role.getDesc())) query.setParameter("desc", role.getDesc());
        if (!StringUtils.isEmpty(role.getId())) query.setParameter("id", role.getId());
        if (!StringUtils.isEmpty(role.getUpdateStamp())) query.setParameter("update_stamp", role.getUpdateStamp());
        if (!StringUtils.isEmpty(role.getUpdatePeople())) query.setParameter("update_people", role.getUpdatePeople());
        return query.executeUpdate() > 0 ? true : false;
    }

    /**
     * 解析
     *
     * @param array
     * @return
     */
    private Role getPrivileges(Object[] array) {
        if (array == null) return null;
        Role role = new Role();
        role.setId(StringUtil.nullToLong(array[0]));
        role.setCode(StringUtil.nullToString(array[1]));
        role.setDesc(StringUtil.nullToString(array[2]));
        role.setName(StringUtil.nullToString(array[3]));
        return role;
    }
}
