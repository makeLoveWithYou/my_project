package com.example.demo.dao.impl;

import com.example.demo.dao.UserDao;
import com.example.demo.dto.ResultDto;
import com.example.demo.dto.UserDto;
import com.example.demo.pojo.User;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 自定义SQL方法
 * @date 2018/5/24 18:23
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * @Description: 自定义SQL查询用户象关信息
     * @author ShengGuang.Ye
     * @date 2018/5/24 21:30
     * @version V1.0
     */
    public ResultDto findUserDao(User user, Integer page, Integer size, String orderBy) {
        //实例化集合存储用户集、分页相关信息
        ResultDto resultDto = new ResultDto();
        // 初始化、拼接sql语句
        StringBuffer sql = new StringBuffer("select t.id,t.username,t.`password`,t.email,t.age,t.sex,t.`status`,GROUP_CONCAT(r.name),t.phone,t.name,t.create_stamp,t.update_stamp from " +
                "sys_user t left join sys_user_role ur ON t.id=ur.user_id LEFT JOIN sys_role r ON ur.role_id=r.id where 1 = 1");
        if (!StringUtils.isEmpty(user.getUsername())) user.setUsername('%' + user.getUsername() + '%');
        setSql(sql, user);
        sql.append(" group by t.id");
        if (!StringUtils.isEmpty(orderBy)) sql.append(" order by " + orderBy);
        // 设置SQL参数、执行SQL获得结果,并将返回数据保存到map
        //TODO createQuery执行的不是sql语法   createNativeQuery执行的是sql语法
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        setQuery(query, user);
        if (user.getId() != null && user.getId() != 1) query.setParameter("id", user.getId());
        List<UserDto> users = new ArrayList();
        query.getResultList().forEach(item -> users.add(getUser((Object[]) item)));
        resultDto.setRows(users);
        // 获得总数据量保存到map返回
        sql = new StringBuffer("select count(1) from sys_user t where 1 = 1");
        setSql(sql, user);
        query = entityManager.createNativeQuery(sql.toString());
        setQuery(query, user);
        if (user.getId() != null && user.getId() != 1) query.setParameter("id", user.getId());
        List countLi = query.getResultList();
        resultDto.setTotal((countLi != null && countLi.size() > 0) ? StringUtil.nullToInteger(countLi.get(0)) : 0);
        return resultDto;
    }

    /**
     * 修改用户信息的方法
     *
     * @param user
     * @return
     */
    @Override
    public boolean updateUserDao(User user) {
        StringBuffer sql = new StringBuffer("update sys_user t set");
        StringBuffer set = new StringBuffer();
        if (!StringUtils.isEmpty(user.getUsername())) set.append(",t.username = :username");
        if (!StringUtils.isEmpty(user.getPassword())) set.append(",t.password = :password");
        if (!StringUtils.isEmpty(user.getEmail())) set.append(",t.email = :email");
        if (!StringUtils.isEmpty(user.getStatus())) set.append(",t.status = :status");
        if (!StringUtils.isEmpty(user.getSex())) set.append(",t.sex = :sex");
        if (!StringUtils.isEmpty(user.getAge())) set.append(",t.age = :age");
        if (!StringUtils.isEmpty(user.getPhone())) set.append(",t.phone = :phone");
        if (!StringUtils.isEmpty(user.getName())) set.append(",t.name = :name");
        if (!StringUtils.isEmpty(user.getUpdateStamp())) set.append(",t.update_stamp = :update_stamp");
        sql.append(" " + set.substring(1, set.length()));
        sql.append(" where t.id = :id");
        //设置sql参数并执行
        Query query = entityManager.createNativeQuery(sql.toString());
        setQuery(query, user);
        query.setParameter("id", user.getId());
        if (query.executeUpdate() > 0 ? true : false) return delUserRole(user.getId());
        return false;
    }

    /**
     * 删除用户关联的角色信息
     *
     * @param userid
     * @return
     */
    public boolean delUserRole(Long userid) {
        Query query = entityManager.createNativeQuery("delete from sys_user_role where user_id=?");
        query.setParameter(1, userid);
        return query.executeUpdate() > 0 ? true : false;
    }

    /**
     * 根据用户编号获得用户信息
     *
     * @param userid
     * @return
     */
    @Override
    public UserDto getUserDao(Long userid) {
        Query query = entityManager.createNativeQuery("select t.id,t.username,t.`password`,t.email,t.age,t.sex,t.`status`,GROUP_CONCAT(r.name),t.phone,t.name,t.create_stamp,t.update_stamp from sys_user t left join sys_user_role ur ON t.id=ur.user_id LEFT JOIN sys_role r ON ur.role_id=r.id where t.id = ? group by t.id");
        query.setParameter(1, userid);
        List list = query.getResultList();
        return getUser((list == null || list.size() < 1) ? null : (Object[]) list.get(0));
    }

    /**
     * 设置sql条件
     *
     * @param sql
     */
    private void setSql(StringBuffer sql, User user) {
        if (user.getId() != null && user.getId() != 1) {
            sql.append(" and t.id != 1");
            sql.append(" and t.create_people = :id");
        }
        if (!StringUtils.isEmpty(user.getUsername())) sql.append(" and t.username like :username");
        if (!StringUtils.isEmpty(user.getEmail())) sql.append(" and t.email = :email");
        if (!StringUtils.isEmpty(user.getStatus())) sql.append(" and t.status = :status");
    }

    /**
     * 设置sql参数
     *
     * @param query
     */
    private void setQuery(Query query, User user) {
        if (!StringUtils.isEmpty(user.getUsername())) query.setParameter("username", user.getUsername());
        if (!StringUtils.isEmpty(user.getPassword())) query.setParameter("password", user.getPassword());
        if (!StringUtils.isEmpty(user.getEmail())) query.setParameter("email", user.getEmail());
        if (!StringUtils.isEmpty(user.getStatus())) query.setParameter("status", user.getStatus());
        if (!StringUtils.isEmpty(user.getSex())) query.setParameter("sex", user.getSex());
        if (!StringUtils.isEmpty(user.getAge())) query.setParameter("age", user.getAge());
        if (!StringUtils.isEmpty(user.getPhone())) query.setParameter("phone", user.getPhone());
        if (!StringUtils.isEmpty(user.getName())) query.setParameter("name", user.getName());
        if (!StringUtils.isEmpty(user.getUpdateStamp())) query.setParameter("update_stamp", user.getUpdateStamp());
    }

    /**
     * 解析完整user字段
     *
     * @param array
     * @return
     */
    private UserDto getUser(Object[] array) {
        if (array == null) return null;
        UserDto UserDto = new UserDto();
        UserDto.setId(StringUtil.nullToLong(array[0]));
        UserDto.setUsername(StringUtil.nullToString(array[1]));
        UserDto.setPassword(StringUtil.nullToString(array[2]));
        UserDto.setEmail(StringUtil.nullToString(array[3]));
        UserDto.setAge(StringUtil.nullToInteger(array[4]));
        UserDto.setSex(StringUtil.nullToBoolean(array[5]));
        UserDto.setStatus(StringUtil.nullToInteger(array[6]));
        UserDto.setDesc(StringUtil.nullToString(array[7]));
        UserDto.setPhone(StringUtil.nullToString(array[8]));
        UserDto.setName(StringUtil.nullToString(array[9]));
        if (!StringUtils.isEmpty(array[10])) UserDto.setCreateStamp(Timestamp.valueOf(array[10].toString()));
        if (!StringUtils.isEmpty(array[11])) UserDto.setUpdateStamp(Timestamp.valueOf(array[11].toString()));
        return UserDto;
    }
}
