package com.example.demo.dao.impl;

import com.example.demo.dao.PrivilegesDao;
import com.example.demo.dto.PrivilegesDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.pojo.Privileges;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 权限后端接口实现
 * @date 2018/6/1 17:29
 */
@Repository
public class PrivilegesDaoImpl implements PrivilegesDao {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * 权限资源查询
     *
     * @param privileges
     * @param orderBy
     * @param page
     * @param size
     * @return
     */
    public ResultDto findPrivileges(Privileges privileges, String orderBy, Integer page, Integer size) {
        //实例化集合存储用户集、分页相关信息
        ResultDto resultDto = new ResultDto();
        // 初始化、拼接sql语句
        StringBuffer sql = new StringBuffer("SELECT t.id,t.url,t.type,t.`desc`,t.parent_id,t.create_stamp,t.create_people,t.`name` FROM sys_privileges t where 1=1");
        setSqlOrQuery(sql, privileges, true, null);
        if (!StringUtils.isEmpty(orderBy)) sql.append(" order by " + orderBy);
        // 设置SQL参数、执行SQL获得结果,并将返回数据保存到map
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setFirstResult((page - 1) * size);
        query.setMaxResults(size);
        setSqlOrQuery(sql, privileges, false, query);
        List<PrivilegesDto> roles = new ArrayList();
        query.getResultList().forEach(item -> roles.add(getPrivileges((Object[]) item)));
        resultDto.setRows(roles);
        // 获得总数据量保存到map返回
        sql = new StringBuffer("select count(1) from sys_privileges t where 1 = 1");
        setSqlOrQuery(sql, privileges, true, null);
        query = entityManager.createNativeQuery(sql.toString());
        setSqlOrQuery(sql, privileges, false, query);
        resultDto.setTotal(StringUtil.nullToInteger(query.getResultList().get(0)));
        return resultDto;
    }

    /**
     * 修改权限资源
     *
     * @param privileges
     * @return
     */
    public boolean updatePrivileges(Privileges privileges) {
        StringBuffer sql = new StringBuffer("update sys_privileges t set");
        StringBuffer set = new StringBuffer();
        if (!StringUtils.isEmpty(privileges.getDesc())) set.append(",t.desc = :desc");
        if (!StringUtils.isEmpty(privileges.getUrl())) set.append(",t.url = :url");
        if (!StringUtils.isEmpty(privileges.getType())) set.append(",t.type = :type");
        if (!StringUtils.isEmpty(privileges.getParentId())) set.append(",t.parent_id = :parent_id");
        if (!StringUtils.isEmpty(privileges.getName())) set.append(",t.name = :name");
        sql.append(" " + set.substring(1, set.length()));
        sql.append(" where t.id = :id");
        //设置sql参数并执行
        Query query = entityManager.createNativeQuery(sql.toString());
        if (!StringUtils.isEmpty(privileges.getDesc())) query.setParameter("desc", privileges.getDesc());
        if (!StringUtils.isEmpty(privileges.getId())) query.setParameter("id", privileges.getId());
        if (!StringUtils.isEmpty(privileges.getUrl())) query.setParameter("url", privileges.getUrl());
        if (!StringUtils.isEmpty(privileges.getType())) query.setParameter("type", privileges.getType());
        if (!StringUtils.isEmpty(privileges.getParentId())) query.setParameter("parent_id", privileges.getParentId());
        if (!StringUtils.isEmpty(privileges.getName())) query.setParameter("name", privileges.getName());
        return query.executeUpdate() > 0 ? true : false;
    }

    /**
     * 设置sql或参数
     *
     * @param sql
     * @param privileges
     * @param bol        true：设置sql，false：设置参数
     */
    private void setSqlOrQuery(StringBuffer sql, Privileges privileges, boolean bol, Query query) {
        if (!StringUtils.isEmpty(privileges.getCreatePeople())) {
            if (bol) {
                sql.append(" and t.create_people=:create_people");
            } else {
                query.setParameter("create_people", privileges.getCreatePeople());
            }
        }
        if (!StringUtils.isEmpty(privileges.getDesc())) {
            if (bol) {
                sql.append(" and t.privilege_code=:privilege_code");
            } else {
                query.setParameter("privilege_code", privileges.getDesc());
            }
        }
        if (!StringUtils.isEmpty(privileges.getParentId())) {
            if (bol) {
                sql.append(" and t.parent_id=:parent_id");
            } else {
                query.setParameter("parent_id", privileges.getParentId());
            }
        }
        if (!StringUtils.isEmpty(privileges.getName())) {
            if (bol) {
                sql.append(" and t.name like :name");
            } else {
                query.setParameter("name", '%' + privileges.getName() + '%');
            }
        }
    }

    /**
     * 根据权限编号查询是否有角色绑定
     *
     * @param privilegesid
     * @return
     */
    public boolean findPrivilegesRole(Long privilegesid) {
        Query query = entityManager.createNativeQuery("SELECT COUNT(r.id) FROM sys_privileges t LEFT JOIN sys_role_privileges rp ON t.id = rp.privileges_id LEFT JOIN sys_role r ON rp.role_id = r.id WHERE t.id=?");
        query.setParameter(1, privilegesid);
        return Integer.valueOf(query.getResultList().get(0).toString()) > 0 ? false : true;
    }

    /**
     * 根据权限编号查询是否有子权限绑定
     *
     * @param privilegesid
     * @return
     */
    public boolean findPrivilegesSonPrivilieges(Long privilegesid) {
        Query query = entityManager.createNativeQuery("SELECT COUNT(t.id) FROM sys_privileges t WHERE t.parent_id = ?");
        query.setParameter(1, privilegesid);
        return Integer.valueOf(query.getResultList().get(0).toString()) > 0 ? false : true;
    }

    /**
     * 查询权限最大code
     *
     * @return
     */
    public String findMaxCode(Long parentid) {
        StringBuffer sql = new StringBuffer("select max(privilege_code) from sys_privileges");
        Query query = entityManager.createNativeQuery(sql.toString());
        if (parentid == 0) sql.append(" WHERE parent_id = ?");
        else sql.append(" WHERE id = ?");
        query.setParameter(1, parentid);
        String codeStr = query.getResultList().get(0).toString();
        if (parentid == 0) {//父权限
            codeStr = codeStr.substring(0, 1);
            codeStr = (Integer.valueOf(codeStr) + 1) + "0001";
        } else {
            codeStr = (Integer.valueOf(codeStr) + 1) + "";
        }
        return codeStr;
    }

    /**
     * @Description: 根据角色编号查询权限
     * @author ShengGuang.Ye
     * @date 2018/6/1 17:07
     * @version V1.0
     */
    public List<Privileges> findByRoleId(Long roleid) {
        Query query = entityManager.createNativeQuery("select t.privilege_code from sys_privileges t LEFT JOIN sys_role_privileges rp ON t.id = rp.privileges_id WHERE rp.role_id = ?");
        query.setParameter(1, roleid);
        List<Privileges> privileges = new ArrayList<>();
        query.getResultList().forEach(item -> {
            Privileges p = new Privileges();
            p.setPrivilegeCode(StringUtil.nullToString(item));
            privileges.add(p);
        });
        return privileges;
    }

    /**
     * 权限详情
     */
    public PrivilegesDto getPrivilegesDao(Long id) {
        Query query = entityManager.createNativeQuery("select t.id,t.url,t.type,t.`desc`,t.parent_id,t.create_stamp,t.create_people,t.`name` from sys_privileges t where t.id = ?");
        query.setParameter(1, id);
        List list = query.getResultList();
        if (list != null && list.size() > 0) {
            return getPrivileges((Object[]) list.get(0));
        }
        return null;
    }

    /**
     * 解析
     *
     * @param array
     * @return
     */
    private PrivilegesDto getPrivileges(Object[] array) {
        if (array == null) return null;
        PrivilegesDto privileges = new PrivilegesDto();
        privileges.setId(StringUtil.nullToLong(array[0]));
        privileges.setUrl(StringUtil.nullToString(array[1]));
        privileges.setType(StringUtil.nullToString(array[2]));
        privileges.setDesc(StringUtil.nullToString(array[3]));
        privileges.setParentId(StringUtil.nullToLong(array[4]));
        if (!StringUtils.isEmpty(array[5])) privileges.setCreateStamp(Timestamp.valueOf(array[5].toString()));
        privileges.setPrivilegeCode(StringUtil.nullToString(array[6]));
        privileges.setName(StringUtil.nullToString(array[7]));
        return privileges;
    }
}
