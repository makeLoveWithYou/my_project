package com.example.demo.dao.impl;

import com.example.demo.dao.InterviewDao;
import com.example.demo.dto.InterviewDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.form.InterviewForm;
import com.example.demo.pojo.Interview;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试后端接口实现
 * @date 2018/6/1 17:29
 */
@Repository
public class InterviewDaoImpl implements InterviewDao {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public ResultDto findInterview(InterviewForm interview, String sort, Integer page, Integer size) {
        ResultDto dto = new ResultDto();
        String column = "t1.*,t2.zq,t2.total,t2.topictype";
        // 查询数据
        StringBuffer sql = new StringBuffer("select ");
        sql.append(column);
        sql.append(" from sys_interview t1 left join ");
        sql.append("(select a.interviewId,group_concat(a.zq) zq,group_concat(a.total) total,group_concat(a.topictype) topictype from ");
        sql.append("(select t2.interviewId,t2.topictype,sum(case when t2.status=1 then 1 else 0 end) zq,count(1) total from sys_interview_answer t2 ");
        sql.append("group by t2.interviewId,t2.topictype) a GROUP BY a.interviewId)t2 ON t1.id = t2.interviewId where 1=1 ");
        tQuerySql(sql, interview, null, true);
        sql.append(" order by t1.update_stamp desc");
        sql.append(" limit ");
        page = (page > 0 ? (page - 1) : page) * size;
        sql.append(page);
        sql.append(",");
        sql.append(size);
        Query query = entityManager.createNativeQuery(sql.toString());
        tQuerySql(sql, interview, query, false);
        List list = query.getResultList();
        List<InterviewDto> interviewDtos = new ArrayList<>(list.size());
        if (list != null && list.size() > 0) {
            list.forEach(item -> interviewDtos.add(getInterview((Object[]) item)));
        }
        dto.setRows(interviewDtos);
        // 获得总数
        sql.replace(7, column.length() + 7, "count(1)");
        sql.replace(sql.lastIndexOf("order"), sql.length(), "");
        tQuerySql(sql, interview, null, true);
        query = entityManager.createNativeQuery(sql.toString());
        tQuerySql(sql, interview, query, false);
        dto.setTotal(StringUtil.nullToInteger(query.getResultList().get(0)));
        int pagesize = (int) Math.ceil((dto.getTotal() * 1.0) / size);
        dto.setPageSize(pagesize < 1 ? 1 : pagesize);
        return dto;
    }

    private void tQuerySql(StringBuffer sql, InterviewForm interviewForm, Query query, boolean bol) {
        if (!StringUtils.isEmpty(interviewForm.getName())) {
            if (bol) {
                sql.append(" AND t1.`name` like :name");
            } else {
                query.setParameter("name", interviewForm.getName());
            }
        }
        if (!StringUtils.isEmpty(interviewForm.getId())) {
            if (bol) {
                sql.append(" AND t1.`id` = :id");
            } else {
                query.setParameter("id", interviewForm.getId());
            }
        }
    }

    /**
     * 获取面试人员详细信息
     *
     * @param id
     * @return
     */
    @Override
    public InterviewDto getInterviewDao(Long id) {
        StringBuffer sql = new StringBuffer("select t.* from sys_interview t where t.id = ?");
        //设置sql参数并执行
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter(1, id);
        List list = query.getResultList();
        if (list != null && list.size() > 0) {
            return getInterview((Object[]) list.get(0));
        }
        return null;
    }

    /**
     * 根据手机号码获取面试人员详细信息
     *
     * @param phone
     * @return
     */
    @Override
    public InterviewDto getInterviewDao(String phone) {
        StringBuffer sql = new StringBuffer("select t.* from sys_interview t where t.phone = ?");
        //设置sql参数并执行
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter(1, phone);
        List list = query.getResultList();
        if (list != null && list.size() > 0) {
            return getInterview((Object[]) list.get(0));
        }
        return null;
    }

    /**
     * 解析
     *
     * @param array
     * @return
     */
    private InterviewDto getInterview(Object[] array) {
        if (array == null) return null;
        InterviewDto interview = new InterviewDto();
        interview.setId(StringUtil.nullToLong(array[0]));
        interview.setName(StringUtil.nullToString(array[1]));
        interview.setSex(StringUtil.nullToBoolean(array[2]));
        interview.setAge(StringUtil.nullToInteger(array[3]));
        interview.setPhone(StringUtil.nullToString(array[4]));
        interview.setPosition(StringUtil.nullToInteger(array[5]));
        interview.setCorporation(StringUtil.nullToString(array[6]));
        interview.setChannel(StringUtil.nullToString(array[7]));
        interview.setStatus(StringUtil.nullToInteger(array[8]));
        interview.setInterview(StringUtil.nullToInteger(array[9]));
        interview.setStage(StringUtil.nullToInteger(array[10]));
        if (!StringUtils.isEmpty(array[11])) interview.setInterviewTime(Timestamp.valueOf(array[11].toString()));
        interview.setInterviewResults(StringUtil.nullToInteger(array[12]));
        interview.setEvaluate(StringUtil.nullToString(array[13]));
        if (!StringUtils.isEmpty(array[14])) interview.setUpdateStamp(Timestamp.valueOf(array[14].toString()));
        interview.setFileName(StringUtil.nullToString(array[15]));
        interview.setStartTime(StringUtil.nullToDate(array[16], "yyyy-MM-dd HH:mm:ss"));
        interview.setEndTime(StringUtil.nullToDate(array[17], "yyyy-MM-dd HH:mm:ss"));
        interview.setSurplusTime(StringUtil.nullToString(array[18]));
        interview.setPage(StringUtil.nullToInteger(array[19]));
        if (array.length > 20) {
            interview.setZq(StringUtil.nullToString(array[20]));
            interview.setTotal(StringUtil.nullToString(array[21]));
            interview.setTopicType(StringUtil.nullToString(array[22]));
        }
        return interview;
    }

    /**
     * 修改面试人员信息
     *
     * @param interview
     * @return
     */
    public boolean updateInterview(InterviewDto interview) {
        StringBuffer sql = new StringBuffer("update sys_interview t set");
        StringBuffer set = new StringBuffer();
        setSqlOrQuery(set, interview, true, null);
        sql.append(" " + set.substring(1, set.length()));
        sql.append(" where t.id = :id");
        //设置sql参数并执行
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter("id", interview.getId());
        setSqlOrQuery(null, interview, false, query);
        return query.executeUpdate() > 0 ? true : false;
    }

    @Override
    public List<Interview> list(Interview interview) {
        return null;
    }

    /**
     * 设置sql或参数
     *
     * @param sql
     * @param interview
     * @param bol       true：设置sql，false：设置参数
     */
    private void setSqlOrQuery(StringBuffer sql, InterviewDto interview, boolean bol, Query query) {
        if (!StringUtils.isEmpty(interview.getName())) {
            if (bol) {
                sql.append(",t.`name`=:name");
            } else {
                query.setParameter("name", interview.getName());
            }
        }
        if (!StringUtils.isEmpty(interview.getCorporation())) {
            if (bol) {
                sql.append(",t.corporation=:corporation");
            } else {
                query.setParameter("corporation", interview.getCorporation());
            }
        }
        if (!StringUtils.isEmpty(interview.getChannel())) {
            if (bol) {
                sql.append(",t.channel=:channel");
            } else {
                query.setParameter("channel", interview.getChannel());
            }
        }
        if (!StringUtils.isEmpty(interview.getPhone())) {
            if (bol) {
                sql.append(",t.phone=:phone");
            } else {
                query.setParameter("phone", interview.getPhone());
            }
        }
        if (!StringUtils.isEmpty(interview.getSex())) {
            if (bol) {
                sql.append(",t.sex=:sex");
            } else {
                query.setParameter("sex", interview.getSex());
            }
        }
        if (!StringUtils.isEmpty(interview.getAge())) {
            if (bol) {
                sql.append(",t.age=:age");
            } else {
                query.setParameter("age", interview.getAge());
            }
        }
        if (!StringUtils.isEmpty(interview.getPosition())) {
            if (bol) {
                sql.append(",t.position=:position");
            } else {
                query.setParameter("position", interview.getPosition());
            }
        }
        if (!StringUtils.isEmpty(interview.getStatus())) {
            if (bol) {
                sql.append(",t.`status`=:status");
            } else {
                query.setParameter("status", interview.getStatus());
            }
        }
        if (!StringUtils.isEmpty(interview.getInterview())) {
            if (bol) {
                sql.append(",t.interview=:interview");
            } else {
                query.setParameter("interview", interview.getInterview());
            }
        }
        if (!StringUtils.isEmpty(interview.getStage())) {
            if (bol) {
                sql.append(",t.stage=:stage");
            } else {
                query.setParameter("stage", interview.getStage());
            }
        }
        if (!StringUtils.isEmpty(interview.getInterviewTime())) {
            if (bol) {
                sql.append(",t.interview_time=:interview_time");
            } else {
                query.setParameter("interview_time", interview.getInterviewTime());
            }
        }
        if (!StringUtils.isEmpty(interview.getInterviewResults())) {
            if (bol) {
                sql.append(",t.interview_results=:interview_results");
            } else {
                query.setParameter("interview_results", interview.getInterviewResults());
            }
        }
        if (!StringUtils.isEmpty(interview.getEvaluate())) {
            if (bol) {
                sql.append(",t.evaluate=:evaluate");
            } else {
                query.setParameter("evaluate", interview.getEvaluate());
            }
        }
        if (!StringUtils.isEmpty(interview.getFileName())) {
            if (bol) {
                sql.append(",t.file_name=:file_name");
            } else {
                query.setParameter("file_name", interview.getFileName());
            }
        }
        if (!StringUtils.isEmpty(interview.getUpdateStamp())) {
            if (bol) {
                sql.append(",t.update_stamp=:update_stamp");
            } else {
                query.setParameter("update_stamp", interview.getUpdateStamp());
            }
        }
        if (!StringUtils.isEmpty(interview.getSurplusTime())) {
            if (bol) {
                sql.append(",t.surplusTime=:surplusTime");
            } else {
                query.setParameter("surplusTime", interview.getSurplusTime());
            }
        }
        if (!StringUtils.isEmpty(interview.getPage())) {
            if (bol) {
                sql.append(",t.page=:page");
            } else {
                query.setParameter("page", interview.getPage());
            }
        }
        if (!StringUtils.isEmpty(interview.getStartTime())) {
            if (bol) {
                sql.append(",t.start_time=:start_time");
            } else {
                query.setParameter("start_time", interview.getStartTime());
            }
        }
        if (!StringUtils.isEmpty(interview.getEndTime())) {
            if (bol) {
                sql.append(",t.end_time=:end_time");
            } else {
                query.setParameter("end_time", interview.getEndTime());
            }
        }
    }
}
