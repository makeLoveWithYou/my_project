package com.example.demo.dao.impl;

import com.example.demo.dao.TopicDao;
import com.example.demo.pojo.Template;
import com.example.demo.pojo.Topic;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 权限后端接口实现
 * @date 2018/6/1 17:29
 */
@Repository
public class TopicDaoImpl implements TopicDao {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * 抽取面试题
     *
     * @return
     */
    public List<Topic> findTopic(List<Template> list) {
        //TODO 定义字段、SQL
        String[] columns = {"t.id", "t.name", "t.answer"};
        String[] orders = {"desc", "asc"};
        List<Topic> topics = new ArrayList<>();
        if (list != null && list.size() > 0) {
            for (Template temp : list) {
                StringBuffer sql = new StringBuffer("select t.* from sys_topic t where t.topicType = ? ");
                int size = temp.getTopicCount();
                //TODO 设置必选数据
                if (!StringUtils.isEmpty(temp.getMustTopic())) {
                    sql.append("AND t.id not in(");
                    sql.append(temp.getMustTopic());
                    sql.append(") ");
                    List<Topic> ls = filter(temp.getMustTopic(), temp.getTopicType());
                    ls.forEach(item -> topics.add(item));
                    size -= temp.getMustTopic().split(",").length;
                }
                if (size > 0) {
                    //TODO 定义随机排序SQL，保证每次抽的题目不一样
                    sql.append("ORDER BY ");
                    sql.append(columns[(int) (Math.random() * 3)]);
                    sql.append(" ");
                    sql.append(orders[(int) (Math.random() * 2)]);
                    sql.append(" limit ");
                    sql.append(size);
                    Query query = entityManager.createNativeQuery(sql.toString());
                    query.setParameter(1, temp.getTopicType());
                    List resultList = query.getResultList();
                    if (!StringUtils.isEmpty(resultList) && resultList.size() > 0) {
                        resultList.forEach(item -> topics.add(getTopic((Object[]) item)));
                    }
                }
            }
        }
        return topics;
    }

    /**
     * 查询必中的数据
     *
     * @param mustId
     * @return
     */
    private List<Topic> filter(String mustId, Integer topicType) {
        List<Topic> list = new ArrayList<>();
        StringBuffer sql = new StringBuffer("select t.* from sys_topic t where t.topicType = ?");
        sql.append(" AND t.id IN(");
        sql.append(mustId);
        sql.append(")");
        Query query = entityManager.createNativeQuery(sql.toString());
        query.setParameter(1, topicType);
        query.getResultList().forEach(item -> list.add(getTopic((Object[]) item)));
        return list;
    }

    /**
     * 解析
     *
     * @param array
     * @return
     */
    private Topic getTopic(Object[] array) {
        if (array == null) return null;
        Topic topic = new Topic();
        topic.setId(StringUtil.nullToLong(array[0]));
        topic.setName(StringUtil.nullToString(array[1]));
        topic.setSelectAnswer(StringUtil.nullToString(array[2]));
        topic.setAnswer(StringUtil.nullToString(array[3]));
        topic.setTopicType(StringUtil.nullToInteger(array[4]));
        return topic;
    }


    /**
     * 更新题库
     *
     * @param topic
     * @return
     */
    public boolean update(Topic topic) {
        StringBuffer sql = new StringBuffer("UPDATE sys_topic SET ");
        setSqlOrQuery(sql, topic, true, null);
        Query query = entityManager.createNativeQuery(sql.toString());
        setSqlOrQuery(sql, topic, false, query);
        return query.executeUpdate() > 0 ? true : false;
    }

    /**
     * 设置sql或参数
     *
     * @param sql
     * @param bol true：设置sql，false：设置参数
     */
    private void setSqlOrQuery(StringBuffer sql, Topic topic, boolean bol, Query query) {
        if (!StringUtils.isEmpty(topic.getName())) {
            if (bol) {
                sql.append(" name = :name,");
            } else {
                query.setParameter("name", topic.getName());
            }
        }
        if (!StringUtils.isEmpty(topic.getSelectAnswer())) {
            if (bol) {
                sql.append(" selectAnswer = :selectAnswer,");
            } else {
                query.setParameter("selectAnswer", topic.getSelectAnswer());
            }
        }
        if (!StringUtils.isEmpty(topic.getAnswer())) {
            if (bol) {
                sql.append(" answer = :answer,");
            } else {
                query.setParameter("answer", topic.getAnswer());
            }
        }
        if (!StringUtils.isEmpty(topic.getTopicType())) {
            if (bol) {
                sql.append(" topicType = :topicType,");
            } else {
                query.setParameter("topicType", topic.getTopicType());
            }
        }
        sql.replace(0, sql.length(), sql.substring(0, sql.length() - 1));
        if (!StringUtils.isEmpty(topic.getId())) {
            if (bol) {
                sql.append(" WHERE id = :id ");
            } else {
                query.setParameter("id", topic.getId());
            }
        }
    }
}
