package com.example.demo.dao.impl;

import com.example.demo.dao.InterviewAnswerDao;
import com.example.demo.dto.ResultDto;
import com.example.demo.dto.TopicAnswerDto;
import com.example.demo.pojo.InterviewAnswer;
import com.example.demo.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试后端接口实现
 * @date 2018/6/1 17:29
 */
@Repository
public class InterviewAnswerDaoImpl implements InterviewAnswerDao {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * @param interviewId
     * @param topicId
     * @param page
     * @param size
     * @param bol         true：则显示答案 false：则不获得数据库的答案
     * @return
     */
    public ResultDto find(Long interviewId, Long topicId, Integer page, Integer size, boolean bol) {
        ResultDto dto = new ResultDto();
        String column = "ti.id,ti.topicId,ti.`status`,ti.answer,ti.interviewId,ti.createStamp,ti.updateStamp,tt.`name`,tt.selectAnswer,tt.answer correctAnswer";
        StringBuffer sql = new StringBuffer("select ");
        sql.append(column);
        sql.append(" from sys_interview_answer ti INNER JOIN sys_topic tt ON ti.topicId = tt.id where 1=1 ");
        setSqlOrQuery(sql, interviewId, topicId, true, null);
        sql.append(" LIMIT ");
        sql.append(page);
        sql.append(",");
        sql.append(size);
        Query query = entityManager.createNativeQuery(sql.toString());
        setSqlOrQuery(sql, interviewId, topicId, false, query);
        //TODO 获得数据
        List<TopicAnswerDto> dtos = new ArrayList<>();
        query.getResultList().forEach(item -> dtos.add(getTopic((Object[]) item, bol)));
        dto.setRows(dtos);
        //TODO 获得总行数
        String countSql = sql.replace(7, column.length() + 7, "COUNT(1)").toString();
        countSql = countSql.substring(0, countSql.lastIndexOf(" "));
        countSql = countSql.substring(0, countSql.lastIndexOf(" "));
        query = entityManager.createNativeQuery(countSql);
        setSqlOrQuery(sql, interviewId, topicId, false, query);
        dto.setTotal(StringUtil.nullToInteger(query.getResultList().get(0)));
        int pagesize = (int) Math.ceil((dto.getTotal() * 1.0) / size);
        dto.setPageSize(pagesize < 1 ? 1 : pagesize);
        return dto;
    }

    /**
     * 设置sql或参数
     *
     * @param sql
     * @param bol true：设置sql，false：设置参数
     */
    private void setSqlOrQuery(StringBuffer sql, Long interviewId, Long topicId, boolean bol, Query query) {
        if (interviewId != null) {
            if (bol) {
                sql.append(" AND ti.interviewId = :interviewId");
            } else {
                query.setParameter("interviewId", interviewId);
            }
        }
        if (topicId != null) {
            if (bol) {
                sql.append(" AND ti.topicId = :topicId");
            } else {
                query.setParameter("topicId", topicId);
            }
        }
    }

    /**
     * 根据面试编号统计回答数据
     *
     * @param interviewId
     * @return
     */
    public TopicAnswerDto statistics(Long interviewId) {
        Query query = entityManager.createNativeQuery("SELECT sum(CASE WHEN t.answer = 'x' THEN 0 ELSE 1 END) answerCount,count(1) total,SUM(CASE WHEN (t.answer = tt.answer AND t.`status`=1) THEN 1 ELSE 0 END) correctAnswer,SUM(CASE t.`status` WHEN 0 THEN 1 ELSE 0 END) timeout FROM sys_interview_answer t INNER JOIN sys_topic tt ON t.topicId = tt.id AND t.interviewId = ?");
        query.setParameter(1, interviewId);
        return getTopic((Object[]) query.getResultList().get(0));
    }

    /**
     * 解析
     *
     * @param array
     * @return
     */
    private TopicAnswerDto getTopic(Object[] array, boolean bol) {
        if (array == null) return null;
        TopicAnswerDto topic = new TopicAnswerDto();
        topic.setId(StringUtil.nullToLong(array[0]));
        topic.setTopicId(StringUtil.nullToLong(array[1]));
        topic.setStatus(StringUtil.nullToInteger(array[2]));
        topic.setAnswer(StringUtil.nullToString(array[3]));
        topic.setInterviewId(StringUtil.nullToLong(array[4]));
        topic.setCreateStamp(StringUtil.nullToDate(array[5], "yyyy-MM-dd HH:mm:ss"));
        topic.setUpdateStamp(StringUtil.nullToDate(array[6], "yyyy-MM-dd HH:mm:ss"));
        topic.setName(StringUtil.nullToString(array[7]));
        topic.setSelectAnswer(StringUtil.nullToString(array[8]));
        if (bol)
            topic.setCorrectAnswer(StringUtil.nullToString(array[9]));
        return topic;
    }

    /**
     * 解析
     *
     * @param array
     * @return
     */
    private TopicAnswerDto getTopic(Object[] array) {
        if (array == null) return null;
        TopicAnswerDto topic = new TopicAnswerDto();
        topic.setAnswerCount(StringUtil.nullToInteger(array[0]));
        topic.setTotal(StringUtil.nullToInteger(array[1]));
        topic.setCorrectAnswerCount(StringUtil.nullToInteger(array[2]));
        topic.setTimeout(StringUtil.nullToInteger(array[3]));
        return topic;
    }

    /**
     * 修改
     *
     * @param interviewAnswer
     * @return
     */
    public boolean update(InterviewAnswer interviewAnswer) {
        Query query = entityManager.createNativeQuery("UPDATE `test`.`sys_interview_answer` SET `status`=?, `answer`=?,`updateStamp`=?,`ip`=? WHERE (`interviewId`=? and `topicId`=?)");
        query.setParameter(1, interviewAnswer.getStatus());
        query.setParameter(2, interviewAnswer.getAnswer());
        query.setParameter(3, interviewAnswer.getUpdateStamp());
        query.setParameter(4, interviewAnswer.getIp());
        query.setParameter(5, interviewAnswer.getInterviewId());
        query.setParameter(6, interviewAnswer.getTopicId());
        return query.executeUpdate() > 0 ? true : false;
    }

}
