package com.example.demo.dao.impl;

import com.example.demo.pojo.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 面试后端接口实现
 * @date 2018/6/1 17:29
 */
@Repository
public class TemplateDaoImpl {

    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    public boolean update(Template template) {
        StringBuffer sql = new StringBuffer("UPDATE sys_template SET ");
        setSqlOrQuery(sql, template, true, null);
        Query query = entityManager.createNativeQuery(sql.toString());
        setSqlOrQuery(sql, template, false, query);
        return query.executeUpdate() > 0 ? true : false;
    }

    /**
     * 设置sql或参数
     *
     * @param sql
     * @param bol true：设置sql，false：设置参数
     */
    private void setSqlOrQuery(StringBuffer sql, Template template, boolean bol, Query query) {
        if (template.getTopicType() != null) {
            if (bol) {
                sql.append(" topicType = :topicType,");
            } else {
                query.setParameter("topicType", template.getTopicType());
            }
        }
        if (template.getJobType() != null) {
            if (bol) {
                sql.append(" jobType = :jobType,");
            } else {
                query.setParameter("jobType", template.getJobType());
            }
        }
        if (template.getTopicCount() != null) {
            if (bol) {
                sql.append(" topicCount = :topicCount,");
            } else {
                query.setParameter("topicCount", template.getTopicCount());
            }
        }
        if (template.getMustTopic() != null) {
            if (bol) {
                sql.append(" mustTopic = :mustTopic,");
            } else {
                query.setParameter("mustTopic", template.getMustTopic());
            }
        }
        sql.replace(0,sql.length(),sql.substring(0, sql.length() - 1));
        if (template.getId() != null) {
            if (bol) {
                sql.append(" WHERE id = :id");
            } else {
                query.setParameter("id", template.getId());
            }
        }
    }

}
