package com.example.demo.dao;

import com.example.demo.dto.PrivilegesDto;
import com.example.demo.dto.ResultDto;
import com.example.demo.pojo.Privileges;

import java.util.List;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 权限后端接口
 * @date 2018/6/1 17:29
 */
public interface PrivilegesDao {
    /**
     * @Description: 根据用户编号查询角色
     * @author ShengGuang.Ye
     * @date 2018/6/1 17:07
     * @version V1.0
     */
    public List<Privileges> findByRoleId(Long roleid);

    /**
     * 权限详情
     */
    PrivilegesDto getPrivilegesDao(Long id);

    /**
     * 权限资源查询
     *
     * @param privileges
     * @param orderBy
     * @param page
     * @param size
     * @return
     */
    ResultDto findPrivileges(Privileges privileges, String orderBy, Integer page, Integer size);

    /**
     * 修改权限资源
     *
     * @param privileges
     * @return
     */
    boolean updatePrivileges(Privileges privileges);

    /**
     * 根据权限编号查询是否有角色绑定
     *
     * @param privilegesid
     * @return
     */
    boolean findPrivilegesRole(Long privilegesid);

    /**
     * 根据权限编号查询是否有子权限绑定
     *
     * @param privilegesid
     * @return
     */
    boolean findPrivilegesSonPrivilieges(Long privilegesid);

    /**
     * 查询角色最大code
     *
     * @return
     */
    String findMaxCode(Long parentid);
}
