package com.example.demo.dao;

import com.example.demo.dto.ResultDto;
import com.example.demo.dto.UserDto;
import com.example.demo.pojo.User;

/**
 * @author ShengGuang.Ye
 * @version V1.0
 * @Description: 用户功能接口定义
 * @date 2018/5/24 21:07
 */
public interface UserDao {

    /**
     * 查询用户信息
     *
     * @param page
     * @param size
     * @param sort
     * @return
     */
    ResultDto findUserDao(User user, Integer page, Integer size, String sort);

    /**
     * 修改用户信息
     *
     * @param user
     * @return
     */
    boolean updateUserDao(User user);

    /**
     * 根据用户编号获得用户信息
     *
     * @param userid
     * @return
     */
    UserDto getUserDao(Long userid);

    /**
     * 删除用户关联的角色信息
     *
     * @param userid
     * @return
     */
    boolean delUserRole(Long userid);

}
